#include <iostream>
#include <queue>

#include "client.h"
#include "memory_map.h"

#define MASK_PAGE            (~(SIZE_PAGE    - 1))
#define MASK_SECTOR            (~(SIZE_SECTOR    - 1))
#define MASK_BLOCK            (~(SIZE_BLOCK    - 1))
#define MASK_DEVICE            (~(SIZE_DEVICE    - 1))

constexpr auto LOWEST_PRIORITY = 5;
static Client* client_list[LOWEST_PRIORITY];

static op_erase_type_t CalcEraseType(uint32_t addr, uint32_t numBytes);
static uint32_t LookupEraseSize(uint16_t num_bytes);

static op_erase_type_t CalcEraseType(uint32_t addr, uint32_t numBytes)
{
    op_erase_type_t return_val = ERASE_MAX_COUNT;

    if (!(addr & ~MASK_PAGE) && (numBytes >= SIZE_PAGE))
    {
        return_val = ERASE_PAGE;
    }

    if (!(addr & ~MASK_SECTOR) && (numBytes >= SIZE_SECTOR))
    {
        return_val = ERASE_SECTOR;
    }

    if (!(addr & ~MASK_BLOCK) && (numBytes >= SIZE_BLOCK))
    {
        return_val = ERASE_BLOCK;
    }

    if (!(addr & ~MASK_DEVICE) && (numBytes >= SIZE_DEVICE))
    {
        return_val = ERASE_DEVICE;
    }

    return return_val;
}

static uint32_t LookupEraseSize(uint16_t num_bytes)
{
    uint32_t return_val = 0;

    if (num_bytes <= SIZE_PAGE)
    {
        return_val = SIZE_PAGE;
    }
    else if (num_bytes <= SIZE_SECTOR)
    {
        return_val = SIZE_SECTOR;
    }
    else if (num_bytes <= SIZE_BLOCK)
    {
        return_val = SIZE_BLOCK;
    }
    else if (num_bytes <= SIZE_DEVICE)
    {
        return_val = SIZE_DEVICE;
    }

    return return_val;
}

Client::Client(uint8_t priority, uint32_t addr, uint16_t num_bytes)
{
    if (priority > LOWEST_PRIORITY)
    {
        priority = LOWEST_PRIORITY - 1;
    }
    else
    {
        priority = priority;
    }

    addr_start    = addr;
    addr_write    = addr;
    addr_read    = addr;
    size        = num_bytes;
    op_status    = STATUS_NOT_QUEUED;
}

Client::~Client()
{
}

op_error_t Client::queue_new_operations(op_cmd_t cmd, void * data, uint16_t num_bytes)
{
    op_error_t return_val = ERROR_MAX_COUNT;

    if ((cmd != CMD_WRITE) && (cmd != CMD_READ))
    {
        return_val = ERROR_INVALID_CMD;
    }
    else if (op_status == STATUS_QUEUED || op_status == STATUS_INPROGRESS)
    {
        return_val = ERROR_CLIENT_BUSY;
    }
    else
    {
        // valid operation
        op_cmd = cmd;
        op_ptr = data;
        op_size = num_bytes;
        client_list[this->priority] = this;
        op_status = STATUS_QUEUED;
        
        if (cmd == CMD_ERASE)
        {
            op_size = LookupEraseSize(num_bytes);
        }
        
        return_val = ERROR_SUCCESS;
    }

    return return_val;
}

op_status_t Client::client_status(void)
{
    return op_status;
}

void process_queued_operations(void)
{
    uint8_t write_count = 0;
    uint8_t read_count = 0;
    uint8_t *input_ref = NULL;

    for (uint8_t i = 0; i < LOWEST_PRIORITY; i++)
    {
        if (client_list[i] != NULL && client_list[i]->op_status == STATUS_QUEUED)
        {
            client_list[i]->op_status = STATUS_INPROGRESS;
        }

        else if (client_list[i] != NULL && client_list[i]->op_status == STATUS_INPROGRESS)
        {
            switch (client_list[i]->op_cmd)
            {
                case CMD_ERASE:
                    while (client_list[i]->op_size--)
                    {
                        flash_map[client_list[i]->addr_write] = 0x00;

                        client_list[i]->addr_write = (client_list[i]->addr_write + 1);

                        if (client_list[i]->addr_write == (client_list[i]->addr_start + client_list[i]->size))
                        {
                            client_list[i]->addr_write = client_list[i]->addr_start;
                        }

                        if (client_list[i]->addr_write == client_list[i]->addr_read)
                        {
                            break;
                        }
                    }
                    break;

                case CMD_WRITE:
                    input_ref = (uint8_t *)client_list[i]->op_ptr;

                    while (client_list[i]->op_size--)
                    {
                        flash_map[client_list[i]->addr_write] = input_ref[write_count++];

                        client_list[i]->addr_write = (client_list[i]->addr_write + 1);

                        if (client_list[i]->addr_write == (client_list[i]->addr_start + client_list[i]->size))
                        {
                            client_list[i]->addr_write = client_list[i]->addr_start;
                        }

                        if (client_list[i]->addr_write == client_list[i]->addr_read)
                        {
                            break;
                        }
                    }
                    break;

                case CMD_READ:
                    input_ref = (uint8_t *)client_list[i]->op_ptr;

                    while (client_list[i]->op_size--)
                    {
                        input_ref[read_count++] = flash_map[client_list[i]->addr_read];

                        client_list[i]->addr_read = (client_list[i]->addr_read + 1);

                        if (client_list[i]->addr_read == (client_list[i]->addr_start + client_list[i]->size))
                        {
                            client_list[i]->addr_read = client_list[i]->addr_start;
                        }

                        if (client_list[i]->addr_write == client_list[i]->addr_read)
                        {
                            break;
                        }
                    }
                    break;

                default:
                    break;
            }

            client_list[i]->op_status = STATUS_COMPLETED;
        }

        else if (client_list[i] != NULL && client_list[i]->op_status == STATUS_COMPLETED)
        {
            client_list[i] = NULL;
        }
    }
}


void client_init(void)
{
}

void client_update(void)
{
    process_queued_operations();
}

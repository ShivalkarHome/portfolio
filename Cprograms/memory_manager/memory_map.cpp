#include <stdio.h>

#include "memory_map.h"

uint8_t flash_map[] = { 0 };

void print_flash_map(void)
{
    for (uint32_t i = 0; i < FLASH_NUM_BYTES_SIZE; i=i+8)
    {
        printf("0x%04x:: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\n", i,
                        flash_map[i], flash_map[i + 1], flash_map[i + 2], flash_map[i + 3],
                        flash_map[i + 4], flash_map[i + 5], flash_map[i + 6], flash_map[i + 7]);
    }
}

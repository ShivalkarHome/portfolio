#ifndef _CLIENT_H_
#define _CLIENT_H_

typedef enum
{
    CMD_ERASE,
    CMD_WRITE,
    CMD_READ,

    CMD_MAX_COUNT,
}op_cmd_t;

typedef enum
{
    ERASE_PAGE,        // (0x10)
    ERASE_SECTOR,    // (0x40)
    ERASE_BLOCK,    // (0x100)
    ERASE_DEVICE,    // (0x400)

    ERASE_MAX_COUNT,
}op_erase_type_t;

typedef enum
{
    STATUS_NOT_QUEUED,
    STATUS_QUEUED,
    STATUS_INPROGRESS,
    STATUS_COMPLETED,

    STATUS_MAX_COUNT,
}op_status_t;


typedef enum
{
    ERROR_SUCCESS,
    ERROR_CLIENT_BUSY,
    ERROR_INVALID_CMD,
    ERROR_INVALID_ERASE_SIZE,

    ERROR_MAX_COUNT,
}op_error_t;

class Client
{
private:
    uint32_t    priority;
    uint32_t    addr_start;
    uint32_t    size;
    
    op_cmd_t    op_cmd;
    void        *op_ptr;
    uint32_t    op_size;

protected:    
    uint32_t    addr_read;
    uint32_t    addr_write;
    op_status_t    op_status;

public:
    static uint32_t num_obj;

    Client(uint8_t priority, uint32_t addr, uint16_t num_bytes);
    ~Client();

    op_error_t queue_new_operations(op_cmd_t cmd, void * data, uint16_t num_bytes);
    op_status_t client_status(void);
    
    friend void process_queued_operations(void);
};

void client_init(void);
void client_update(void);

#endif // _CLIENT_H_

#ifndef _MEMORY_MAP_H_
#define _MEMORY_MAP_H_

#include <stdint.h>

constexpr auto SIZE_PAGE    = (0x10);        // 10
constexpr auto SIZE_SECTOR    = (0x40);        // 64
constexpr auto SIZE_BLOCK    = (0x100);        // 256
constexpr auto SIZE_DEVICE    = (0x400);        // 1024

constexpr auto FLASH_NUM_BYTES_SIZE = SIZE_DEVICE;

extern uint8_t flash_map[FLASH_NUM_BYTES_SIZE];

void print_flash_map(void);

#endif // _MEMORY_MAP_H_
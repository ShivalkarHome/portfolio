#include "delay.h"
#include "comparator.h"
#include "common_includes_list.h"

#define DELAY_PERIOD__ms     100

void cmp_app(void)
{
    comparator<float32_t> temp;
    temp.assign_set_conditions(75.0f, GREATER_THAN, 3000);
    temp.assign_clear_conditions(65.0f, LESS_THAN_EQUAL_TO, 3000);

    for (float32_t y = 65.0f; y < 85.0f; y++)
    {
        std::cout << "temp = " << y << std::endl;
        temp.fault_monitor(y);
        delay_ms(DELAY_PERIOD__ms);

        std::cout << "error = " << temp.state() << std::endl << std::endl;
    }

    for (float32_t y = 85.0f; y > 60.0f; y--)
    {
        std::cout << "temp = " << y << std::endl;
        temp.fault_monitor(y);
        delay_ms(DELAY_PERIOD__ms);

        std::cout << "error = " << temp.state() << std::endl << std::endl;
    }

    return;
}
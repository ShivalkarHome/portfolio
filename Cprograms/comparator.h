#ifndef _COMPARATOR_H_
#define _COMPARATOR_H_

#include "common_includes_list.h"

#include "timer.h"
#include "types.h"

typedef enum
{
    EQUAL_TO,
    LESS_THAN,
    LESS_THAN_EQUAL_TO,
    GREATER_THAN,
    GREATER_THAN_EQUAL_TO,
    RANGE
} operation_enum_t;

typedef enum
{
    CLEAR,
    SET
}error_state_t;

template <typename T>
bool run_comparison(T val_1, T val_2, T val_3, operation_enum_t cmp_op)
{
    bool output = false;

    switch (cmp_op)
    {
    case EQUAL_TO:
        output = (val_1 == val_2);
        break;

    case LESS_THAN:
        output = (val_1 < val_2);
        break;

    case LESS_THAN_EQUAL_TO:
        output = (val_1 <= val_2);
        break;

    case GREATER_THAN:
        output = (val_1 > val_2);
        break;

    case GREATER_THAN_EQUAL_TO:
        output = (val_1 >= val_2);
        break;

    case RANGE:
        output = ((val_1 > val_2) || (val_1 < val_3));
        break;

    default:
        break;
    }

    return output;
}

template <class T>
class comparator
{
private:
    T source_val;
    error_state_t current_state;
    //set conditions
    T set_threshold;
    operation_enum_t set_op;
    time_t set_time;
    //clear conditions
    T clear_threshold;
    operation_enum_t clear_op;
    time_t clear_time;

public:
    comparator();
    void assign_set_conditions(T threshold, operation_enum_t op, time_t time);
    void assign_clear_conditions(T threshold, operation_enum_t op, time_t time);
    void fault_monitor(T new_sample);
    error_state_t state(void);
    ~comparator();
};

template<class T>
inline comparator<T>::comparator()
{
    current_state = CLEAR;
}

template<class T>
inline void comparator<T>::assign_set_conditions(T threshold, operation_enum_t op, time_t time)
{
    set_threshold = threshold;
    set_op = op;
    set_time = time;
}

template<class T>
inline void comparator<T>::assign_clear_conditions(T threshold, operation_enum_t op, time_t time)
{
    clear_threshold = threshold;
    clear_op = op;
    clear_time = time;
}

template<class T>
inline void comparator<T>::fault_monitor(T new_sample)
{
    source_val = new_sample;
    static uint8_t set_counter = 0;
    static uint8_t clear_counter = 0;
    static timer set_timer(set_time);
    static timer clear_timer(clear_time);

    if ((current_state == CLEAR) && run_comparison<float32_t>(source_val, set_threshold, 0, set_op))
    {
        std::cout << "trying to set" << std::endl;
        
        if(!set_timer.timer_is_active())
        {
            set_timer.timer_start();
        }
        
        set_counter++;

        if (set_timer.timer_is_expired() && set_counter >= 3)
        {
            current_state = SET;
            set_timer.timer_stop();
        }
    }

    if((current_state == SET) && (run_comparison<float32_t>(source_val, clear_threshold, 0, clear_op)))
    {
        std::cout << "trying to clear" << std::endl;

        if (!clear_timer.timer_is_active())
        {
            clear_timer.timer_start();
        }

        clear_counter++;

        if (clear_timer.timer_is_expired() && clear_counter >= 3)
        {
            current_state = CLEAR;
            clear_timer.timer_stop();
        }
    }
}

template<class T>
inline error_state_t comparator<T>::state()
{
    return current_state;
}

template<class T>
inline comparator<T>::~comparator()
{
}


void cmp_app(void);

#endif /* _COMPARATOR_H_ */

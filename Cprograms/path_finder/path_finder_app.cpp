#include "path_finder.h"

uint8_t matrix[max_row_count][max_col_count] = {
        { 3, 4, 1, 2, 8, 6 },
        { 6, 1, 8, 2, 7, 4 },
        { 5, 9, 3, 9, 9, 5 },
        { 8, 4, 1, 3, 2, 6 },
        { 3, 7, 2, 8, 6, 4 }
};

int path_finder()
{
    position_t dimesion = { max_row_count, max_col_count };
    
    position_t start_pt = { 2, 0 };
    position_t dest_pt = { 2, 5 };
    printf("start pos = {%hhu, %hhu}\tdest pos = {%u, %u}\tdistance = %u\n",
        start_pt.row, start_pt.col,
        dest_pt.row, dest_pt.col,
        compute_path(matrix, start_pt, dest_pt));

    return 0;
}

#include <list>

#include "path_finder.h"

bool is_valid_cell(position_t pos)
{
    return (pos.row >= 0) && (pos.row < max_row_count) &&
        (pos.col >= 0) && (pos.col < max_col_count);
}

uint32_t compute_path(uint8_t array_2d[][max_col_count], position_t src, position_t dst)
{
    node_t tracker[max_row_count][max_col_count] = { 0 };

    tracker[src.row][src.col].dist = array_2d[src.row][src.col];
    tracker[src.row][src.col].visited = true;

    std::list<position_t> list_of_nodes;
    
    position_t origin = { src };

    list_of_nodes.push_back(origin);

    while (!list_of_nodes.empty())
    {
        position_t current_node = list_of_nodes.front();
        position_t temp_cell;

        printf("cell = {%hhu, %hhu}\n", current_node.row, current_node.col);
        printf("dist = %hhu\n", array_2d[current_node.row][current_node.col]);

        if (current_node.row == dst.row && current_node.col == dst.col)
        {
            return tracker[current_node.row][current_node.col].dist;
        }

        list_of_nodes.pop_front();

        temp_cell.row = current_node.row - 1;
        temp_cell.col = current_node.col + 1;

        if (is_valid_cell(temp_cell))
        {
            if (!tracker[temp_cell.row][temp_cell.col].visited)
            {
                tracker[temp_cell.row][temp_cell.col].cell = { current_node };
                tracker[temp_cell.row][temp_cell.col].visited = true;
                tracker[temp_cell.row][temp_cell.col].dist = array_2d[temp_cell.row][temp_cell.col]
                    + array_2d[current_node.row][current_node.col];

                printf("\tcell = {%hhu, %hhu}\n", temp_cell.row, temp_cell.col);
                printf("\tdist = %hhu\n", array_2d[temp_cell.row][temp_cell.col]);

                list_of_nodes.push_back(temp_cell);
            }
        }

        temp_cell.row = current_node.row;
        temp_cell.col = current_node.col + 1;

        if (is_valid_cell(temp_cell))
        {
            if (!tracker[temp_cell.row][temp_cell.col].visited)
            {
                tracker[temp_cell.row][temp_cell.col].cell = { current_node };
                tracker[temp_cell.row][temp_cell.col].visited = true;
                tracker[temp_cell.row][temp_cell.col].dist = array_2d[temp_cell.row][temp_cell.col]
                    + array_2d[current_node.row][current_node.col];

                printf("\tcell = {%hhu, %hhu}\n", temp_cell.row, temp_cell.col);
                printf("\tdist = %hhu\n", array_2d[temp_cell.row][temp_cell.col]);

                list_of_nodes.push_back(temp_cell);
            }
        }

        temp_cell.row = current_node.row + 1;
        temp_cell.col = current_node.col + 1;

        if (is_valid_cell(temp_cell))
        {
            if(!tracker[temp_cell.row][temp_cell.col].visited)
            {
                tracker[temp_cell.row][temp_cell.col].cell = { current_node };
                tracker[temp_cell.row][temp_cell.col].visited = true;
                tracker[temp_cell.row][temp_cell.col].dist = array_2d[temp_cell.row][temp_cell.col]
                    + array_2d[current_node.row][current_node.col];

                printf("\tcell = {%hhu, %hhu}\n", temp_cell.row, temp_cell.col);
                printf("\tdist = %hhu\n", array_2d[temp_cell.row][temp_cell.col]);

                list_of_nodes.push_back(temp_cell);
            }
        }
    }

    // No pah found
    return -1;
}

void print_matrix(uint8_t array_2d[][max_col_count], position_t dimesion)
{
    uint8_t k = 0;
    for (uint8_t i = 0; i < dimesion.row; i++)
    {
        for (uint8_t j = 0; j < dimesion.col; j++, k++)
        {
            printf("%d\t", array_2d[i][j]);
        }
        printf("\n");
    }
}

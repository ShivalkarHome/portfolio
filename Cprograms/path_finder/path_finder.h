#ifndef _PATH_FINDER_H_
#define _PATH_FINDER_H_

#include <stdbool.h>
#include <stdint.h>

typedef struct
{
    uint8_t row;
    uint8_t col;
}position_t;

typedef struct
{
    position_t cell;
    uint8_t dist;
    bool visited;
}node_t;

const uint8_t max_row_count = 5;
const uint8_t max_col_count = 6;

bool is_valid_cell(position_t pos);
uint32_t compute_path(uint8_t array_2d[][max_col_count], position_t src, position_t dst);
void print_matrix(uint8_t array_2d[][max_col_count], position_t dimesion);

#endif /* _PATH_FINDER_H_ */
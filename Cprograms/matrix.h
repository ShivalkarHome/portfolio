#ifndef _MATRIX_H_
#define _MATRIX_H_

#include "common_includes_list.h"

#define SIZE  10

typedef enum
{
    row,
    col,
}swap_by_enum_t;

void rotate_matrix(void);
void print_matrix(uint8_t matrix_ins[SIZE][SIZE], uint8_t row_count, uint8_t col_count);
void transpose_matrix(uint8_t matrix_ins[SIZE][SIZE], uint8_t row_count, uint8_t col_count);
void swap_matrix(uint8_t matrix_ins[SIZE][SIZE], uint8_t row_count, uint8_t col_count, swap_by_enum_t swap_by);

#endif /* _MATRIX_H_ */

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>

#define NUM_THREADS 2

pthread_mutex_t A;
pthread_mutex_t B;

char kFirstString[16] = "First Values.";
char kSecondString[16] = "Second Values.";
char kThirdString[16] = "Second Values.";

void* Thread1(void* arg)
{
    int n = 100;
    char* ptr = (char*)malloc(n * sizeof(char));
    (void)arg;
    pthread_mutex_lock(&A);
    pthread_mutex_lock(&B);

    strcpy(kFirstString, kThirdString);
    printf("%s\n", kFirstString);

    pthread_mutex_unlock(&B);
    pthread_mutex_unlock(&A);

    return NULL;
}

void* Thread2(void* arg)
{
    (void)arg;
    pthread_mutex_lock(&B);
    pthread_mutex_lock(&A);

    printf("%s\n", kSecondString);

    pthread_mutex_unlock(&A);
    pthread_mutex_unlock(&B);

    return NULL;
}

char* Function(const char* s, bool b) {
    char* x = (char*)malloc(strlen(s) + 1);
    for (uint32_t i = 0; i < strlen(s); i++) {
        x[i] = b ? s[strlen(s) - 1 - i] : s[i];
    }
    return x;
}

int main()
{
    uint32_t error;
    pthread_t p1;
    pthread_t p2;

    if (pthread_mutex_init(&A, NULL) != 0) {
        printf("\n mutex A init has failed\n");
        return 1;
    }

    if (pthread_mutex_init(&B, NULL) != 0) {
        printf("\n mutex B init has failed\n");
        return 1;
    }

    error = pthread_create(&(p1), NULL, &Thread1, NULL);
    if (error != 0)
    {
        printf("Thread can't be created :[%s]\n", strerror(error));
    }

    error = pthread_create(&(p2), NULL, &Thread1, NULL);
    if (error != 0)
    {
        printf("Thread can't be created :[%s]\n", strerror(error));
    }

    pthread_join(p1, NULL);
    pthread_join(p2, NULL);

    pthread_mutex_destroy(&A);
    pthread_mutex_destroy(&B);

    char* copy_ptr = Function("HELLO", false);
    printf("[function test]:%s", copy_ptr);

    return 0;
}

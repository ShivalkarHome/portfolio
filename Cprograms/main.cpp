#include <iostream>

#include "state_machine\state_machine_mgr.h"
#include "state_machine\state_machine.h"
#include "\Portfolio\Cprograms\time\timer.h"
#include "memory_manager\client.h"
#include "memory_manager\memory_map.h"

constexpr auto TASK1_INTERVAL_MS = 10;
constexpr auto TASK2_INTERVAL_MS = 50;

static bool flag = true;

int main(void)
{    
    static uint16_t counter = 50;

    timer task1_interval_timer(TASK1_INTERVAL_MS);
    task1_interval_timer.timer_repeating_start();

    timer task2_interval_timer(TASK2_INTERVAL_MS);
    task2_interval_timer.timer_repeating_start();

    state_machine_init();
    state_machine_printStates();

    client_init();
    
    Client client_0(0, SIZE_SECTOR, SIZE_SECTOR);

    uint8_t data_array[] = { 1, 2, 3, 4, 5 };
    uint32_t data_size = sizeof(data_array);

    uint8_t data_read[] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
    uint32_t data_read_size = sizeof(data_read);

    while (1)
    {
        if (task1_interval_timer.timer_has_expired())
        {
            state_machine_update();
            state_machine_printStates();
        }

        if (task2_interval_timer.timer_has_expired())
        {
            if (counter%5==0)
            {
                if (client_0.queue_new_operations(CMD_READ, data_read, data_read_size) == ERROR_SUCCESS)
                {
                    flag = false;
                }
            }
            else
            {
                if (client_0.queue_new_operations(CMD_WRITE, data_array, data_size) == ERROR_SUCCESS)
                {
                    flag = true;
                }
            }
            
            client_update();

            for (uint8_t i = 0; i < data_read_size; i++)
            {
                printf("0x%x\t", data_read[i]);
            }
            printf("\n");

            if (counter-- == 0)
            {
                task1_interval_timer.timer_stop();
                task2_interval_timer.timer_stop();
                print_flash_map();
                break;
            }
        }
    }

    return 0;
}
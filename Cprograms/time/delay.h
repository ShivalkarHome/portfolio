#ifndef _DELAY_H_
#define _DELAY_H_

void delay_ms(time_t time_period_ms);
void delay_sec(time_t time_period_s);

#endif /* _DELAY_H_ */

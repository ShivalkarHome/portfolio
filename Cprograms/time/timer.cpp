#include "clock.h"
#include "timer.h"

timer::timer(time_t time_period_ms)
{
    active    = false;
    repeating = false;
    tripped = false;
    start_time_ms = 0;
    trigger_time_ms = time_period_ms;
}

void timer::timer_oneshot_start(void)
{
    active = true;
    repeating = false;
    tripped = false;

    start_time_ms = Clock_Time__ms();
}

void timer::timer_repeating_start(void)
{
    active = true;
    repeating = true;
    tripped = false;

    start_time_ms = Clock_Time__ms();
}

void timer::timer_stop(void)
{
    active = false;
    repeating = false;
    tripped = false;
    start_time_ms = 0;
    trigger_time_ms = 0;
}

bool timer::timer_has_expired()
{
    tripped = false;

    time_t current_time_ms = Clock_Time__ms();

    if (active)
    {
        if ((current_time_ms - start_time_ms) > trigger_time_ms)
        {
            if (repeating)
            {
                start_time_ms = current_time_ms;
            }
            else
            {
                active = false;
            }

            tripped = true;
        }
    }

    return tripped;
}

bool timer::timer_is_active()
{
    return active;
}

timer::~timer()
{
    timer_stop();
}

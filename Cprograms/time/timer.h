#ifndef _TIMER_H_
#define _TIMER_H_

#include <time.h>

class timer
{
private:
    bool    active;
    bool    repeating;
    bool    tripped;
    time_t    start_time_ms;
    time_t    trigger_time_ms;
public:
    timer(time_t time_period_ms);
    void timer_oneshot_start(void);
    void timer_repeating_start(void);
    void timer_stop(void);
    bool timer_has_expired();
    bool timer_is_active();
    ~timer();
};

#endif /* _TIMER_H_ */

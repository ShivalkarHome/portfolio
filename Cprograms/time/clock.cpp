#include "clock.h"

time_t Clock_Time__ms(void)
{
    auto time_point = std::chrono::duration_cast<std::chrono::milliseconds>(
        +std::chrono::steady_clock::now().time_since_epoch());
    return time_point.count();
}
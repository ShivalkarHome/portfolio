#ifndef _TIME_H_
#define _TIME_H_

#include <chrono>
#include <ctime>

time_t Clock_Time__ms(void);

#endif /* _TIME_H_ */
#include "timer.h"
#include "delay.h"

void delay_ms(time_t time_period_ms)
{
    timer delay_ms(time_period_ms);
    delay_ms.timer_oneshot_start();

    while (true)
    {
        if (delay_ms.timer_has_expired())
        {
            delay_ms.timer_stop();
            break;
        }
    }
}

void delay_sec(time_t time_period_s)
{
    delay_ms(time_period_s * 1000);
}

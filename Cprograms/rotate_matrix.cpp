#include "common_includes_list.h"

#include "matrix.h"

void rotate_matrix(void)
{
    uint8_t rows = 6;
    uint8_t cols = 9;

    uint8_t matrix_ins[SIZE][SIZE];
    
    uint8_t k = 0;
    for (uint8_t i = 0; i < rows; i++)
    {
        for (uint8_t j = 0; j < cols; j++)
        {
            matrix_ins[i][j] = k++;
        }
    }
    
    print_matrix(matrix_ins, rows, cols);

    transpose_matrix(matrix_ins, rows, cols);
    swap_matrix(matrix_ins, cols, rows, row);

    transpose_matrix(matrix_ins, rows, cols);
    swap_matrix(matrix_ins, rows, cols, row);

    print_matrix(matrix_ins, rows, cols);

    return;
}


void print_matrix(uint8_t matrix_ins[SIZE][SIZE], uint8_t row_count, uint8_t col_count)
{
    uint8_t i, j, k = 0;

    for (i = 0; i < row_count; i++)
    {
        for (j = 0; j < col_count; j++, k++)
        {
            printf("%2hhu  ", matrix_ins[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}


void transpose_matrix(uint8_t matrix_ins[SIZE][SIZE], uint8_t row_count, uint8_t col_count)
{
    uint8_t count = (row_count > col_count ? row_count : col_count);

    for (uint8_t i = 0; i < count; i++)
    {
        for (uint8_t j = i; j < count; j++)
        {
            uint8_t temp = matrix_ins[i][j];
            matrix_ins[i][j] = matrix_ins[j][i];
            matrix_ins[j][i] = temp;
        }
    }
}

void swap_matrix(uint8_t matrix_ins[SIZE][SIZE], uint8_t row_count, uint8_t col_count, swap_by_enum_t swap_by)
{
    if (swap_by == col)
    {
        for (uint8_t i = 0; i < row_count; i++)
        {
            for (uint8_t j = 0; j < col_count / 2; j++)
            {
                uint8_t temp = matrix_ins[i][j];
                matrix_ins[i][j] = matrix_ins[i][col_count - 1 - j];
                matrix_ins[i][col_count - 1 - j] = temp;
            }
        }
    }
    else if(swap_by == row)
    {
        for (uint8_t i = 0; i < row_count / 2; i++)
        {
            for (uint8_t j = 0; j < col_count; j++)
            {
                uint8_t temp = matrix_ins[i][j];
                matrix_ins[i][j] = matrix_ins[row_count - 1 - i][j];
                matrix_ins[row_count - 1 - i][j] = temp;
            }
        }
    }
}


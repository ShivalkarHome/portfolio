#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_

#include "state_machine_mgr.h"

typedef enum
{
    UNINITIALIZED    = 0xFF,
    PFAIL             = 0x0,
    IDLE,
    DRIVE,
    CHARGE,
    SLEEP,

    MAX_STATES,
}Vehicle_t;

extern state_model_t vehicle_model;

void state_machine_init(void);
void state_machine_update(void);
void state_machine_printStates(void);

#endif /* _STATE_MACHINE_H_ */
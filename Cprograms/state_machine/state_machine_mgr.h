#ifndef _STATE_MACHINE_MGR_H_
#define _STATE_MACHINE_MGR_H_

#include <stdint.h>

// Sample values, should be part of the state model struct
constexpr auto MAX_NUM_TRANSITIONS        = 16;
constexpr auto MAX_NUM_ENTRY_FUNCTIONS    = 6;
constexpr auto MAX_NUM_EXIT_FUNCTIONS    = 6;

typedef struct
{
    uint32_t    from_state;
    uint32_t     to_state;
    bool        (*transitionCriteria)();
}transition_matrix_t;

typedef struct
{
    bool (*function)();
}state_transition_functions_t;

typedef struct
{
    uint32_t previous;
    uint32_t current;
    uint32_t next;

    transition_matrix_t *transition_matrix_obj;
    uint32_t transition_matrix_size;

    state_transition_functions_t *state_entry_obj;
    state_transition_functions_t *state_exit_obj;
}state_model_t;


void state_machine_mgr_init(void);
void state_machine_mgr_update(state_model_t *state_machine_obj);

#endif /* _STATE_MACHINE_MGR_H_ */
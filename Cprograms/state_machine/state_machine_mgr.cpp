#include "state_machine_mgr.h"

void state_machine_mgr_init(void)
{
}

void state_machine_mgr_update(state_model_t * state_machine_obj)
{
    for (uint8_t i = 0; i < state_machine_obj->transition_matrix_size; i++)
    {
        if (state_machine_obj->current == state_machine_obj->transition_matrix_obj[i].from_state)
        {
            bool should_transition = state_machine_obj->transition_matrix_obj[i].transitionCriteria();
        
            if(should_transition)
            {
                // Execute Old State's Exit Function
                if (state_machine_obj->state_exit_obj[state_machine_obj->current].function != NULL)
                {
                    state_machine_obj->state_exit_obj[state_machine_obj->current].function();
                }

                state_machine_obj->previous = state_machine_obj->transition_matrix_obj[i].from_state;
                state_machine_obj->current = state_machine_obj->transition_matrix_obj[i].to_state;
                state_machine_obj->next = state_machine_obj->transition_matrix_obj[i].to_state;

                // Execute New State's Entry Function
                if (state_machine_obj->state_entry_obj[state_machine_obj->current].function != NULL)
                {
                    state_machine_obj->state_entry_obj[state_machine_obj->current].function();
                }
                break;
            }
        }
    }
}
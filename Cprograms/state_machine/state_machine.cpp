#include <iostream>
#include "state_machine.h"

static state_model_t vehicle_model;

// Test Purpose Only
Vehicle_t cmd_rx;
static void change_vehicle_cmds(void);

static bool initialization_complete(void);
static bool permenant_failure_detected(void);
static bool permenant_failure_cleared(void);
static bool idle_cmd_received(void);
static bool drive_cmd_received(void);
static bool charge_cmd_received(void);
static bool sleep_cmd_received(void);

static bool permenant_entry_function(void);
static bool idle_entry_function(void);
static bool drive_entry_function(void);
static bool charge_entry_function(void);
static bool sleep_entry_function(void);

static bool permenant_exit_function(void);
static bool idle_exit_function(void);
static bool drive_exit_function(void);
static bool charge_exit_function(void);
static bool sleep_exit_function(void);

static transition_matrix_t transition_matrix_ins[] =
{
    {UNINITIALIZED, IDLE, initialization_complete},

    {IDLE, PFAIL,    permenant_failure_detected},
    {IDLE, DRIVE,    drive_cmd_received},
    {IDLE, CHARGE,    charge_cmd_received},
    {IDLE, SLEEP,    sleep_cmd_received},

    {DRIVE, PFAIL,    permenant_failure_detected},
    {DRIVE, IDLE,    idle_cmd_received},

    {CHARGE, PFAIL, permenant_failure_detected},
    {CHARGE, IDLE,    idle_cmd_received},

    {PFAIL, IDLE, permenant_failure_cleared},
    {SLEEP, IDLE, idle_cmd_received},
};

static state_transition_functions_t entry_functions[MAX_STATES] =
{
    {permenant_entry_function},
    {idle_entry_function},
    {drive_entry_function},
    {charge_entry_function},
    {sleep_entry_function},
};

static state_transition_functions_t exit_functions[] =
{
    {permenant_exit_function},
    {idle_exit_function},
    {drive_exit_function},
    {charge_exit_function},
    {sleep_exit_function},
};

void state_machine_init(void)
{
    vehicle_model.previous = UNINITIALIZED;
    vehicle_model.current = UNINITIALIZED;
    vehicle_model.next = IDLE;

    vehicle_model.transition_matrix_size = sizeof(transition_matrix_ins)/ sizeof(transition_matrix_ins[0]);
    vehicle_model.transition_matrix_obj = transition_matrix_ins;

    vehicle_model.state_entry_obj = entry_functions;
    vehicle_model.state_exit_obj = exit_functions;
}

void state_machine_update(void)
{
    change_vehicle_cmds();
    state_machine_mgr_update(&vehicle_model); 
}

void state_machine_printStates(void)
{
    std::cout << vehicle_model.previous << ", " << vehicle_model.current << ", " << vehicle_model.next << std::endl;
}

static bool initialization_complete(void)
{
    return true;
}
static bool idle_cmd_received(void)
{
    return (cmd_rx == IDLE);
}
static bool drive_cmd_received(void)
{
    return (cmd_rx == DRIVE);
}
static bool charge_cmd_received(void)
{
    return (cmd_rx == CHARGE);
}
static bool sleep_cmd_received(void)
{
    return (cmd_rx == SLEEP);
}
static bool permenant_failure_detected(void)
{
    return false;
}

bool permenant_failure_cleared(void)
{
    return true;
}

static bool idle_entry_function(void)
{
    std::cout << "entring IDLE state" << std::endl;
    return true;
}
static bool drive_entry_function(void)
{
    std::cout << "entring DRIVE state" << std::endl;
    return true;
}
static bool charge_entry_function(void)
{
    std::cout << "entring CHARGE state" << std::endl;
    return true;
}
static bool sleep_entry_function(void)
{
    std::cout << "entring SLEEP state" << std::endl;
    return true;
}
static bool permenant_entry_function(void)
{
    std::cout << "entring PFAIL state" << std::endl;
    return true;
}

static bool idle_exit_function(void)
{
    std::cout << "existing IDLE state" << std::endl;
    return true;
}
static bool drive_exit_function(void)
{
    std::cout << "existing DRIVE state" << std::endl;
    return true;
}
static bool charge_exit_function(void)
{
    std::cout << "existing CHARGE state" << std::endl;
    return true;
}
static bool sleep_exit_function(void)
{
    std::cout << "existing SLEEP state" << std::endl;
    return true;
}
static bool permenant_exit_function(void)
{
    std::cout << "existing PFAIL state" << std::endl;
    return true;
}


// Unit Test - Change the vehicle cmd
static void change_vehicle_cmds(void)
{
    static uint32_t itr = 0;

    switch (itr%8)
    {
        case 0:
        case 4:
            cmd_rx = IDLE;
            break;
        case 1:
        case 5:
            cmd_rx = DRIVE;
            break;
        case 2:
        case 6:
            cmd_rx = CHARGE;
            break;
        case 3:
        case 7:
            cmd_rx = SLEEP;
            break;
        default:
            break;
    }

    itr++;
}
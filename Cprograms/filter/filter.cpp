#include "filter.h"
#include "signal.h"

void test_filter(void)
{
    uint16_t num_samples = 0;
    std::cin >> num_samples;

    for (uint16_t i = 0; i < 360; i++)
    {
        std::cout << moving_avg_window(signal, i, num_samples) << std::endl;
    }
}

float32_t moving_avg_window(float32_t * sample,
                            uint16_t ptr,
                            uint16_t num_samples)
{
    float32_t moving_avg = 0;
    float32_t running_sum = 0;

    for (uint16_t i = 0; i < num_samples; i++)
    {
        running_sum += sample[ptr - i -1];
    }

    moving_avg = running_sum / num_samples;

    return moving_avg;
}

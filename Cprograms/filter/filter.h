#ifndef _FILTER_H_
#define _FILTER_H_

#include "types.h"

void test_filter(void);
float32_t moving_avg_window(float32_t * sample, uint16_t ptr, uint16_t num_samples);

#endif /* _FILTER_H_ */
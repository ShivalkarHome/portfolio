#include "uart.h"
#include "buffer.h"

static uint16_t calculate_crc(uint8_t *frame_ptr, uint8_t len)
{
    uint16_t crc = 0;
    for(uint8_t i=0;i<len;i++)
    {
        crc = crc ^ frame_ptr[i];
    }
    printf("msg, len:%d crc:%x\n", len, crc);
    return crc;
}

UART::UART()
{
    printf("UART Test harness\n");
}

void UART::receive_byte(uint8_t byte)
{
    fifo_buffer[write_pointer] = byte;
    // printf("wr_ptr:%x, %x\n", write_pointer, fifo_buffer[write_pointer]);
    write_pointer = (write_pointer+1)%BUFFER_SIZE;
    if(write_pointer == BUFFER_SIZE-1)
    {
        ISR_simulation();
    }
}


void UART::ISR_simulation(void)
{
    is_buffer_full = true;
    printf("ISR sim, Buffer Full\n");
}

UART::~UART()
{
    printf("Tear down UART object\n");
}
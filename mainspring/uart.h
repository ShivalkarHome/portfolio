#ifndef _UART_H_
#define _UART_H_

#include<stdint.h>

class UART
{
private:
public:
    UART();
    void receive_byte(uint8_t byte);
    void ISR_simulation(void);

    ~UART();
};

#endif /* _UART_H_ */

#include "dispatch.h"
#include "buffer.h"

#define SIZE_HEADER         0x2
#define SIZE_LENGTH         0x1
#define START_SEQ_CRC       0xFF
uint8_t start_sequence[] = {0xA5, 0x5A};

typedef struct
{
    bool is_valid;
    uint32_t header;
    uint16_t len_received;
    uint16_t len_args;
    uint8_t args[11];
    uint16_t crc_received;
    uint16_t crc_calc;
}dispatch_msg_t;


dispatch_msg_t dispatch_msg_rx;


static uint16_t calculate_crc(uint8_t *frame_ptr, uint8_t len)
{
    uint16_t crc = 0;
    for(uint8_t i=0; i<len; i++)
    {
        crc = crc ^ frame_ptr[i];
    }
    // printf("crc: %x\n", crc);
    return crc;
}


DISPATCH::DISPATCH()
{
    printf("DISPATCH init Test harness\n");
}

void DISPATCH::parser(void)
{
    bool header_found = false;
    uint8_t i = 0;
    uint8_t j = 0;
    uint8_t len_copy = 0;

    if(is_buffer_full)
    {
        // Parser for header sequence
        for(i=0; i<BUFFER_SIZE-1; i++)
        {
            if(fifo_buffer[i] == start_sequence[0]
            && fifo_buffer[i+1] == start_sequence[1])
            {
                header_found = true;
                dispatch_msg_rx.header 	= (fifo_buffer[i] << 8) | fifo_buffer[i+1];

                // printf("header found at:%x\n", i);
                break;
            }
        }

        // Parser for len byte
        if(header_found && i+2 < BUFFER_SIZE)
        {
            i = i + SIZE_HEADER; // accomdating 2 bytes for header
    	    dispatch_msg_rx.len_received = fifo_buffer[i];
            dispatch_msg_rx.len_args 	= fifo_buffer[i] - SIZE_HEADER - SIZE_LENGTH; // accomdating 2 bytes for header and 1 byte of len
            len_copy = dispatch_msg_rx.len_args;
            i = i + SIZE_LENGTH;
        }

        while(header_found && len_copy--)
        {
            dispatch_msg_rx.args[j++] = fifo_buffer[i];
            i = (i+1) % BUFFER_SIZE; //accomadates for wrap around
        }

        if(header_found)
        {
            dispatch_msg_rx.crc_received = (fifo_buffer[i] << 8) | fifo_buffer[i+1];
            // crc of incoming msg is calcualted with header, length of message, data arge
            dispatch_msg_rx.crc_calc = START_SEQ_CRC ^  dispatch_msg_rx.len_received ^ calculate_crc(dispatch_msg_rx.args, dispatch_msg_rx.len_args);

            if(dispatch_msg_rx.crc_received == dispatch_msg_rx.crc_calc)
            {
                dispatch_msg_rx.is_valid = true;
                printf("Valid msg received\n");
            }
            else
            {
                printf("Invalid CRC received, ");
                printf("crc_received:%x, crc_calc:%x\n", dispatch_msg_rx.crc_received, dispatch_msg_rx.crc_calc);
            }
        }
        else
        {
            printf("Invalid msg received\n");
        }

	    is_buffer_full = false;
    }
}

DISPATCH::~DISPATCH()
{
    printf("Tear down DISPATCH object\n");
}

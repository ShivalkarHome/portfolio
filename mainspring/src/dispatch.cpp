#include "dispatch.h"
#include "buffer.h"

typedef enum
{
    P_INVALID_MSG,
    P_HEADER_DETECTED,
    P_LENGTH_DETECTED,
    P_CMD_DETECTED,
    P_CRC_DETECTED,

    P_MAX_STATES = 0xFF,
}parser_states_et;

static uint8_t j = 0;
static parser_states_et parser_states = P_INVALID_MSG;
static int8_t len_copy = 0x0;
static uint8_t start_sequence[] = {0xA5, 0x5A};

static uint16_t calculate_crc(uint8_t *frame_ptr, uint8_t len)
{
    uint16_t crc = 0;
    for(uint8_t i=0; i<len; i++)
    {
        crc = crc ^ frame_ptr[i];
    }
    // printf("crc: %x\n", crc);
    return crc;
}


DISPATCH::DISPATCH()
{
    printf("DISPATCH init Test harness\n");
}

void DISPATCH::parser(dispatch_msg_st *dispatch_msg_rx)
{
    switch (parser_states)
    {
        case P_INVALID_MSG:
            for(uint8_t i=0; i<BUFFER_SIZE-1; i++)
            {
                if(fifo_buffer[read_pointer] == start_sequence[0]
                && fifo_buffer[read_pointer+1] == start_sequence[1])
                {
                    printf("header:%d, %X\t", read_pointer, fifo_buffer[read_pointer]);
                    read_pointer = (read_pointer + SIZE_HEADER) % BUFFER_SIZE;
                    parser_states = P_HEADER_DETECTED;
                    break;
                }
            }
            break;
        case P_HEADER_DETECTED:
            dispatch_msg_rx->pckt_length = fifo_buffer[read_pointer];
            dispatch_msg_rx->args_length = fifo_buffer[read_pointer] - SIZE_METADATA;
            len_copy = dispatch_msg_rx->args_length;
            printf("length:%d, %X, %d\t", read_pointer, fifo_buffer[read_pointer], len_copy);
            read_pointer = (read_pointer + 1) % BUFFER_SIZE;
            parser_states = P_LENGTH_DETECTED;
            break;
        case P_LENGTH_DETECTED:
            dispatch_msg_rx->cmd = fifo_buffer[read_pointer];
            printf("cmd:%d, %X\t", read_pointer, fifo_buffer[read_pointer]);
            read_pointer = (read_pointer + 1) % BUFFER_SIZE;
            parser_states = P_CMD_DETECTED;
            break;
        case P_CMD_DETECTED:
            printf("args:%d, %X\t", read_pointer, fifo_buffer[read_pointer]);
            dispatch_msg_rx->args[j++] = fifo_buffer[read_pointer];
            read_pointer = (read_pointer + 1) % BUFFER_SIZE;
            if(--len_copy == 0)
            {
                parser_states = P_CRC_DETECTED;
            }
            break;
        case P_CRC_DETECTED:
            printf("crc:%d, %X\t", read_pointer, fifo_buffer[read_pointer]);
            dispatch_msg_rx->crc_received = fifo_buffer[read_pointer];
            read_pointer = (read_pointer + 1) % BUFFER_SIZE;

            dispatch_msg_rx->crc_calculated = START_SEQ_CRC ^ dispatch_msg_rx->pckt_length ^ calculate_crc(dispatch_msg_rx->args, dispatch_msg_rx->args_length);

            if(dispatch_msg_rx->crc_received == dispatch_msg_rx->crc_calculated)
            {
                dispatch_msg_rx->is_valid = true;
                printf("Valid msg received\n");
            }
            else
            {
                dispatch_msg_rx->is_valid = false;
                printf("Invalid CRC received\t");
                printf("crc_received:%x\t", dispatch_msg_rx->crc_received);
                printf("crc_calculated:%x\n", dispatch_msg_rx->crc_calculated);
            }
            j=0;
            parser_states = P_INVALID_MSG;
            break;

        default:
            parser_states = P_INVALID_MSG;
            break;
    }

    printf("parser state:%d\n", parser_states);
}

DISPATCH::~DISPATCH()
{
    printf("Tear down DISPATCH object\n");
}

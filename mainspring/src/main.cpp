#include "dispatch.h"
#include "uart.h"
#include "buffer.h"

uint16_t counter = 32;

int main()
{
    // Create pointer for class objects
    UART uart_client;
    DISPATCH dispatch_client;

    clear_buffer();
    dispatch_msg_st msg_pwm;
    uint8_t msg1[] = {0xA5, 0x5A, 0x10, 0x10, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0x0};
    for(uint8_t i = 0; i<sizeof(msg1);i++)
    {
        uart_client.receive_byte(msg1[i]);
        dispatch_client.parser(&msg_pwm);
    }
    // dispatch_client.parser(&msg_pwm);

    printf("%d\n", msg_pwm.pckt_length);
    printf("%d\n", msg_pwm.args_length);
    printf("%x\n", msg_pwm.cmd);
    printf("%x\n", msg_pwm.crc_received);
    printf("%x\n", msg_pwm.crc_calculated);

    // clear_buffer();
    // dispatch_msg_st msg_adc;
    // uint8_t msg2[] = {0xA5, 0x5A, 0x8, 0x11, 0x22, 0x33, 0x0, 0x0};
    // for(uint8_t i = 0; i<sizeof(msg2);i++)
    // {
    //     uart_client.receive_byte(msg2[i]);
    //     dispatch_client.parser(&msg_adc);
    // }
    // dispatch_client.parser(&msg_adc);

    // printf("%d\n", msg_adc.pckt_length);
    // printf("%d\n", msg_adc.args_length);
    // printf("%x\n", msg_adc.crc_received);
    // printf("%x\n", msg_adc.crc_calculated);

    // dispatch_msg_st msg_can;
    // uint8_t msg3[] = {0xA5, 0x5A, 0x8, 0xAA, 0xBB, 0xCC, 0x0, 0x0};
    // for(uint8_t i = 0; i<sizeof(msg3);i++)
    // {
    //     uart_client.receive_byte(msg3[i]);
    //     dispatch_client.parser(&msg_can);
    // }
    // dispatch_client.parser(&msg_can);

    // printf("%d\n", msg_can.pckt_length);
    // printf("%d\n", msg_can.args_length);
    // printf("%x\n", msg_can.crc_received);
    // printf("%x\n", msg_can.crc_calculated);

    print_buffer();

    return 0;
}

#include "uart.h"
#include "buffer.h"

UART::UART()
{
    printf("UART Test harness\n");
}

void UART::receive_byte(uint8_t byte)
{
    fifo_buffer[write_pointer] = byte;
    write_pointer = (write_pointer+1)%BUFFER_SIZE;
    // printf("Buff[%x]:%x\n", write_pointer, fifo_buffer[write_pointer]);
}

UART::~UART()
{
    printf("Tear down UART object\n");
}
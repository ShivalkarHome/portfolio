#ifndef _BUFFER_H_
#define _BUFFER_H_

#include <stdio.h>
#include <cstring>
#include <stdint.h>

#define BUFFER_SIZE             16

extern uint8_t new_byte;
extern uint8_t write_pointer;
extern uint8_t read_pointer;
extern uint8_t fifo_buffer[BUFFER_SIZE];

void print_buffer(void);
void clear_buffer(void);

#endif /* _BUFFER_H_ */

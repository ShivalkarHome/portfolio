#ifndef _DISPATCH_H_
#define _DISPATCH_H_

#include "msg.h"

class DISPATCH
{
private:
public:
    DISPATCH();
    void parser(dispatch_msg_st *dispatch_msg_rx);

    ~DISPATCH();
};


#endif /* _DISPATCH_H_ */

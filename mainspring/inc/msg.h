#ifndef _MSG_H_
#define _MSG_H_

#include <stdio.h>
#include <cstring>
#include <stdint.h>

#define SIZE_HEADER         0x2
#define SIZE_LENGTH         0x1
#define SIZE_CMD            0x1
#define SIZE_CRC            0x1
#define SIZE_METADATA       0x5
#define START_SEQ_CRC       0xFF

typedef struct
{
    bool is_valid;
    uint8_t pckt_length;
    uint8_t args_length;
    uint8_t cmd;
    uint8_t args[10];
    uint16_t crc_received;
    uint16_t crc_calculated;
}dispatch_msg_st;

#endif /* _MSG_H_ */

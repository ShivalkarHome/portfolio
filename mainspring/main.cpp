#include <thread>

#include "dispatch.h"
#include "uart.h"
#include "buffer.h"

int main()
{
    // Create pointer for class objects
    UART * uart_ptr = new UART();
    DISPATCH dispatch_client;

    // Test Case 1: 16 byte msg with valid header and valid crc
    clear_buffer(); // clean up
    printf("Test Case 1:\n");
    uint8_t msg1[] = {0xA5, 0x5A, 0xE, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0x0, 0xFA};
    std::thread uart_thread();
    for(uint8_t i=0;i<sizeof(msg1);i++)
    {
        std::thread uart_thread(&UART::receive_byte, uart_ptr, msg1[i]);
        uart_thread.join(); // Threads job is complete, termiante.

        dispatch_client.parser();
    }

    // Test Case 2: 16 byte msg with valid header and invalid crc
    clear_buffer(); // clean up
    printf("Test Case 2:\n");
    uint8_t msg2[] = {0xA5, 0x5A, 0xE, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0x0, 0x0};
    std::thread uart_thread();
    for(uint8_t i=0;i<sizeof(msg2);i++)
    {
        std::thread uart_thread(&UART::receive_byte, uart_ptr, msg2[i]);
        uart_thread.join(); // Threads job is complete, termiante.

        dispatch_client.parser();
    }

    // Test Case 3: 8 byte msg with valid header and invalid crc
    clear_buffer(); // clean up
    printf("Test Case 3:\n");
    uint8_t msg3[] = {0x5A, 0xA5, 0xE, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0x0, 0xFA};
    std::thread uart_thread();
    for(uint8_t i=0;i<sizeof(msg3);i++)
    {
        std::thread uart_thread(&UART::receive_byte, uart_ptr, msg3[i]);
        uart_thread.join(); // Threads job is complete, termiante.

        dispatch_client.parser();
    }

    // Test Case 4: 10 byte msg with valid header and invalid crc
    // In this case, dispatch will not parser the message from buffer because the buffer is not full.
    clear_buffer(); // clean up
    printf("Test Case 4:\n");
    uint8_t msg4[] = {0x5A, 0xA5, 0xE, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6};
    std::thread uart_thread();
    for(uint8_t i=0;i<sizeof(msg4);i++)
    {
        std::thread uart_thread(&UART::receive_byte, uart_ptr, msg4[i]);
        uart_thread.join(); // Threads job is complete, termiante.

        dispatch_client.parser();
    }

    // Test Case 5: 18 byte msg with valid header and valid crc
    // In this case, dispatch will parser the message from buffer mark valid message invalid, cause the header gets overriden
    clear_buffer(); // clean up
    printf("Test Case 5:\n");
    uint8_t msg5[] = {0x5A, 0xA5, 0xA, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0x0, 0xFD};
    std::thread uart_thread();
    for(uint8_t i=0;i<sizeof(msg5);i++)
    {
        std::thread uart_thread(&UART::receive_byte, uart_ptr, msg5[i]);
        uart_thread.join(); // Threads job is complete, termiante.

        dispatch_client.parser();
    }

    // free the memory of class object and delete the resp pointers
    delete uart_ptr;

    return 0;
}

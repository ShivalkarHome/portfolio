#include <stdio.h>
#include "buffer.h"

bool is_buffer_full = false;
uint8_t write_pointer = 0;
uint8_t fifo_buffer[BUFFER_SIZE] = {0};


void print_buffer(void)
{
    for(uint8_t i=0; i<BUFFER_SIZE; i++)
    {
        printf("Buff[%d]:%x\t", i, fifo_buffer[i]);
    }
    printf("\n");
}

void clear_buffer(void)
{
    memset(fifo_buffer, 0, BUFFER_SIZE);
}
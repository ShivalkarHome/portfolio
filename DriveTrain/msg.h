#ifndef _MSG_H_
#define _MSG_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

#define SIZE_HEADER         0x2
#define SIZE_LENGTH         0x1
#define SIZE_ARGS           0xA
#define SIZE_CRC            0x2

typedef struct
{
    bool is_valid;
    uint8_t pckt_length;
    uint8_t cmd;
    uint8_t args[SIZE_ARGS];
    uint16_t crc_received;
    uint16_t crc_calculated;
}dispatch_msg_st;

#endif /* _MSG_H_ */

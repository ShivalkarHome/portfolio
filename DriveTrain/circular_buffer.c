//==================================================================================================
//    I N C L U D E   F I L E S
//==================================================================================================
#include "circular_buffer.h"

//==================================================================================================
//    E X T E R N A L   V A R I A B L E S   &   F U N C T I O N S
//==================================================================================================

//==================================================================================================
//    P R I V A T E   C O N S T A N T S   &   M A C R O S
//==================================================================================================
#define PAYLOAD_SIZE 80
#define BUF_SIZE 10

//==================================================================================================
//    P R I V A T E   T Y P E   D E F I N I T I O N S
//==================================================================================================
typedef struct
{
    uint8_t buffer[BUF_SIZE][PAYLOAD_SIZE];
    uint32_t size;
    uint32_t head;
    uint32_t tail;
    uint32_t count;
} CircularBuffer;

//==================================================================================================
//    S T A T I C   V A R I A B L E S   D E F I N I T I O N S
//==================================================================================================

//==================================================================================================
//    F O R W A R D   F U N C T I O N   D E C L A R A T I O N S
//==================================================================================================

//==================================================================================================
//    P U B L I C   V A R I A B L E S   D E F I N I T I O N S
//==================================================================================================
CircularBuffer uart_tx_buf;

//==================================================================================================
//    P R I V A T E     F U N C T I O N S
//==================================================================================================

//==================================================================================================
//    P U B L I C     F U N C T I O N S
//==================================================================================================
void CircularBuffer_Init()
{
    memset(uart_tx_buf.buffer, 0, BUF_SIZE * PAYLOAD_SIZE);
    uart_tx_buf.size = BUF_SIZE;
    uart_tx_buf.head = 0;
    uart_tx_buf.tail = 0;
    uart_tx_buf.count = 0;
}

void CircularBuffer_Update()
{
}

void enqueue(uint8_t *usrData, uint8_t usrDataLen)
{
    if (uart_tx_buf.count == uart_tx_buf.size)
    {
        // Circular buffer is full. Overwriting oldest element.
        uart_tx_buf.head = (uart_tx_buf.head + 1) % uart_tx_buf.size;
    }
    else
    {
        uart_tx_buf.count++;
    }

    if (usrDataLen > PAYLOAD_SIZE)
    {
        usrDataLen = PAYLOAD_SIZE;
    }

    uart_tx_buf.tail = (uart_tx_buf.tail + 1) % uart_tx_buf.size;
    memcpy(uart_tx_buf.buffer[uart_tx_buf.tail], usrData, usrDataLen);
}

int8_t dequeue(uint8_t *usrData)
{
    if (uart_tx_buf.count == 0)
    {
        // Circular buffer is empty
        return -1;
    }

    uint8_t payloadLen = sizeof(uart_tx_buf.buffer[uart_tx_buf.tail]);

    memcpy(usrData, uart_tx_buf.buffer[uart_tx_buf.tail], payloadLen);
    uart_tx_buf.head = (uart_tx_buf.head + 1) % uart_tx_buf.size;
    uart_tx_buf.count--;

    return payloadLen;
}

bool bufferIsEmpty()
{
    if (uart_tx_buf.count == 0)
    {
        // Circular buffer is empty
        return true;
    }
    return false;
}

//==================================================================================================
//    E N D   O F   F I L E
//==================================================================================================

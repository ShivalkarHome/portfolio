#include "clock.h"
#include "timer.h"

void TIMER_Init(timer_st *ins, time_t time_period_ms)
{
    ins->active    = false;
    ins->repeating = false;
    ins->tripped = false;
    ins->start_time_ms = 0;
    ins->trigger_time_ms = time_period_ms;
}

void TIMER_start(timer_st *ins, timer_et timer_type)
{
    ins->active = true;
    ins->tripped = false;
    ins->repeating = false;

    ins->start_time_ms = CLOCK_Time__ms();

    if(timer_type == TIMER_REPEATING)
    {
        ins->repeating = true;
    }    
}

void TIMER_stop(timer_st *ins)
{
    ins->active = false;
    ins->repeating = false;
    ins->tripped = false;
    ins->start_time_ms = 0;
    ins->trigger_time_ms = 0;
}

bool TIMER_has_expired(timer_st *ins)
{
    ins->tripped = false;

    time_t current_time_ms = CLOCK_Time__ms();

    if (ins->active)
    {
        if ((current_time_ms - ins->start_time_ms) > ins->trigger_time_ms)
        {
            if (ins->repeating)
            {
                ins->start_time_ms = current_time_ms;
            }
            else
            {
                ins->active = false;
            }

            ins->tripped = true;
        }
    }

    return ins->tripped;
}

bool TIMER_is_active(timer_st *ins)
{
    return ins->active;
}

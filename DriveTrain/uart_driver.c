/**
 ******************************************************************************
 * @file    uart_driver.c
 * @brief   Provides interface for UART driver.
 ******************************************************************************
 */

//==================================================================================================
//    I N C L U D E   F I L E S
//==================================================================================================
#include <stdarg.h>
#include <string.h>

#include "main.h"
#include "msg.h"
#include "uart_driver.h"

//==================================================================================================
//    E X T E R N A L   V A R I A B L E S   &   F U N C T I O N S
//==================================================================================================
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_rx;

//==================================================================================================
//    P R I V A T E   C O N S T A N T S   &   M A C R O S
//==================================================================================================
#define DEBUG_PRINT 1
#define RX_BUF_SIZE 16
#define TX_PAYLOAD_SIZE 80
#define TX_BUF_SIZE 10

//==================================================================================================
//    P R I V A T E   T Y P E   D E F I N I T I O N S
//==================================================================================================

//==================================================================================================
//    S T A T I C   V A R I A B L E S   D E F I N I T I O N S
//==================================================================================================
static bool UART_initialized = false;
UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;

//==================================================================================================
//    F O R W A R D   F U N C T I O N   D E C L A R A T I O N S
//==================================================================================================
static void vprint(const char *fmt, va_list argp);
static void UART_DMA_util(void);

//==================================================================================================
//    P U B L I C   V A R I A B L E S   D E F I N I T I O N S
//==================================================================================================
int8_t rx_pointer = 0;
uint8_t rx_buffer[RX_BUF_SIZE] = {0};
int8_t write_pointer = 0;
uint8_t tx_buffer[TX_BUF_SIZE][TX_PAYLOAD_SIZE] = {{0}};

//==================================================================================================
//    P R I V A T E     F U N C T I O N S
//==================================================================================================
static void vprint(const char *fmt, va_list argp)
{
    char string[80] = {0};
    uint32_t num_char = vsprintf(string, fmt, argp); // build string
    HAL_UART_StateTypeDef uart_state = HAL_UART_STATE_READY;
    HAL_StatusTypeDef tx_status = HAL_OK;

    if (num_char > 0)
    {
        string[num_char++] = '\0';

        uart_state = HAL_UART_GetState(&huart2);

        if (uart_state == HAL_UART_STATE_READY)
        {
            tx_status = HAL_UART_Transmit_DMA(&huart2, (uint8_t *)string, num_char);
            HAL_Delay(3);
        }

        if (uart_state != HAL_UART_STATE_READY || tx_status != HAL_OK)
        {
            if (write_pointer < TX_BUF_SIZE)
            {
                memcpy((tx_buffer + write_pointer), string, num_char);
                write_pointer = (write_pointer + 1);
            }
        }
    }
}

static void UART_DMA_util(void)
{
    static uint8_t timeout = 5;
    HAL_UART_StateTypeDef uart_state = HAL_UART_STATE_READY;
    HAL_StatusTypeDef tx_status = HAL_OK;
    const char *str_prt = (const char *)(tx_buffer + write_pointer - 1);
    uint16_t str_len = strlen(str_prt);

    uart_state = HAL_UART_GetState(&huart2);

    if (uart_state == HAL_UART_STATE_READY)
    {
        tx_status = HAL_UART_Transmit_DMA(&huart2, (uint8_t *)str_prt, str_len);
        HAL_Delay(3);
    }
    else if (timeout == 0 || uart_state != HAL_UART_STATE_READY || tx_status != HAL_OK)
    {
        HAL_UART_AbortTransmit(&huart2);
    }
    else if (uart_state == HAL_UART_STATE_BUSY_TX || uart_state == HAL_UART_STATE_BUSY_TX_RX)
    {
        timeout--;
    }
    else if (tx_status == HAL_OK)
    {
        write_pointer--;
    }
}

void HAL_UART_MspInit(UART_HandleTypeDef *uartHandle)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};

    if (uartHandle->Instance == USART2)
    {
        /* USART2 clock enable */
        __HAL_RCC_USART2_CLK_ENABLE();

        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**USART2 GPIO Configuration
        PA2     ------> USART2_TX
        PA3     ------> USART2_RX
        */
        GPIO_InitStruct.Pin = USART_TX_Pin | USART_RX_Pin;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

        /* USART2 DMA Init */
        /* USART2_RX Init */
        hdma_usart2_rx.Instance = DMA1_Stream5;
        hdma_usart2_rx.Init.Channel = DMA_CHANNEL_4;
        hdma_usart2_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
        hdma_usart2_rx.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_usart2_rx.Init.MemInc = DMA_MINC_ENABLE;
        hdma_usart2_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_usart2_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
        hdma_usart2_rx.Init.Mode = DMA_CIRCULAR;
        hdma_usart2_rx.Init.Priority = DMA_PRIORITY_LOW;
        hdma_usart2_rx.Init.FIFOMode = DMA_FIFOMODE_ENABLE;
        hdma_usart2_rx.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
        hdma_usart2_rx.Init.MemBurst = DMA_MBURST_SINGLE;
        hdma_usart2_rx.Init.PeriphBurst = DMA_PBURST_SINGLE;
        if (HAL_DMA_Init(&hdma_usart2_rx) != HAL_OK)
        {
            Error_Handler();
        }

        __HAL_LINKDMA(uartHandle, hdmarx, hdma_usart2_rx);

        /* USART2_TX Init */
        hdma_usart2_tx.Instance = DMA1_Stream6;
        hdma_usart2_tx.Init.Channel = DMA_CHANNEL_4;
        hdma_usart2_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
        hdma_usart2_tx.Init.PeriphInc = DMA_PINC_DISABLE;
        hdma_usart2_tx.Init.MemInc = DMA_MINC_ENABLE;
        hdma_usart2_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
        hdma_usart2_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
        hdma_usart2_tx.Init.Mode = DMA_CIRCULAR;
        hdma_usart2_tx.Init.Priority = DMA_PRIORITY_LOW;
        hdma_usart2_tx.Init.FIFOMode = DMA_FIFOMODE_ENABLE;
        hdma_usart2_tx.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
        hdma_usart2_tx.Init.MemBurst = DMA_MBURST_SINGLE;
        hdma_usart2_tx.Init.PeriphBurst = DMA_PBURST_SINGLE;
        if (HAL_DMA_Init(&hdma_usart2_tx) != HAL_OK)
        {
            Error_Handler();
        }

        __HAL_LINKDMA(uartHandle, hdmatx, hdma_usart2_tx);

        /* USART2 interrupt Init */
        HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
        HAL_NVIC_EnableIRQ(USART2_IRQn);
    }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef *uartHandle)
{
    if (uartHandle->Instance == USART2)
    {
        /* Peripheral clock disable */
        __HAL_RCC_USART2_CLK_DISABLE();

        /**USART2 GPIO Configuration
        PA2     ------> USART2_TX
        PA3     ------> USART2_RX
        */
        HAL_GPIO_DeInit(GPIOA, USART_TX_Pin | USART_RX_Pin);

        /* USART2 DMA DeInit */
        HAL_DMA_DeInit(uartHandle->hdmarx);
        HAL_DMA_DeInit(uartHandle->hdmatx);

        /* USART2 interrupt Deinit */
        HAL_NVIC_DisableIRQ(USART2_IRQn);
    }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    HAL_UART_Receive_DMA(&huart2, rx_buffer, RX_BUF_SIZE);
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
    UART_DMA_util();
}

//==================================================================================================
//    P U B L I C     F U N C T I O N S
//==================================================================================================
void UART_Init(void)
{
    if (UART_initialized)
    {
        return;
    }

    huart2.Instance = USART2;
    huart2.Init.BaudRate = 921600;
    huart2.Init.WordLength = UART_WORDLENGTH_8B;
    huart2.Init.StopBits = UART_STOPBITS_1;
    huart2.Init.Parity = UART_PARITY_NONE;
    huart2.Init.Mode = UART_MODE_TX_RX;
    huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart2.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart2) != HAL_OK)
    {
        Error_Handler();
    }

    UART_initialized = true;
}

void UART_Update(void)
{
}

void UART_printf(const char *fmt, ...)
{
#if DEBUG_PRINT == 1
    va_list argp;
    va_start(argp, fmt);
    vprint(fmt, argp);
    va_end(argp);
#endif
}

//==================================================================================================
//    E N D   O F   F I L E
//==================================================================================================

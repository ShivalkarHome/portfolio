#ifndef _TIMER_H_
#define _TIMER_H_

#include <time.h>

#include "main.h"

//typedef uint32_t time_t;

typedef enum
{
    TIMER_ONESHOT,
    TIMER_REPEATING,

    TIMER_MAX,
}timer_et;

typedef struct
{
    bool   active;
    bool   repeating;
    bool   tripped;
    time_t start_time_ms;
    time_t trigger_time_ms;
}timer_st;


void TIMER_Init(timer_st *ins, time_t time_period_ms);
void TIMER_start(timer_st *ins, timer_et timer_type);
void TIMER_stop(timer_st *ins);
bool TIMER_has_expired(timer_st *ins);
bool TIMER_is_active(timer_st *ins);


#endif /* _TIMER_H_ */

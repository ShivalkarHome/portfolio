#ifndef _CLOCK_H_
#define _CLOCK_H_

#include "main.h"

extern TIM_HandleTypeDef htim2;

void CLOCK_Init(void);
void CLOCK_Start(void);
uint32_t CLOCK_Time__ms(void);
void CLOCK_Delay__us(uint32_t delay__us);

#endif /* _CLOCK_H_ */

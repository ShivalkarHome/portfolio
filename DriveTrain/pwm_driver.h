#ifndef _PWM_DRIVER_H_
#define _PWM_DRIVER_H_

#include "main.h"

typedef enum
{
    PWM_CHANNEL_NONE    = 0xFFFFFFFFU,
    PWM_CHANNEL_1       = 0x00000000U,
    PWM_CHANNEL_2       = 0x00000004U,
    PWM_CHANNEL_3       = 0x00000008U,
    PWM_CHANNEL_4       = 0x0000000CU,
    PWM_CHANNEL_ALL     = 0x0000003CU,
}pwm_channel_et;

extern TIM_HandleTypeDef htim1;

void PWM_Init(uint32_t time_period__ms);
void PWM_Update(void);

void PWM_set_duty_cycle(pwm_channel_et channel, uint8_t duty_cycle);
void PWM_channel_start(pwm_channel_et channel);
void PWM_channel_stop(pwm_channel_et channel);

#endif /* _PWM_DRIVER_H_ */

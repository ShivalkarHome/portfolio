#include "mtmcu_registers.h"

#include <stdbool.h>
#include <stdio.h> // for debug only

#define N_FACTOR            10 /* N factor is not specified in the problem statement*/
#define LONG_PRESS__s       3*1000

// pin connections
#define PIN_LED_RED         0
#define PIN_LED_GREEN       1
#define PIN_LED_BLUE        2
#define PIN_BUTTON          3

#define CLEAR_BIT(PR, N)    (PR | (1UL << N))
#define SET_BIT(PR, N)      (PR & ~(1UL << N))
#define IS_BIT_SET(PR, N)   ((PR >> N) & 1UL)

typedef enum
{
    PIN_LOW,
    PIN_HIGH,

    PIN_MAX, //MAX value possible
} pin_states_et;

typedef enum
{
    STATE_OFF,
    STATE_X,
    STATE_Y,

    STATE_MAX, //MAX value possible
} states_et;

typedef enum
{
    PRESS_NONE,
    PRESS_SHORT,
    PRESS_LONG,

    PRESS_MAX, //MAX value possible
} button_press_et;

typedef struct
{
    uint32_t trigger_time__ms;
    unsigned is_active : 1;
    unsigned has_tripped : 1;
} timer_st;

static states_et led_state_curr = STATE_OFF;
static states_et led_state_prev = STATE_OFF;
static uint8_t led_red_state = STATE_OFF;
static button_press_et button_press_state = PRESS_NONE;

pin_states_et led_red   = PIN_LOW;
pin_states_et led_green = PIN_LOW;
pin_states_et led_blue  = PIN_LOW;
uint32_t led_blue_period = 100;

timer_st pwm_red_led;
timer_st pwm_green_led;
timer_st pwm_blue_led;

timer_st button_press;

// This clock should theorically run for 24 days without any overflow
static uint32_t largeFreeRunningClock_ms = 0;

uint32_t TIME_ms__Clock(void);

void GPIO_init(void);
void GPIO_update(void);
void TIMER_init(void);
void TIMER_update(void);
void STATUS_init(void);
void STATUS_update(void);
void LED_init(void);
void LED_update(void);
void BUTTON_init(void);
void BUTTON_update(void);
void SW_TMR_init(timer_st *tmr, uint32_t time__ms);
void SW_TMR_update(timer_st * tmr);
bool SW_TMR_HasTripped(timer_st * tmr);

uint32_t TIME_ms__Clock(void)
{
    return largeFreeRunningClock_ms;
}

int main(void)
{
    // Disable interrupts to avoid undefined/unwanted behaviour
    // __disable_interrupts();

    // Initialize all the system peripherals
    GPIO_init();
    TIMER_init();
    STATUS_init();
    LED_init();    
    BUTTON_init();

    // Initialization is complete. Safe to enable interrupts
    // __enable_interrupts();

    while(1)
    {
        GPIO_update();
        TIMER_update();
        STATUS_update();
        LED_update();
        BUTTON_update();
    }

    // program should not reach here
    return 0;
}

void GPIO_init(void)
{
    /* Since the port is byte addresssable, directly assign values registers. */
    GPIO->DIR   = 0xFFF7; // P.3 is input, p.0-p.2 are used outputs while other unused outputs
    GPIO->ODR   = 0x8; // Initial state of P.3 should be high but low for other
    GPIO->PU    = 0x8; // Internal pull up for P.3, active low signal
    GPIO->PCIC  = 0x8; // Trigger interrupt for falling edge of switch on P.3
    GPIO->DF    = 0x0; // use as gpio only
    GPIO->PCF   = 0x8; // Interrupt on P.3, active low signal
}

void GPIO_update(void)
{
    // RED LED is connected to pin 0 
    if(led_red == PIN_LOW)
    {
        SET_BIT(GPIO->ODR, 0);
    }
    else if(led_red == PIN_HIGH)
    {
        CLEAR_BIT(GPIO->ODR, 0);
    }

    // GREEN LED is connected to pin 1
    if(led_green == PIN_LOW)
    {
        SET_BIT(GPIO->ODR, 1);
    }
    else if(led_green == PIN_HIGH)
    {
        CLEAR_BIT(GPIO->ODR, 1);
    }

    // BLUE LED is connected to pin 2
    if(led_blue == PIN_LOW)
    {
        SET_BIT(GPIO->ODR, 2);
    }
    else if(led_blue == PIN_HIGH)
    {
        CLEAR_BIT(GPIO->ODR, 2);
    }
}

// GPIO interrupt routine. Simply increments the timer.
#pragma interrupt (GPIO_IRQHandler (vect=_VECT(X_IRQ(GPIO->ISRA))))
void GPIO_IRQHandler(void)
{
    // Clear the interrupt flag
    SET_BIT(GPIO->PCF, PIN_BUTTON);

    // start timer to detect if long press or not
    SW_TMR_init(&button_press, LONG_PRESS__s);
}

void TIMER_init(void)
{
    largeFreeRunningClock_ms = 0;

    TIMER->STAT = TIMER_STAT_CPM | TIMER_STAT_OVF; // Clearing CPM and OVF bits
    TIMER->PSC  = 999; // (system clock / 1kz freq)-1, count pulse every 1 msec 
    TIMER->CPR  = 1000; // 1 Khz timer

    TIMER->CTRL = TIMER_CTRL_TMEN | TIMER_CTRL_CPMIE | TIMER_CTRL_OVFIE; // Enable the timer
}

void TIMER_update(void)
{

}

// Timer interrupt routine. Simply increments the timer.
#pragma interrupt (TIMER_IRQHandler (vect=_VECT(X_IRQ(TIMER->ISRA))))
void TIMER_IRQHandler(void)
{
    SET_BIT(TIMER->STAT, TIMER_STAT_OVF);
    SET_BIT(TIMER->STAT, TIMER_STAT_CPM);
    /*
     * Assumition, the clock does not have any drift
     * Each interrupt is triggered excatly at 1msec.
    */
    largeFreeRunningClock_ms++;
}

void STATUS_init(void)
{
    led_state_curr = STATE_OFF;
    led_state_prev = STATE_OFF;
}

void STATUS_update(void)
{
    switch(led_state_curr)
    {
        case STATE_OFF:
            if((button_press_state == PRESS_SHORT || button_press_state == PRESS_LONG)
            && (led_state_prev == STATE_X))
            {
                led_state_curr = STATE_X;
            }
            
            if((button_press_state == PRESS_SHORT || button_press_state == PRESS_LONG)
            && (led_state_prev == STATE_Y))
            {
                led_state_curr = STATE_Y;
            }

            led_state_prev = STATE_OFF;
            break;

        case STATE_X:
            if(button_press_state == PRESS_SHORT)
            {
                led_state_curr = STATE_Y;
            }

            if(button_press_state == PRESS_LONG)
            {
                led_state_curr = STATE_OFF;
            }

            led_state_prev = STATE_X;
            break;

        case STATE_Y:
            if(button_press_state == PRESS_SHORT)
            {
                led_state_curr = STATE_X;
            }
            
            if(button_press_state == PRESS_LONG)
            {
                led_state_curr = STATE_OFF;
            }
            
            led_state_prev = STATE_Y;
            break;

        case STATE_MAX:
            led_state_curr = STATE_OFF;
            led_state_prev = STATE_OFF;
            break;      
    }
}

void LED_init(void)
{
    SW_TMR_init(&pwm_red_led, 50);
    SW_TMR_init(&pwm_green_led, 53);
    SW_TMR_init(&pwm_blue_led, 100);
}

void LED_update(void)
{
    SW_TMR_update(&pwm_red_led);
    SW_TMR_update(&pwm_green_led);
    SW_TMR_update(&pwm_blue_led);

    if(SW_TMR_HasTripped(&pwm_red_led))
    {
        if(led_red == PIN_LOW)
        {
            led_red = PIN_HIGH;
        }
        else if(led_red == PIN_HIGH)
        {
            led_red = PIN_LOW;
        }

        SW_TMR_init(&pwm_red_led, 50);
    }

    if(SW_TMR_HasTripped(&pwm_green_led))
    {
        if(led_green == PIN_LOW)
        {
            SW_TMR_init(&pwm_green_led, 6);
            led_green = PIN_HIGH;
        }
        else if(led_green == PIN_HIGH)
        {
            SW_TMR_init(&pwm_green_led, 53);
            led_green = PIN_LOW;
        }
    }

    if(SW_TMR_HasTripped(&pwm_blue_led))
    {
        led_blue_period += 100;

        if(led_blue == PIN_LOW)
        {
            led_blue = PIN_HIGH;
        }
        else if(led_blue == PIN_HIGH)
        {
            led_blue = PIN_LOW;
        }

        SW_TMR_init(&pwm_blue_led, led_blue_period);
    }
    
    SW_TMR_HasTripped(&pwm_red_led);
}

void BUTTON_init(void)
{
    button_press_state = PRESS_NONE;
}

void BUTTON_update(void)
{
    SW_TMR_update(&button_press);

    /* 
     * If the pin is still high, long press is detected.
     * otherwise, short press is detected.
     */
    if(SW_TMR_HasTripped(&button_press))
    {
        if(IS_BIT_SET(GPIO->IDR, PIN_BUTTON))
        {
            button_press_state = PRESS_LONG;
        }
        else
        {
            button_press_state = PRESS_SHORT;
        }
    }
}

void SW_TMR_init(timer_st *tmr, uint32_t time__ms)
{
    tmr->trigger_time__ms = TIME_ms__Clock() + time__ms;

    tmr->is_active = 1;
    tmr->has_tripped = 0;
}

void SW_TMR_update(timer_st *tmr)
{
    if (tmr->is_active)
    {
        if((TIME_ms__Clock() - tmr->trigger_time__ms) >= 0)
        {
            tmr->is_active = 0;
            tmr->has_tripped = 1;
        }
    }
}

bool SW_TMR_HasTripped(timer_st * tmr)
{
    if (tmr->has_tripped)
    {
        tmr->has_tripped = 0;
        return 1;
    }
    return 0;
}
#include"list.h"

//Class Definition for Doubly Linked List
//Create Doubly Linked List
void Doubly_Linked_List::add_end(uint32_t value)
{
    node *ptr;
    node *temp;
    // Check if list exist or not.
    // If not, create List and add node
    if(head == NULL)
    {
        head = new node;
        head->data = value;
        head->prev = NULL;
        head->next = NULL;
    }
    // If yes, Append new node at the end
    else
    {
        ptr = head;
        while(ptr->next != NULL)
        {
            ptr = ptr->next;
        }
        //Add New Node at Last Position
        temp = new node;
        temp->data = value;
        temp->next = NULL;
        ptr->next = temp;
        ptr->next->prev = ptr;
        
        // Assign tail node to last node of the list
        tail = ptr->next;
        tail->prev = ptr;
        tail->data = value;
        tail->next = NULL;
    }
    // Once new node is added, increment size by 1
    size++;
    return;
}


//Display Elements of Linked List
void Doubly_Linked_List::display_forward(void)
{
    struct node *ptr;
    cout << "Forward Order" << endl;
    for(ptr=head; ptr!=NULL; ptr=ptr->next)
    {
        cout << ptr->data << " ";
    }
    cout << "\nLength = " << size << endl;
    return;
}


//Display Elements of Linked List in Reverse Order
void Doubly_Linked_List::display_reverse(void)
{
    struct node *ptr;
    cout << "Backward Order" << endl;
    for(ptr=tail; ptr!=NULL; ptr=ptr->prev)
    {
        cout << ptr->data << " ";
    }
    cout << "\nLength = " << size << endl;
    return;
}


//Sort the Element in Doubly Linked List
void Doubly_Linked_List::sort_ascending()
{
    node *ptr_i,*ptr_j;
    int temp_val;
    cout << "Sort List in Ascending Order" << endl;
    
    for(ptr_i=head;ptr_i!=NULL;ptr_i=ptr_i->next)
    {
        for(ptr_j=ptr_i;ptr_j!=NULL;ptr_j=ptr_j->next)
        {
            if(ptr_i->data > ptr_j->data)
            {
                temp_val = ptr_i->data;
                ptr_i->data = ptr_j->data;
                ptr_j->data = temp_val;
            }
        }
    }
    return;
}


// Called only after, the list is sorted
//Searching Element in Doubly Linked List
void Doubly_Linked_List::delete_duplicate()
{
    struct node *ptr = head;
    int val;
    while((ptr!=NULL) && (ptr->next!=NULL))
    {
        if(ptr->data == ptr->next->data)
        {
            val = ptr->next->data;
            delete_element(val);
        }
        ptr=ptr->next;
    }
    return;

}


// Delete Element from the DLL
// If A, B, C are 3 elements of the list and B is deleted.
// make A->next point to C and C->prev point to A
// Use delete operator to deallocate memory
// which previously contained B
void Doubly_Linked_List::delete_element(uint32_t value)
{
    struct node *temp, *ptr;
     /*first element deletion*/
    if (head->data == value)
    {
        temp = head;
        head = head->next;  
        head->prev = NULL;
        size--;
        delete temp;
        return;
    }
    ptr = head;
    while (ptr->next->next != NULL)
    {   
        /*Element deleted in between*/
        if (ptr->next->data == value)  
        {
            temp = ptr->next;
            ptr->next = temp->next;
            temp->next->prev = ptr;
            size--;
            delete temp;
            return;
        }
        ptr = ptr->next;
    }
     /*last element deleted*/
    if (ptr->next->data == value)    
    {     
        tail = ptr;
        temp = ptr->next;
        ptr->next = NULL;
        delete temp;
        size--;
        cout << tail->data << endl;
        return;
    }
    cout<<"Element "<<value<<" not found"<<endl;
}

void Doubly_Linked_List::reverse_list()
{
    struct node *temp, *ptr;
    temp = NULL;
    ptr = head;

    while (ptr != NULL)
    {
        temp = ptr->prev;
        ptr->prev = ptr->next;
        ptr->next = temp;
        ptr = ptr->prev;
    }

    if(temp != NULL)
    {
        head = temp->prev;
    }
}

void Doubly_Linked_List::get_mthIndex_value(uint32_t position)
{
    if (position > this->size)
    {
        cout << "Element not found." << endl;
        return;
    }
    struct node *ptr;
    ptr = tail;

    cout << "mth element from last is = ";

    for(uint32_t i = 1; i < position; i++)
    {
        ptr=ptr->prev;
    }
    cout << ptr->data << endl;
}

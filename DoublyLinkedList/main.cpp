#include"list.h"
#include <string>
#define max 10

int main(void)
{
    Doubly_Linked_List List;

    int A[] = {1,25,12,9,24,25,2,10,2,3};
    for(int i=0; i<max; i++)
    {
        List.add_end(A[i]);
    }

    List.sort_ascending();
    List.display_forward();
    List.display_reverse();

    List.get_mthIndex_value(4);

//    search_duplicate is called only after, the list is sorted
    List.delete_duplicate();
    List.display_forward();

    List.reverse_list();
    List.display_forward();

    return 0;
}

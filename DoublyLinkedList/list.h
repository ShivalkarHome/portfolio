#ifndef LIST_H_
#define LIST_H_

#include<iostream>
#include<stdint.h>
using namespace std;

//Class Declaration for Doubly Linked List
class Doubly_Linked_List
{
    private:
        //Structure Declaration for Doubly_Linked_List
        struct node
        {
            uint32_t     data;        // Store data
            struct node *prev;    // Pointer to previous node
            struct node *next;    // Pointer to next node
        };
        // Pointer to Head node and Tail Node of the DLL
        struct node *head;
        struct node *tail;
        // Store length of DLL
        uint32_t size;
    public:
        //Default Constructor for Doubly Linked List
        Doubly_Linked_List()
        {
            head = NULL;
            tail = NULL;
            size = 0;
        }

        //Method Declaration for Doubly_Linked_List
        void add_end(uint32_t val);
        void display_forward(void);
        void display_reverse(void);
        void sort_ascending(void);
        void delete_duplicate(void);
        void delete_element(uint32_t val);
        void reverse_list(void);
        void get_mthIndex_value(uint32_t position);
};

#endif /* LIST_H_ */

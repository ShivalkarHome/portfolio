#include <iostream>
#include <map>

typedef union {
  struct {
    uint32_t msg_id : 2;
    uint32_t pkt_len : 3;
    uint32_t seq_num : 3;
    uint32_t file_perm : 4;
    uint32_t file_name : 16;
    uint32_t err : 4;
  } bits;
  uint32_t raw;
} msg_pkt;

typedef struct {
  uint32_t msg_id;
  char filename[16];
  uint32_t permission;
  uint32_t counter;
} file_st;

void process_packets(uint32_t packet) {
  msg_pkt parsedPkt;
  parsedPkt.raw = packet;

  // printf("msg_id:%d\n", parsedPkt.bits.msg_id);
  // printf("pkt_len:%d\n", parsedPkt.bits.pkt_len);
  // printf("seq_num:%d\n", parsedPkt.bits.seq_num);
  // printf("file_perm:%d\n", parsedPkt.bits.file_perm);
  // printf("filename:%x\n", parsedPkt.bits.file_name);
  // printf("err:%d\n", parsedPkt.bits.err);
  // printf("\n\n\n");

  static file_st listFilenames[4] = {0};

  if (parsedPkt.bits.err != 0) {
    printf("Error detected in packet:%u\n", packet);
  }

  if (parsedPkt.bits.pkt_len != 0) {
    listFilenames[parsedPkt.bits.msg_id].msg_id = parsedPkt.bits.msg_id;
    listFilenames[parsedPkt.bits.msg_id].counter = parsedPkt.bits.pkt_len - 1;
    listFilenames[parsedPkt.bits.msg_id].permission = parsedPkt.bits.file_perm;
    listFilenames[parsedPkt.bits.msg_id].filename[0] =
        (parsedPkt.bits.file_name & 0xFF00) >> 8;
    listFilenames[parsedPkt.bits.msg_id].filename[1] =
        (parsedPkt.bits.file_name & 0x00FF);

  } else {
    uint32_t index = parsedPkt.bits.seq_num * 2;
    listFilenames[parsedPkt.bits.msg_id].filename[index] =
        (parsedPkt.bits.file_name & 0xFF00) >> 8;
    listFilenames[parsedPkt.bits.msg_id].filename[index + 1] =
        (parsedPkt.bits.file_name & 0x00FF);
    listFilenames[parsedPkt.bits.msg_id].counter--;
  }

  if (listFilenames[parsedPkt.bits.msg_id].counter == 0) {
    printf("Filename:");
    for (uint8_t i = 0; i < 16; i++) {
      printf("%c", listFilenames[parsedPkt.bits.msg_id].filename[i]);
    }
    printf("created with permission:%d\n",
           listFilenames[parsedPkt.bits.msg_id].permission);
  }
}

int main() {
  msg_pkt msg_pkt1;

  msg_pkt1.bits.msg_id = 1;
  msg_pkt1.bits.pkt_len = 4;
  msg_pkt1.bits.seq_num = 0;
  msg_pkt1.bits.file_perm = 0;
  msg_pkt1.bits.file_name = 0x7368;
  msg_pkt1.bits.err = 0;

  // printf("msg_pkt1:%d\n", msg_pkt1.raw);

  msg_pkt msg_pkt2;
  msg_pkt2.bits.msg_id = 1;
  msg_pkt2.bits.pkt_len = 0;
  msg_pkt2.bits.seq_num = 1;
  msg_pkt2.bits.file_perm = 0;
  msg_pkt2.bits.file_name = 0x7265;
  msg_pkt2.bits.err = 1;

  printf("msg_pkt2:%d\n", msg_pkt2.raw);

  msg_pkt msg_pkt3;
  msg_pkt3.bits.msg_id = 1;
  msg_pkt3.bits.pkt_len = 0;
  msg_pkt3.bits.seq_num = 2;
  msg_pkt3.bits.file_perm = 0;
  msg_pkt3.bits.file_name = 0x7961;
  msg_pkt3.bits.err = 0;

  // printf("msg_pkt3:%d\n", msg_pkt3.raw);

  msg_pkt msg_pkt4;
  msg_pkt4.bits.msg_id = 1;
  msg_pkt4.bits.pkt_len = 0;
  msg_pkt4.bits.seq_num = 3;
  msg_pkt4.bits.file_perm = 0;
  msg_pkt4.bits.file_name = 0x7320;
  msg_pkt4.bits.err = 0;

  // printf("msg_pkt4:%d\n", msg_pkt4.raw);

  uint32_t packets[] = {121012241, 119951393, 127275073, 120717409};

  for (auto x : packets) {
    process_packets((x));
  }

  uint32_t packets2[] = {119951393, 121012241, 127275073, 120717409};

  for (auto x : packets) {
    process_packets((x));
  }

  uint32_t packets3[] = {119951393, 388386849, 121012241, 127275073, 120717409};

  for (auto x : packets) {
    process_packets((x));
  }
}
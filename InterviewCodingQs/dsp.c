#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

uint32_t function(uint32_t N);
/*
a(0) = 0;
a(1) = 1;
a(n) = a(n-1) + a(n-2)
a(15)?
*/


uint32_t function(uint32_t N)
{
    if(N == 0)
    {
        return 0;
    }
    if(N == 1)
    {
        return 1;
    }

    bool flag = true;
    uint32_t num1 = 0;
    uint32_t num2 = 1;
    uint32_t next_num = 0;

    for(uint32_t i = 2;i<=N;i++)
    {
        next_num = num1 + num2;
        if(flag)
        {
            num1 = next_num;
            flag = false;
        }
        else // if(flag)
        {
            num2 = next_num;
            flag = true;
        }
        printf("temp:%d, %d, %d\n", num1, num2, next_num);
    }

    return next_num;
}

void main(void)
{
    printf("Factorial of 15:%d\n", function(15));
    printf("hope this works!\n");
}

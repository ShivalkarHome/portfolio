#include <stdio.h>
#include <stdint.h>

void print_array(uint8_t *arr_ptr, uint8_t len);

void print_array(uint8_t *arr_ptr, uint8_t len)
{
    for(uint8_t i=0;i<len;i++)
	{
	    printf("%d, ", arr_ptr[i]);
	}
	printf("\n");
}

int main() {
	//code
	uint8_t data[] = {1,0,2,0,3,0,4,0,5,0};
	uint8_t data_len = sizeof(data)/sizeof(data[0]);
	uint8_t zero_counter = 0;

	print_array(data, data_len);

	for(uint8_t i=0;i<data_len;i++)
	{
	    if(data[i] != 0)
	    {
	        uint8_t temp = data[i];
	        data[i] = data[zero_counter];
	        data[zero_counter] = temp;
	        zero_counter++;
	    }
        printf("%d,%d,%d,%d\n", i, zero_counter, data[i], data[zero_counter]);
	}

	print_array(data, data_len);

	return 0;
}
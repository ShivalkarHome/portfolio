#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <cstdlib>

#define BITSET(r, n) r |= (1 << n)
#define BITCLEAR(r, n) r &= ~(1 << n)

typedef struct
{
    uint8_t data;
    uint8_t LA:1;
    uint8_t LB:1;
    uint8_t UB4:1;
    uint8_t SA:1;
    uint8_t SB:1;
    uint8_t UB3:1;
    uint8_t UB2:1;
    uint8_t UB1:1;
}payload_t;

void foo(void);

void foo(void)
{
    unsigned int a = 6;
    int b = -20;
    (a+b > 6) ? puts(">6") : puts("<=6");
}

int main(void)
{
#if 0
    foo();
#endif
#if 0
    uint8_t val_u8 = 0;
    printf("%d\n", val_u8);
    BITSET(val_u8, 1<<5);
    printf("%d\n", val_u8);
    BITCLEAR(val_u8, 1<<5);
    printf("%d\n", val_u8);
#endif
#if 0
    payload_t payload = {0};
    payload.data = 0x55;
    payload.UB3 = 1;
    payload.LA = 1;

    uint16_t data_buf = 0;
    memcpy(&data_buf, &payload, 2);

    printf("raw: 0x%x\n", data_buf);
#endif
    return 0;
}

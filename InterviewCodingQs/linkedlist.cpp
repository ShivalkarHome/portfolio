#include <iostream>

class Node {
 public:
  uint32_t data;
  Node *next;

  Node(uint32_t uData) {
    data = uData;
    next = nullptr;
  }
};

class List {
 private:
  Node *head;
  Node *tail;
  uint32_t size = 0;

 public:
  List() {
    head = nullptr;
    tail = nullptr;
  }

  bool isEmpty() { return head == nullptr ? true : false; }

  uint32_t getSize() { return size; }

  void addNewElement(uint32_t uData) {
    Node *curr = new Node(uData);

    if (isEmpty()) {
      head = curr;
      tail = head;
    } else {
      tail->next = curr;
      tail = curr;
    }
    size++;
  }

  void removeFrontElement() {
    if (isEmpty()) {
      return;
    }

    Node *temp = head;
    head = head->next;

    if (head == nullptr) {
      head = tail;
    }

    delete temp;
    size--;
  }

  void removeElement(uint32_t uData) {
    if (isEmpty()) {
      return;
    }

    Node *temp = head;
    head = head->next;

    if (head == nullptr) {
      head = tail;
    }

    delete temp;
  }

  void printList() {
    Node *curr = head;

    while (curr != nullptr) {
      std::cout << curr->data << " ";
      curr = curr->next;
    }
    std::cout << "\n";
  }

  void reverseLL() {
    Node *curr = head;
    Node *prev = nullptr;
    Node *temp = nullptr;

    while (curr != nullptr) {
      // Store next
      temp = curr->next;
      if (curr->next == nullptr) {
        tail = temp;
      }

      // Reverse current nodes next pointer
      curr->next = prev;

      // Move pointers one position ahead
      prev = curr;
      curr = temp;
    }
    head = prev;
  }

  void rotateLL(uint32_t k) {
    if (k == 0 || k > size) {
      return;
    }

    tail->next = head;

    Node *curr = head;
    for (int i = 1; i < k; i++) {
      curr = curr->next;
    }

    // update (k+1) node to new head
    head = curr->next;

    curr->next = nullptr;
    tail = curr;
  }

  bool hasCycle() {
    if (head == NULL || head->next == NULL) {
      return false;
    }

    Node *slow = head;
    Node *fast = head->next;

    while (slow != NULL && fast != NULL) {
      if (slow == fast) {
        return true;
      }

      if (slow->next == NULL || fast->next == NULL ||
          fast->next->next == NULL) {
        return false;
      }
      slow = slow->next;
      fast = fast->next->next;
    }

    return false;
  }
};

int main() {
  List ll;
  ll.addNewElement(1);
  ll.addNewElement(2);
  ll.addNewElement(3);
  ll.addNewElement(4);
  std::cout << ll.getSize() << "\n";

  ll.removeFrontElement();
  std::cout << ll.getSize() << "\n";

  ll.addNewElement(5);
  ll.addNewElement(6);
  ll.addNewElement(7);
  std::cout << ll.getSize() << "\n";

  ll.printList();

  //   ll.reverseLL();
  ll.rotateLL(3);

  ll.printList();

  std::string result = ll.hasCycle() ? "Has Cycles" : "Does not have cycles";

  std::cout << result << "\n";

  return 0;
}
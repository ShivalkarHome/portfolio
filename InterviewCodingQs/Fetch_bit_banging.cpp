#include <cstdint>
#include <cstring>
#include <iostream>

// In this method, compile of Coderpad puts ctrl byte in upper 8bits and data in
// lower 8bits
typedef struct {
  uint8_t data;
  uint8_t ctrl;
} dac_payload_t;

/**
 * This represents a "fake" hardware clock
 */
class Clock {
 public:
  static void delay(unsigned nanoseconds) { time_ += nanoseconds; }

  static unsigned getTime() { return time_; }

 protected:
  static unsigned time_;
};

unsigned Clock::time_ = 0;

/**
 * @brief This simulates a HW GPIO pin
 */
template <char PORT, unsigned PIN>
class Gpio {
 public:
  static void high() {
    std::cout << Clock::getTime() << " : " << "PIN" << PORT << PIN << " high"
              << std::endl;
  }

  static void low() {
    std::cout << Clock::getTime() << " : " << "PIN" << PORT << PIN << " low"
              << std::endl;
  }
};

template <typename CHIP_SELECT_GPIO, typename SCLK_GPIO, typename DATA_GPIO>
class MAX522_DAC {
 public:
  void init() {
    // de-assert chipselect
    CHIP_SELECT_GPIO::high();

    // clock stays low when SPI bus is idle
    SCLK_GPIO::low();

    // Have data line stay low when idle
    DATA_GPIO::low();
  }

  void set(char channel, uint8_t value) {
    dac_payload_t dac_payload;
    dac_payload.data = value;

    if (channel == 'A') {
      // According to data-sheet, UB3 and LA bits shoudld be set to select
      // channel A
      dac_payload.ctrl = 0x21;
    } else if (channel == 'B') {
      // According to data-sheet, UB3 and LB bits shoudld be set to select
      // channel B
      dac_payload.ctrl = 0x22;
    } else  // both channels are selected
    {
      // According to data-sheet, UB3, LA and LB bits shoudld be set to select
      // both channels
      dac_payload.ctrl = 0x23;
    }

    uint16_t dac_payload_raw = 0;
    memcpy(&dac_payload_raw, &dac_payload, 2);

    // assert chipselect
    CHIP_SELECT_GPIO::low();

    // Wait for setup-time
    Clock::delay(t_CSS);

    //*******************************************************************
    // TODO : change code in this function to bit-bang data to MAX522 DAC
    // Currently loop only drives clock, you also need to bit-bang data line
    // look at the data-sheet for protocol and timing requirements
    // the goal of set() is to set the output of a particular DAC channel
    //
    // WARNING : Attention to detail is important in this question.
    //           Read the data-sheet, consider signal timing.
    //           Answers that don't meet a minimum standard will not be
    //           considered.
    //*******************************************************************
    // payload to DAC is 16 bits, not 8 bits
    for (unsigned ii = 0; ii < 16; ++ii) {
      // debug, get bit ordering of payload
      // printf("raw:%x\n", dac_payload_raw);

      if (dac_payload_raw & 0x8000) {
        DATA_GPIO::high();
      } else {
        DATA_GPIO::low();
      }

      // Data/Signal setup before rising edge of clock
      Clock::delay(t_DS);

      // According to data-sheet, DAC supports upto 5MHz clock frequency.
      // Increasing the delay from 100nsecs to 200nsecs, ensures that firmware
      // is able to match the DAC's freq without over running the DAC.
      SCLK_GPIO::high();
      Clock::delay(t_CH);
      Clock::delay(t_CH);

      // delay of 150nsec after setting clock low and delay of 50nsec before
      // rising edge ensures 200nsec interval of low state.
      SCLK_GPIO::low();
      Clock::delay(t_CL);
      Clock::delay(t_DS);

      // shift 1 bit to left
      dac_payload_raw <<= 1;
    }

    // Wait for setup-time before de-asserting chip-select
    Clock::delay(t_CSH);

    CHIP_SELECT_GPIO::high();

    Clock::delay(t_CSPWH);
  }

  // SCLK fall to CS rise setup time in nanoseconds
  static const unsigned t_CSS = 150;

  // SCLK rise to CS rise setup time in nanoseconds
  static const unsigned t_CSH = 150;

  // SLCK pulse width low in nanoseconds
  static const unsigned t_CL = 100;

  // SLCK pulse width high in nanoseconds
  static const unsigned t_CH = 100;

  // CS pulse width high in nanoseconds
  static const unsigned t_CSPWH = 200;

  // DIN to SCLK rise hold time
  static const unsigned t_DS = 50;
};

int main() {
  typedef Gpio<'B', 0> PinB0;
  typedef Gpio<'B', 7> PinB7;
  typedef Gpio<'A', 3> PinA3;

  MAX522_DAC<PinB0, PinB7, PinA3> dac;

  // Simple use case for DAC to set output channels A and B to specific values
  dac.init();
  dac.set('A', 0x82);
  dac.set('B', 0xA5);

  return 0;
}

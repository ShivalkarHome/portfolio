#include <stdio.h>
#include <stdint.h>

uint32_t fibo(uint32_t num);

uint32_t fibo(uint32_t num)
{
    uint32_t num1 = 0;
    uint32_t num2 = 1;
    bool val_updated = false;
    uint32_t index = 0;
    uint32_t n_val;

    while(index <= num)
    {
        n_val = num1 + num2;
        if(val_updated)
        {
            num1 = n_val;
            val_updated = false;
        }
        else
        {
            num2 = n_val;
            val_updated = true;
        }
        index++;
    }

    return n_val;
}

int main() {
	//code

    for(int i=0;i<33;i++)
    {
        printf("%d\n", fibo(i));
    }

	return 0;
}
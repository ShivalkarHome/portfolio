#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define NUM_SAMPLES     1024


int main(void)
{
    float sampling_time = (float) 1/2100;
    int16_t raw_signal[NUM_SAMPLES] = {0};
    float freqs[] = {47.0f, 97.0f, 149.0f, 199.0f, 251.0f, 293.0f, 349.0f, 397.0f};
    uint8_t j = 0;

    for (uint16_t i = 0; i < NUM_SAMPLES; i++)
    {
        j = i/128;
        float theta = 2.0 * M_PI * freqs[j] * (float)i * sampling_time; 

        raw_signal[i] = (int16_t)(10 * sin(theta));

        printf("%d\n", raw_signal[i]);
    }

    return 0;
}
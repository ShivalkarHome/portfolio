
// Design a bank account with deposit, withdraw and check balance functionality

// Curve ball: Multiple users share this account
// Multiple people can be depositing, withdrawing or checking balance at same
// time (lets say from different ATMs)

#include <iostream>
#include <mutex>
#include <thread>

class BankAccount {
  int64_t balance;

  std::mutex control;

  int check_balance() { return balance; }

  void deposit(int amount) { balance += amount; }

  bool withdraw(int amount) {
    control.try_lock();

    if (amount > balance) {
      std::cout << "cant withdraw, insufficent balance\n";
      control.unlock();
      return false;
    }

    balance = balance - amount;
    control.unlock();
    return true;
  }
};

void perform_transactions(BankAccount &account, int id) {
  for (int i = 0; i < 5; ++i) {
    account.deposit(100.0 * id);
    std::this_thread::sleep_for(
        std::chrono::milliseconds(100));  // Simulate some delay
    account.withdraw(50.0 * id);
    std::this_thread::sleep_for(
        std::chrono::milliseconds(100));  // Simulate some delay
  }
}

int main() {
  BankAccount account(5000.0);  // Initial balance of 5000

  std::thread t1(perform_transactions, std::ref(account), 1);
  std::thread t2(perform_transactions, std::ref(account), 2);

  t1.join();
  t2.join();

  return 0;
}

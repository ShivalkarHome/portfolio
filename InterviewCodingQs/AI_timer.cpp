#include <chrono>
#include <functional>
#include <stdexcept>

constexpr uint8_t max_tasks = 10;

typedef struct {
  uint32_t task_id;
  void (*funcptr)();
  uint32_t period;
  uint32_t delay;
  uint32_t startTime;
} Task;

uint32_t counter = 0;

time_t get_current_time(void) { return counter; }

void Logger_task_cb() { printf("ts:[%u] Logger Task!\n", get_current_time()); }

void Uart_task_cb() { printf("ts:[%u] Uart Task!\n", get_current_time()); }

// Scheduler class
class Scheduler {
 private:
  uint8_t num_task = 0;
  Task TaskList[max_tasks];

 public:
  void scheduleTask() { num_task = 0; }

  void addTask(Task task) {
    if (num_task >= max_tasks) {
      throw std::runtime_error(
          "Error: Task List full, cannot add more tasks!!!");
    }

    task.startTime = task.period + task.delay;

    TaskList[num_task] = task;
    num_task = (num_task + 1) % max_tasks;
    printf("New task added:%x!\n", num_task);
  }

  void execute() {
    uint32_t current_ts = get_current_time();

    for (uint8_t i = 0; i < num_task; i++) {
      if (current_ts >= TaskList[i].startTime) {
        TaskList[i].funcptr();
        TaskList[i].startTime = current_ts + TaskList[i].period;
      }
    }
  }
};

int main() {
  Task Uart_task{
      .task_id = 1, .funcptr = Uart_task_cb, .period = 500, .delay = 0};
  Task Logger_task{
      .task_id = 0, .funcptr = Logger_task_cb, .period = 1000, .delay = 700};

  Scheduler TaskScheduler;

  TaskScheduler.addTask(Uart_task);
  TaskScheduler.addTask(Logger_task);

  for (uint32_t i = 0; i < 5000; i++) {
    TaskScheduler.execute();
    counter++;
  }
}

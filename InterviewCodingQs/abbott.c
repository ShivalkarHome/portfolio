// 0x4 , 2
// Assumption, position is zero based
uint32_t toggle_bit (uint32_t input_val, uint8_t bit_pos)
{
    uint32_t mask = 0x1 << bit_pos;         // mask = 4
    uint32_t bit_val = (input_val & mask) >> bit_pos;

    if (bit_val == 0x1)
    {
        //clear bit
        return (input_val & ~mask);
    }
    else
    {
        //set bit
        return (input_val ^ mask);
    }
}


int a = 4660 -> 0x1234

02%x

4660 / 16 = 291, 4
291 / 16 = 18, 3
18 / 16 = 1, 2
1 / 16 = 0, 1

void convert_int_asci_hex(uint32_t input_val, uint8_t* ascii_hex)
{
    uint32_t temp = input_val;
    uint8_t k = 7;
    uint32_t local_ascii_hex[8] = {0};

    while (temp > 0)
    {
        uint8_t rem = temp % 16;
        temp = temp / 16;
        local_ascii_hex[k] = rem + '0';
        // print
        k--;
    }

    memcpy(local_ascii_hex, ascii_hex, 8);
}

single linked list
each node has int 32 bit

print it in reverse order?

main.c

typedef struct
{
    int data;
    node_t* next;
}node_t

node_t* head;

node_t* sll_create(int data); // -> create head node
void add_node (node_t* head, int data); // add new node to the end
node_t* add_node_to_front (node_t* head, int data); // add new node to front and return new head
void print_sll(node_t * head);

void sll_reverse (node_t* head)
{
    // create another
    node_t* new_head;

    while (head->next != null)
    {
        new_head = add_node_to_front (node_t* new_head, int head->data);
        head = head->next;
    }

    // print
}

[1:39 PM] Zhang, Nelson

uint32_t * A = (uint32_t*) 0x4 ;

A += 2 ;


static int a = 0 ;

static int a ;
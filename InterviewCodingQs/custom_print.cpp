#include <stdio.h>
#include <stdint.h>
#include <math.h>

#define NEW_LINE_FEED         0xA
#define CARRIAGE_RETURN       0xD
#define NEGATIVE_SIZE         0x2D

static uint8_t num_digit(int8_t p_char);
static void newline(void);
void print_to_console(int8_t p_char);


static uint8_t num_digit(int8_t p_char)
{
    if (p_char <= -100 | p_char >= 100)
        return 3;
    else if (p_char <= -10 | p_char >= 10)
        return 2;
    else
        return 1;
}


void print_to_console(int8_t p_char)
{
    FILE *fp;
    fp = stdout;
    int8_t p_char_copy = 0;

    int8_t div_op = 0;
    int8_t mod_op = 0;
    
    if (p_char < 0)
    {
        p_char_copy = p_char * (-1);
        putc(NEGATIVE_SIZE, fp);
    }
    else
    {
        p_char_copy = p_char;
    }

    uint8_t num_of_digits = num_digit(p_char);

    for (uint8_t i = num_of_digits; i > 0; i--)
    {
        double power = pow(10, (i - 1));
        div_op = p_char_copy / (int8_t)power;
        mod_op = p_char_copy % (int8_t)power;

        if (mod_op != p_char_copy)
        {
            putc(div_op + (int8_t)0x30, fp);
        }

        p_char_copy = mod_op;
    }
}

uint8_t ASCII_NibbleToASCII(uint8_t nibble)
{
    /* Make sure it's actually a nibble */
    nibble &= 0x0F;
    
    /* Check if 0-9 */
    if(nibble <= 0x09)
    {
        nibble += '0';
    }
    else if(nibble >= 0x0A && nibble <= 0x0F)
    {
        nibble += '7';
    }
    else
    {
        /* Should never occur, but is invalid so do nothing */
        return 0;
    }

    return nibble;
}

static void print_nibble(uint8_t val)
{
    FILE *fp;
    fp = stdout;

    putc(ASCII_NibbleToASCII(val & 0x0F), fp);
}

static void print_U8(uint8_t p_char)
{
    printf("%x\n", p_char);
    print_nibble(p_char>>4);
    print_nibble(p_char>>0);
}



static void newline(void)
{
    FILE *fp;
    fp = stdout;
    putc(NEW_LINE_FEED, fp);
    putc(CARRIAGE_RETURN, fp);
}


int main(void)
{
    // print_to_console(1);
    // print_to_console(-1);
    // print_to_console(12);
    // print_to_console(-12);
    // print_to_console(123);
    // print_to_console(-123);

    int16_t num = 12345;
    print_U8((int8_t)(num >> 8));
    print_U8((int8_t)(num >> 0));
}
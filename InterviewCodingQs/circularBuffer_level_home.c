#include <stdio.h>
#include <stdint.h>

#include <stdbool.h>
/**A circular buffer is a Implement a FIFO circular buffer!
 *
 * Define a macro that specifies how many elements can fit inside the buffer
 * The buffer should store 8 characters and include an api to:
 *
 * Write a new element to the buffer
 * If the buffer is full then the newest character should overwrite the oldest character
 *
 * Read the oldest element and remove it from the buffer
 * If the buffer is empty then return ‘\0’
 * Test your solution
 */

/* Define any helper macros, functions, or structs */
#define BUF_SIZE 16
uint8_t buffer[BUF_SIZE] = {0};
uint8_t write_pointer = 0;
uint8_t read_pointer = 0;

//brute force
bool first_time = true;

/** The write function takes a single char and
 *  adds it to the end of the circular buffer.
 *  Return anything you want.
 */
void cBufWrite( char inChar )
{
    // Write implementation goes here
    buffer[write_pointer] = inChar;

    if(first_time && write_pointer == (BUF_SIZE-1))
    {
        first_time = false;
    }

    if(!first_time && (write_pointer == read_pointer))
    {
        read_pointer = (read_pointer+1)% BUF_SIZE;
    }
    write_pointer = (write_pointer+1)%BUF_SIZE;   // wrap around the buffer when buffer is full
}

/** The read function reads the oldest present
 *  char and puts it in outChar.
 *  Return anything you want
 */
char cBufRead( void )
{
    // Read implementation goes here
    char ch = buffer[read_pointer];

    // NULL check
    if( ch != '\0')
    {
        buffer[read_pointer] = '\0';
        read_pointer = (read_pointer+1)% BUF_SIZE;
    }
    return ch;
}


char printBuf( void )
{
    // Read implementation goes here
    for(uint8_t i=0;i<BUF_SIZE;i++)
    {
        printf("%d, ", buffer[i]);
    }
    printf("\n");

    printf("wrt_pt:%d\n", write_pointer);
    printf("read_pt:%d\n", read_pointer);
}

int main() {
    // Test your solution

    for(uint8_t i=0;i<5;i++)
    {
        cBufWrite(i+1);
    }

    for(uint8_t i=0;i<8;i++)
    {
        printf("read top of buf:%d\n", cBufRead());
    }

    for(uint8_t i=0;i<5;i++)
    {
        cBufWrite(i+1);
    }

    for(uint8_t i=0;i<8;i++)
    {
        printf("read top of buf:%d\n", cBufRead());
    }

    printBuf();
    return 0;
}

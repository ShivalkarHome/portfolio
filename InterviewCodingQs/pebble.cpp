#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

#include <iostream>
#include <vector>

using namespace std;

#define PORT 8080
#define BUFFER_SIZE 1024

const float T_SAMPL = 0.1;  // [s] sampling time
const float T_SIM = 3;      // [s] simulation time
const float K = 1;          // [-] proportional gain
const float T_I = 1;        // [s] integrator time constant
const float Y_REF = 0.5;    // [-] desired value for the controlled system

int setupUDPConnection(struct sockaddr_in& actuator_receiver_addr) {
  // sets up the UDP connection and returns the socket descriptor
  // use the protocol IPPROTO_UDP during socket creation, INADDR_ANY for the
  // receiver address and INADDR_LOOPBACK for the receiver address
  int socket_fd = 0;

  // Creating socket file descriptor
  if ((socket_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    perror("socket creation failed");
    exit(EXIT_FAILURE);
  }

  memset(&actuator_receiver_addr, 0, sizeof(actuator_receiver_addr));

  // Filling server information
  actuator_receiver_addr.sin_family = AF_INET;
  actuator_receiver_addr.sin_addr.s_addr = INADDR_ANY;
  actuator_receiver_addr.sin_port = htons(PORT);

  // Bind the socket with the server address
  if (bind(socket_fd, (const struct sockaddr*)&actuator_receiver_addr,
           sizeof(actuator_receiver_addr)) < 0) {
    perror("bind failed");
    exit(EXIT_FAILURE);
  }

  return socket_fd;
}

int transmitUDP(int socket_fd, struct sockaddr_in actuator_receiver_addr,
                uint32_t data_to_transmit, uint32_t* data_received) {
  // transmits the UDP packet, returns 0 on success

  socklen_t addr_len = sizeof(actuator_receiver_addr);

  // Receive and respond
  uint32_t len = recvfrom(socket_fd, data_received, sizeof(uint32_t), 0,
                          (struct sockaddr*)&actuator_receiver_addr, &addr_len);
  if (len < 0) {
    perror("Receive failed");
    break;
  }
  std::cout << "Received: " << data_received << "\n";

  // Send
  sendto(socket_fd, &data_to_transmit, strlen(uint32_t), 0,
         (struct sockaddr*)&actuator_receiver_addr, addr_len);

  close(socket_fd);
  return 0;
}

int encodeControl(float control_output, uint32_t* control_output_uint) {
  memcpy(control_output_uint, &control_output, sizeof(uint32_t));
  return 1;
}

int decodeControl(uint32_t control_output_uint, float* control_output) {
  memcpy(control_output, &control_output_uint, sizeof(uint32_t));
  return 1;
}

float getProcessNextState(float u, float x) {
  // returns the next of a first order low pass filter with input u and state x
  // x(t+1) = 0.9 * x(t) + 0.1 * u(t)

  return ((0.9 * x) + (0.1 * x));
}

float getControlOutput(float control_error, float control_state) {
  // returns the controller output for a PI controller
  return 1.0;
}

float getNextControlState(float control_error, float control_state) {
  // returns the controller state
  return 2.0;
}

vector<float> simulateClosedLoop(int socket_fd,
                                 struct sockaddr_in actuator_receiver_addr) {
  // simulate closed loop system with controller and process, using a sampling
  // time of 0.1s, over 3 seconds returns the process state vector

  vector<float> process_state(T_SIM / T_SAMPL, 0);

  return process_state;
}

int printProcessOutput(vector<float> out) {
  // print the simulated closed loop output values. Return 1 if successful.

  if (out.size() == 0) return 0;

  for (auto elem : out) {
    cout << elem << ", ";
  }
  cout << "." << endl;

  return 1;
}

int main() {
  vector<float> controlled_output;
  int socket_fd = 0;
  struct sockaddr_in actuator_receiver_addr;

  if ((socket_fd = setupUDPConnection(actuator_receiver_addr)) < 0) {
    perror("sendto failed");
    exit(EXIT_FAILURE);
  }

  controlled_output = simulateClosedLoop(socket_fd, actuator_receiver_addr);
  cout << "closed loop output: \n";

  if (!printProcessOutput(controlled_output))
    cerr << "Simulation didn't return any results." << endl;
}

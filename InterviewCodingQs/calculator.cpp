#include <iostream>
#include <stack>

using namespace std;

class Solution {
 public:
  int calculate(string s) {
    int64_t l1 = 0;
    int64_t l2 = 1;
    int64_t o1 = 1;
    int64_t o2 = 1;
    int64_t n = s.size();

    stack<int64_t> st;

    for (int64_t i = 0; i < n; i++) {
      char x = s[i];
      printf("x: %c\n", x);

      if (x >= '0' && x <= '9') {
        int64_t num = x - '0';
        while (i + 1 < n && s[i + 1] >= '0' && s[i + 1] <= '9') {
          i++;
          num = (num * 10) + (s[i] - '0');
        }
        l2 = (o2 == 1) ? l2 * num : l2 / num;
      } else if (x == '(') {
        printf("(: %ld, %ld, %ld, %ld\n", l1, o1, l2, o2);
        st.push(l1);
        st.push(o1);
        st.push(l2);
        st.push(o2);
        l1 = 0;
        o2 = 1;
        o1 = 1;
        l2 = 1;
      } else if (x == ')') {
        printf("): %ld, %ld, %ld, %ld\n", l1, o1, l2, o2);
        int64_t temp = l1 + o1 * l2;
        o2 = st.top();
        st.pop();
        l2 = st.top();
        st.pop();
        o1 = st.top();
        st.pop();
        l1 = st.top();
        st.pop();
        l2 = (o2 == 1) ? l2 * temp : l2 / temp;
      } else if (x == '*' || x == '/') {
        o2 = (x == '*') ? 1 : -1;
      } else if (x == '+' || x == '-') {
        if (x == '-' && (i == 0 || (i - 1 >= 0 && s[i - 1] == '('))) {
          o1 = -1;
          continue;
        }

        l1 += o1 * l2;
        o1 = (x == '+') ? 1 : -1;
        l2 = 1;
        o2 = 1;
      }
    }

    return l1 + o1 * l2;
  }
};

int main(void) {
  Solution ob;

  cout << (ob.calculate("(1+(4+5+2)-3)+(6+8)")) << endl;

  return 0;
}
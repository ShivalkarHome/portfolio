#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <unistd.h>

/* Status enum */
enum {
  ERROR = 0,
  NO_CHANGE = 1,
  VALID = 2,
};

/* Result from the vendor api */
typedef struct SensorReading {
  int status;
  float lux;
  uint64_t timestamp;
} SensorReading;

/* these functions are provided for you; no need to implement */
uint64_t get_timestamp();
struct SensorReading read_next_sample();

//store the last readout
volatile struct SensorReading ALS_last_reading;

// Part 1
void get_sensor_data(void)
{
    //updating the last value
    struct SensorReading temp = read_next_sample();

    if(VALID == temp.status)
    {
        ALS_last_reading.status = VALID;
        ALS_last_reading.lux = temp.lux;
        ALS_last_reading.timestamp = temp.timestamp;
    }

    printf("sample #: status: %d, lux: %f, ts: %lu\n",
            ALS_last_reading.status, ALS_last_reading.lux, ALS_last_reading.timestamp);
}

float get_most_recent_lux() {
    return ALS_last_reading.lux;
}

// Part 2
// return -2 if the input timestamp is in the future
// return -1 if the input timestamp is not represented in the history
float get_lux_at(uint64_t timestamp) {
  return -1.0;
}

int main() {
    uint64_t start = get_timestamp(); // don't move this from the start of main

      // Example behavior of read_next_sample, comment out when ready to start testing your code
    for (int i = 0; i < 10; i++) {
        struct SensorReading reading = read_next_sample();
        printf("sample #: %d, status: %d, lux: %f, ts: %lu\n",
            i, reading.status, reading.lux, reading.timestamp);
    }

    // tests for part 1
    get_sensor_data();
    usleep(999 * 1000);
    float lux = get_most_recent_lux();
    printf("lux [1] = %f, should be 3.94383\n", lux);

    get_sensor_data();
    usleep(500 * 1000);

    lux = get_most_recent_lux();
    printf("lux [2] = %f, should be 3.35223\n", lux);

    usleep(500 * 1000);

    lux = get_most_recent_lux();
    printf("lux [3] = %f, should be 5.5397\n", lux);

    // tests for part 2
    sleep(1);

  int test_points[5] = {0, 200 * 1000, 1600 * 1000, 2600 * 1000, 3100 * 1000};
    for (int i = 0; i < 5; i++) {
      uint64_t time_at = start + test_points[i];
      float lux = get_lux_at(time_at);
      printf("t: %i, lux: %f\n", i, lux);
    }

  return 0;
}

// nothing past here, don't bother scrolling down!

// read next sample -> called any interval ex 3 sec -> task 1, store last 100 readouts
// get_sensor_data()
// read last lux value () -> task 2 -> read last value or value from last 10 mins
// get_most_recent_lux()





































/* Don't edit this code */
float random_float() {
    return (float)rand() / RAND_MAX;
}

/* Don't edit this code */
uint64_t get_timestamp() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return 1000000 * tv.tv_sec + tv.tv_usec;
}

/* Don't edit this code */
struct SensorReading read_next_sample() {
  static bool first_run = true;
  struct SensorReading return_value;

  uint64_t max_wait = 1 * 1000000;

  if (random_float() < 0.0001) {
    return_value.status = ERROR;
    return return_value;
  }
  if (first_run) {
    first_run = false;
    return_value.lux = random_float() * 10;
    return_value.status = VALID;
    return_value.timestamp = get_timestamp();
    return return_value;
  }
  uint64_t next_value_change = 2 * 1000000 * random_float();
  if (max_wait <= next_value_change) {
    usleep(max_wait);
    return_value.status = NO_CHANGE;
    return return_value;
  }
  usleep(next_value_change);
  return_value.lux = random_float() * 10;
  return_value.status = VALID;
  return_value.timestamp = get_timestamp();
  return return_value;
}
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/*
 * This is a circular buffer that allows enqueue or dequeue arbitrary
 * amount of bytes. The buffer size is limited to BUF_SIZE.
 */

static char *GetErrorString(int x);

#define BUF_SIZE    4096

typedef struct {
	int write_index;
	int read_index;
	char *buf; /* should be renamed to more descriptive variable name such as 'buffer_1byte' indicating, each node will be 1 byte is size */
	int size;
} circular_buf_t;

circular_buf_t buf;	/* should be static and should be renamed to more descriptive variable name such as 'circular_buffer_obj' indicating its a object of circular buffer class/struct */

/*
 * Enqueue (size) bytes from (client_buf) to the local buf
 * memory of (client_buf) is allocated by the caller
 */
int enqueue(char *client_buf, int size)
{
	if (size > BUF_SIZE) {
		printf("%s", GetErrorString(3)); // GetErrorString function is called with incorrect error code, should be 1
		return 3;	// incorrect error code, should be 2
	}

	/* There is no clause for checking if input 'size' is less then the available memory. */

	int part1 = buf.size - buf.write_index;	/* variable part1 should be renamed to 'available_mem' indicating available memory bytes */

	memcpy(client_buf, buf.buf + buf.write_index, part1); /* client_buf is source ptr but is placed as destination ptr.
														  This statement indicates that part1 num of bytes of data is being coped from buf.buf to client_buf.
														  part1 could greater that input 'size' which is potential overwrite in memory allocated for client_buf */

	if (part1 != size) {
		/* should check if part1 is greater than size,
						 if yes, allow memcpy operation
						 if no, do not allow memcpy operation and erro return*/
		int part2 = size - part1; // this could result in negative number
		memcpy(client_buf + part1, buf.buf[0], part2);
		/* incorrect syntax, buf.buf is pointer (required for memcpy call,
													   buf.buf[0] is dereferencing the pointer and  is accessing the data at 0th location - making the syntax incorrect*/
		/* if part2 value is negative, it will be promoted to unsigned intergers which will be very large. That will result in run-time segmentation fault */

	}

	/* buf.write_index and buf.read_index are never updated in this function */

	/* There is no clause in this function which implements the circular behaviour of the buffer ie buf.write_index never resets to 0, when it reached 4096*/

    /* There is no clause in this function where buf.read_index is update to (buf.write_index + 1) so as to point to oldest value in the buffer. This should happen when buf.write_index wraps around from 4096 to 0 i.e. buffer full scenario. */
	return 0;
}

/*
 * Dequeue (size) bytes from the local buf to (client_buf),
 * (client_buf) is allocated by the caller. It is okay to assume
 * (client_buf) is big enough to hold all the (size) bytes
 */
int dequeue(char *client_buf, int size)
{
	if (size < BUF_SIZE) {
		/* This clause is throwing error for a valid a valid case ie no of bytes to deque is less than the size of the buffer */
		printf("%s", GetErrorString(2));
		return 2;
	}

	/* This code is reachable only is input 'size' is greater than BUF_SIZE which is a Invalid case. */

	int copy_size = buf.write_index - buf.read_index + 1; /* copy_size is calculating the num of bytes that can be dequed.
														  This variable should be renamed to something more descriptive such as 'used_memory'.*/
	memcpy(client_buf, buf.buf + buf.write_index, copy_size); /* Since, there is no check if copy_size is equal to input 'size'. This operation may deque more or may deque less num of bytes to be dequed.
															   But, this operation will deque all the data present in the circular buffer, which is not desired behaviour. */
    /* buf.read_index is never updated in this fucntion */
	return 0;
}

static char *GetErrorString(int x)
{
	char errorString[20];

	switch (x)
	{
	case 0:
		errorString = "Success -- No error."; /* incorrect syntax, errorString is a pointer, and cannot be assigned any data without dereferencing,
							 instead create a new string with error message and use memcpy to copy it over to errorString
							 or move the print here and change return from pointer to void type */
		break;
	case 2:
		errorString = "Overflow!"; /* incorrect syntax, errorString is a pointer, and cannot be assigned any data without dereferencing,
				    	      instead create a new string with error message and use memcpy to copy it over to errorString
					      or move the print here and change return from pointer to void type  */
		break;
	}

	errorString[19] = 0;	// Not a valid NULL terminator, should be represented as '\0'
	return errorString;
}


int main(int argc, char* argv[])
{
	// initialize buffer
	buf.buf = malloc(BUF_SIZE); //malloc return a void pointer and it is necessary to cast it to char as buf.buf is char type pointer
	buf.size = BUF_SIZE;

	// Perform enqueue() and dequeue();

    // All completed, return
	return 0;
}

#include <stdio.h>
#include <stdint.h>
#include <string.h>

static uint8_t num_digit(uint32_t p_char)
{
	if (p_char < 10)
		return 1;
	else if (p_char < 100)
	    return 2;
	else if (p_char < 1000)
		return 3;
	else if (p_char < 10000)
		return 4;
	else if (p_char < 100000)
		return 5;
	else if (p_char < 1000000)
		return 6;
	else if (p_char < 10000000)
		return 7;
	else if (p_char < 100000000)
		return 8;
	else if (p_char < 1000000000)
		return 9;
	else
		return 10;
}

uint32_t UART_print_S32(uint8_t *pData, int32_t num_copy)
{
	uint32_t factor = 1;
	uint32_t num = 0;
	uint8_t buffer[12] = {0};
	uint8_t count = 0;
	uint8_t count_copy = 0;

	if(num_copy < 0)
	{
		num = -1 * num_copy;
        buffer[0] = 0x2D;
        count = 1;
	}
	else if(num_copy == 0)
	{
		buffer[0] = 0x30;
		// count = 1;
	}
	else
	{
		num = num_copy;
		buffer[0] = 0x2B;
	}

	count += num_digit(num);
	count_copy = count;
	
    // printf("%d,", num);
    // printf("%d,", count);

	while(num != 0)
	{
		uint32_t rem = num % 10;
		num = num / 10;
		factor = factor*10;
		buffer[--count] = rem + 0x30;
	}
    
    // printf("%s\t", buffer);
    memcpy(pData, buffer, count_copy);
    
    return count_copy;
}

int main() {
	//code
	char data[100] = {0};
    uint8_t index = 0;
	uint32_t char_len = 0;
	
	char_len = UART_print_S32(data+index, -1);
	printf("%d\t", char_len);
	
    index = index + char_len + 1;
	char_len = UART_print_S32(data+index, 0);
	printf("%d\t", char_len);
	
    index = index + char_len + 1;
	char_len = UART_print_S32(data+index, 1);
	printf("%d\t", char_len);
	
    index = index + char_len + 1;
	char_len = UART_print_S32(data+index, 2147483647);
	printf("%d\t", char_len);
	
    index = index + char_len + 1;
	char_len = UART_print_S32(data+index, -2147483648);
	printf("%d\n", char_len);

    for(uint32_t i=0;i<30;i++)
    {
        if(data[i] == 0)
        {
            printf("\n");
        }
        else
        {
            printf("%c", data[i]);
        }
        
        
    }

	return 0;
}
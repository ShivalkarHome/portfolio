/*
Stock span
The span of the stock's price today is defined as the maximum number of
consecutive days (starting from today and going backwards) for which the price
of the stock was less than or equal to today's price. For example, if the price
of a stock over the next 7 days were [100, 80, 60, 70, 60, 75, 85], then the
stock spans would be [1, 1, 1, 2, 1, 4, 6].
*/

// iterator over the array
// choose a key -> 85, iterating over past entries, compare with past entries
// and update counter if lower.

#include <stdint.h>

#include <iostream>
#include <stack>

using namespace std;

int main(void) {
  uint32_t stock_prices[] = {100, 80, 60, 70, 60, 75, 85};
  uint32_t size_of_array = sizeof(stock_prices) / sizeof(stock_prices[0]);  // 7
  uint32_t count[size_of_array] = {0};  // size is 7

  std::stack<uint32_t> tracker;
  uint32_t count_stack[size_of_array] = {0};  // size is 7

  // Brute Force Method for price tracking
  for (uint32_t i = 0; i < size_of_array; i++)  // i => 0 -> 6
  {
    uint32_t key = stock_prices[i];  // key = 70
    count[i] = 1;
    for (uint32_t j = 0; j <= i; j++) {
      if (key > stock_prices[j])  // 70 <= 100, count++; 70 <=80; count++;
                                  // 70<=60; 70<=70 count++
      {
        count[i]++;
      }
    }
  }

  // Using Stack for price tracking
  // printf("%u, %d\n", tracker.top(), tracker.empty());
  tracker.push(0);
  count_stack[0] = 1;                           // first element in the list
  for (uint32_t i = 1; i < size_of_array; i++)  // i => 1 -> 6
  {
    // printf("%u, %u, %u, %d\n", i, stock_prices[i], tracker.top(),
    // tracker.empty());
    while (!tracker.empty() &&
           (stock_prices[tracker.top()] <= stock_prices[i])) {
      tracker.pop();
    }

    if (tracker.empty()) {
      count_stack[i] = i + 1;
      // printf("T:%u, %u\n", i, count_stack[i]);
    } else {
      count_stack[i] = i - tracker.top();
      // printf("F:%u, %u\n", i, count_stack[i]);
    }

    tracker.push(i);
  }
  // Print stock prices per day
  for (uint32_t i = 0; i < size_of_array; i++)  // i => 0 -> 6
  {
    printf("%u\t", stock_prices[i]);
  }
  printf("\n");
  // Print stock span per day using Brute Force
  for (uint32_t i = 0; i < size_of_array; i++)  // i => 0 -> 6
  {
    printf("%u\t", count[i]);
  }
  printf("\n");
  // Print stock span per day using Stack DS
  for (uint32_t i = 0; i < size_of_array; i++)  // i => 0 -> 6
  {
    printf("%u\t", count_stack[i]);
  }
  printf("\n");
  return 0;
}

// i = 0, key = 100, count = 1
// i = 1, key = 80, count = 1
// i = 2, key = 60, count = 1
// i = 3, key = 70, count = 2
// i = 4, key = 60, count = 1
// i = 5, key = 75, count = 4
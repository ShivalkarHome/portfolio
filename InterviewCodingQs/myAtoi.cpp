#include <iostream>

int32_t string_to_integer(std::string s) {
  int32_t result = 0;
  bool isNegative = false;
  bool isSpecialSymbol = false;
  std::cout << s << "\n";

  for (uint32_t i = 0; i < s.length(); i++) {
    // std::cout << i << " " << s[i] << "\n";
    if (s[i] == ' ') {
      isSpecialSymbol = true;
    } else if (s[i] == '-') {
      isNegative = true;
    } else if (s[i] >= '0' && s[i] <= '9') {
      result = (result * 10) + (s[i] - '0');
      //   std::cout << result << "\n";
    }
  }

  result = isNegative ? (result * -1) : result;
  return result;
}

int main() {
  std::string str = "  -000123";

  std::cout << string_to_integer(str) << "\n";

  return 0;
}
#include <iostream>
#include <string.h>
#include <stdint.h>

using namespace std;
#define DEBUG

// Complete the isValid function below.
string isValid(string s)
{
	string return_value = "YES";
	uint32_t frequency[26] = { 0 };
	uint32_t first_index = 0;
	uint32_t mismatch_index = 0xFF;
	uint32_t num_mismatch_index = 0;
	bool first_index_found = false;

	for (std::string::iterator it = s.begin(); it != s.end(); ++it)
	{
		frequency[*it - 'a']++;

		if (!first_index_found)
		{
			first_index = *it - 'a';
			first_index_found = true;
		}
	}

	for (uint8_t i = 0; i < 26; i++)
	{
		if (frequency[i] != 0 && i != first_index)
		{
			if (frequency[first_index] != frequency[i])
			{
				mismatch_index = i;
				num_mismatch_index++;
			}
		}
	}

	if (num_mismatch_index > 1)
	{
		return_value = "NO";
	}
	else if (mismatch_index != 0xFF && frequency[mismatch_index] != 1)
	{
		if (frequency[mismatch_index] - frequency[first_index] > 1)
		{
			return_value = "NO";
		}
	}

#ifdef DEBUG
	cout << first_index << "\t" << mismatch_index << '\t' << num_mismatch_index << '\n';
	cout << frequency[mismatch_index] << "\t" << frequency[first_index] << '\n';
	for (uint8_t i = 0; i < 26; i++)
	{
		cout << frequency[i] << ", ";
	}
	cout << "\n";
#endif /* DEBUG */
	return return_value;
}

int main()
{
	//ofstream fout(getenv("OUTPUT_PATH"));

	//string s;
	//getline(cin, s);

	//string result = isValid(s);

	//fout << result << "\n";

	//fout.close();


	string s = "ibfdgaeadiaefgbhbdghhhbgdfgeiccbiehhfcggchgghadhdhagfbahhddgghbdehidbibaeaagaeeigffcebfbaieggabcfbiiedcabfihchdfabifahcbhagccbdfifhghcadfiadeeaheeddddiecaicbgigccageicehfdhdgafaddhffadigfhhcaedcedecafeacbdacgfgfeeibgaiffdehigebhhehiaahfidibccdcdagifgaihacihadecgifihbebffebdfbchbgigeccahgihbcbcaggebaaafgfedbfgagfediddghdgbgehhhifhgcedechahidcbchebheihaadbbbiaiccededchdagfhccfdefigfibifabeiaccghcegfbcghaefifbachebaacbhbfgfddeceababbacgffbagidebeadfihaefefegbghgddbbgddeehgfbhafbccidebgehifafgbghafacgfdccgifdcbbbidfifhdaibgigebigaedeaaiadegfefbhacgddhchgcbgcaeaieiegiffchbgbebgbehbbfcebciiagacaiechdigbgbghefcahgbhfibhedaeeiffebdiabcifgccdefabccdghehfibfiifdaicfedagahhdcbhbicdgibgcedieihcichadgchgbdcdagaihebbabhibcihicadgadfcihdheefbhffiageddhgahaidfdhhdbgciiaciegchiiebfbcbhaeagccfhbfhaddagnfieihghfbaggiffbbfbecgaiiidccdceadbbdfgigibgcgchafccdchgifdeieicbaididhfcfdedbhaadedfageigfdehgcdaecaebebebfcieaecfagfdieaefdiedbcadchabhebgehiidfcgahcdhcdhgchhiiheffiifeegcfdgbdeffhgeghdfhbfbifgidcafbfcd";
	/*string s = "abcdefghhgfedecba";*/

	string result = isValid(s);

	return 0;
}

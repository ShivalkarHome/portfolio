1.) Code snippet
Mutex A; // spelling-error => mutex A
Mutex B;  // spelling-error => mutex B

/*
Up through C++03, example was valid, but used a deprecated implicit conversion.
String literal should be treated as being of type char const *, since you can't
modify its contents (without causing undefined behavior). In C++11, the implicit
conversion that had been deprecated was officially removed, so code that depends
on it (like your first example) should no longer compile. String literals are
stored in a read-only part of memory
*/

/* The following are considered const char arrays. They are stored in read-only
 * section of memory. */
char kFirstString[13] = "First Value.";
char kSecondString[14] = "Second Value.";

/* argv is not passed as parameter, it cannot be used in the function directly.
    For the program to compile, the function prototype should be
    void Thread1(int argc, char *argv[]) */
void Thread1() {
  A.Lock();  // spelling-error => A.lock();
  B.Lock();  // spelling-error => B.lock();

  /* Since string literals like kFirstString cannot be modified,
  (without causing undefined behaviour),
  the following will cause segmentation fault. */
  strcpy(kFirstString, argv[1]);

  printf("%s\n", kFirstString);

  B.Unlock();  // spelling-error => B.unlock();
  A.Unlock();  // spelling-error => A.unlock();
}

void Thread2() {
  B.Lock();  // spelling-error => B.lock();
  A.Lock();  // spelling-error => A.lock();
  printf("%s\n", kSecondString);
  A.Unlock();  // spelling-error => A.unlock();
  B.Unlock();  // spelling-error => B.unlock();
}

/*
The program in its original form will not compile.
Once, the spelling errors are fixed the program will compile but will not
run/execute.

If line 'strcpy(kFirstString, argv[1]);' under THread1() function is
removed/commented-out, the program will compile and run. It will product the
following output

First Value.
Second Value.
*/

2.) ENOMEM indicates Out of memory.
This happens when kernel does not allocate the physical memory.
Rather simply reserve a range of virtual addresses for the process.

#define MEM_BYTE 1
#define MEM_TOTAL 12

void *myblock = NULL;
myblock = malloc(MEM_BYTE);
free(ptr);
myblock = malloc(MEM_SIZE);

3.)

#define ALERT_VAL 0xFEFE
uint16_t volatile* pReg = (uint16_t volatile*) 0xABCD1234;

void foobar() {
  /* Wait until the register value equals alert value*/
  while (*pReg != ALERT_VAL) {
    /* Do something */
  }
}

4.)

char* foobar(const char* s)
{
  int freq_a = 0;
  int size = strlen(s);
  char* x = malloc(size + 1);

  for (uint32_t i = 0; i < size; i++) {
    /* If char is 'a', copy over the next char
    thereby, effectively removing the char 'a' from new string */
    if (s[i] != 'a') {
      x[freq_a++] = s[i];
    }
  }

  /* Null terminate the new string */
  x[freq_a] = '\0';

  // debug prints
  // printf("%d\t", size);
  // printf("%d\t", freq_a);
  // printf("%d\t", strlen(x));
  // printf("%c\n", x[freq_a]);

  /* resize the memory allocated to new string,
  and return the unused memory */
  x = (char*)realloc(x, freq_a);

  return x;
}

5.)
/**
 * If the input bool flag is set/true/1, the function will reveres the input string
 *
 * @param const char* s - input string
 * @param bool b - bool flag
 * @return char* - dynamically allocated null-terminated C-string
 */
char* Function(const char* s, bool b)
{
  char* x = malloc(strlen(s) + 1);
  for (uint32_t i = 0; i < strlen(s); i++) {
    /* If the b is true/1, the following ternary condition with revere the
     * string
     */
    x[i] = b ? s[strlen(s) - 1 - i] : s[i];
  }

  return x;
}
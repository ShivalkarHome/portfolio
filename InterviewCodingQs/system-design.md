1. *Define the Problem Space:*
   - Understand problem and define scope.
   - Clarify functional and non-functional requirements.
   - State assumptions and decisions explicitly.

2. *Design the System at a High Level:*
   - Design APIs to define how clients access system resources.
   - Consider request parameters, response types, and communication between client and server.
   - Create a high-level design diagram to illustrate system architecture.

3. *Deep Dive into the Design:*
   - Examine system components and relationships in detail.
   - Consider non-functional requirements' impact on design choices.
   - Present different design options with pros and cons.

4. *Identify Bottlenecks and Scaling Opportunities:*
   - Assess system's ability to operate under various conditions and support growth.
   - Address single points of failure, data replication, global service support, and scalability.
   - Consider concepts like horizontal sharding, CDN, caching, rate limiting, and databases.

5. *Review and Wrap Up:*
   - Summarize major decisions with justifications and trade-offs.
   - Ensure design satisfies all requirements.
   - Identify directions for further improvement.
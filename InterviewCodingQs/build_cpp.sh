#!/bin/bash
g++ -Wall -std=c++11 $1 -o out.exe
if [ -f "out.exe" ]; then
	./out.exe
fi

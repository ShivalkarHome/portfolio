// Approximation of the number PI through the Viete's series

// https://en.wikipedia.org/wiki/Vi%C3%A8te%27s_formula#Interpretation_and_convergence

#include <stdio.h>
#include <stdlib.h>
#include <math.h>



int main()
{
    double n = 100;
    double f;
    double pi = 1;

    for(double i = n; i > 1; i--)
    {
        f = 2;
        for(double j = 1; j < i; j++)
        {
            f = 2 + sqrt(f);
        }
        f = sqrt(f);
        pi = pi * f / 2;
    }
    pi *= sqrt(2) / 2;
    pi = 2 / pi;

    printf("\nApproximated value of PI = %1.16lf\n", pi);
}
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <cstdlib>

#define MASK_L(P, L)    ((1 << (P+1))-1)
#define MASK_R(P, L)    ((1 << (P-L+1))-1)
#define MASK(P, L)      ((MASK_L(P, L) ^ MASK_R(P, L)))

uint8_t reg_value[] = {0x12, 0x34, 0x56, 0x78, 0x9A, 0xBC, 0xDE, 0xF0,};

typedef union {
    float fval;
    uint32_t i32val;
    struct {
        uint32_t fraction:23;
        uint32_t exponent:8;
        uint32_t sign:1;
    };
} data_ut;

typedef struct
{
    uint32_t header;
    uint16_t length;
    uint16_t cmd;
    uint32_t args;
    uint32_t crc;
}payload_t;

void parce_info(uint8_t *stream, uint8_t size, payload_t *payload);
uint16_t write_register(uint16_t *address, uint8_t value);
uint32_t revers_bits(uint32_t num);
uint32_t replace_0s_with_1s(uint32_t num);
uint32_t read_32bits(uint8_t *address);
void read_u32bits(uint32_t *reg_addr, uint32_t *buf_ptr);
bool is_num_pow_2(uint32_t num);
void pointer_example(void);


void parce_info(uint8_t *stream, uint8_t size, payload_t *payload)
{
    payload->header = stream[0] | (stream[1] << 8) | (stream[2] << 16) | (stream[3] << 24);
    payload->length = stream[4] | (stream[5] << 8);
    payload->cmd    = stream[6] | (stream[7] << 8);
    payload->args   = stream[8] | (stream[9] << 8) | (stream[10] << 16)| (stream[11] << 24);
    payload->crc    = stream[12] | (stream[13] << 8) | (stream[14] << 16)| (stream[15] << 24);
}

uint16_t write_register(uint16_t *address, uint8_t value)
{
  *(uint8_t *)address = value;

  return *address;
}

uint32_t revers_bits(uint32_t num)
{
    uint32_t result = 0;
    uint8_t num_bits = 31;

    while(num_bits--)
    {
        result = result | (num & 0x1);
        result = result << 1;
        num = num >> 1;
        // printf("%x\n", result);
    }

    return result;
}

uint32_t replace_0s_with_1s(uint32_t num)
{
    uint32_t result = 0;
    uint32_t factor = 1;

    while(num != 0)
    {
        uint32_t rem = num % 10;
        num = num / 10;
        if(rem == 0)
        {
            result = result + (1*factor);
        }
        else
        {
            result = result + (rem*factor);
        }
        factor = factor*10;
        printf("%d, %d\t", factor, result);
        printf("%d, %d\n", num, rem);
    }

    return result;
}

uint32_t read_32bits(uint8_t *address)
{
    return (address[0] | address[1]<<8 | address[2]<<16 | address[3]<<24);
}

void read_u32bits(uint32_t *reg_addr, uint32_t *buf_ptr)
{
    memcpy(buf_ptr, reg_addr, 4);
}

bool is_num_pow_2(uint32_t num)
{
    return ((num & (num-1)) == 0) ? true:false;
}

uint32_t NextPowerOf2(uint32_t n)
{
    n |= (n >> 16);
    n |= (n >> 8);
    n |= (n >> 4);
    n |= (n >> 2);
    n |= (n >> 1);

    return n;
}

void pointer_example(void)
{
    /*
    int *a; -> pointer without malloc or const assigned value uninitialized variable.
    *a = 50;
    (True behaviour is undefined) (on Win 10, caused segmentation fault)
    */

    int *a = (int *)malloc(sizeof(int));
    *a = 50;
    printf("*a:%d\n", *a);

    free(a);
}

int main(void)
{
#if 0
    char *ptr;
    if ((ptr = (char *)malloc(0)) == NULL)
    {
        puts("Got a null pointer");
    }
    else
    {
        puts("Got a valid pointer");
    }
#endif
#if 0
    data_ut fi;
    fi.fval = -65.6525;

    printf("%f\n", fi.fval);
    printf("%x,%x,0x%x\n", fi.sign, fi.exponent, fi.fraction);

    uint32_t intVal = *((uint32_t *) &fi.fval);
    printf("%x,%x,0x%x\n", (intVal>>31), ((intVal>>23)&0xFF), (intVal&0x7FFFFFFF));
#endif
#if 0
    uint16_t reg_address = 0x00;
    payload_t payload_ins;

    printf("%d\n", (write_register(&reg_address, 120)));
    printf("%d\n", reg_address);
#endif
#if 0
    uint8_t data[] = {0x53, 0x48, 0x52, 0x45, 0x00, 0x10, 0x00, 0x11, 0x00, 0x00, 0x00, 0x32, 0xFF, 0xFF, 0xFF, 0xFF};
    uint8_t msg_len = sizeof(data)/sizeof(data[0]);

    for(uint8_t i = 0; i < msg_len; i++)
    {
        printf("%d:%x\t", i, data[i]);
    }
    printf("\n");

    parce_info(data, msg_len, &payload_ins);

    printf("header: %x\t", payload_ins.header);
    printf("length: %hx\t", payload_ins.length);
    printf("cmd: %hx\t", payload_ins.cmd);
    printf("args: %x\t", payload_ins.args);
    printf("crc: %x\n", payload_ins.crc);
#endif
#if 0
    printf("%x, %x, %x\n", MASK_L(3,3), MASK_R(3,3), MASK(3,3));
    printf("%x, %x, %x\n", MASK_L(5,4), MASK_R(5,4), MASK(5,4));
    printf("%x, %x, %x\n", MASK_L(7,2), MASK_R(7,2), MASK(7,2));
#endif
#if 0
    uint32_t num = 0x1;
    printf("reverse:%x\t%x\n", num, revers_bits(num));
#endif
#if 0
    uint32_t num1 = 104508;
    printf("%d\n", num1);
    printf("%d\n", replace_0s_with_1s(num1));
#endif
#if 0
    uint64_t a = 0;
    uint32_t b = 0x11223344;
    uint32_t c = 0;
    uint32_t d[2] = {0};


    a = ((uint64_t)read_32bits(reg_value+4)) << 32 | ((uint64_t)read_32bits(reg_value));

    read_u32bits(&b, &c);

    read_u32bits(&b, &d[0]);
    read_u32bits(&b, &d[1]);

    printf("0x%llx\n", a);
    printf("0x%x\n", b);
    printf("0x%x\n", c);
    printf("0x%llx\n", d);
#endif


#if 0
    uint32_t val = 128;
    printf("%d:%d\n", val, is_num_pow_2(val));
    val = 243;
    printf("%d:%d\n", val, is_num_pow_2(val));
    val = 371;
    printf("%d:%d\n", val, is_num_pow_2(val));
#endif
#if 0
    clock_t t;
    t = clock();

    printf("%d\n", 100 ^ NextPowerOf2(100));
    printf("%d\n", 50  ^ NextPowerOf2(50));
    printf("%d\n", 30  ^ NextPowerOf2(30));
    printf("%d, %u\n", 1, 1^NextPowerOf2(1));
    printf("%d, %u\n", 0, 0^NextPowerOf2(0));
    printf("%d, %u\n", 1, 1^NextPowerOf2(0xFFFFFFFF));
    printf("%d, %u\n", 0, 0^NextPowerOf2(0xFFFFFFFF));

    t = clock() - t;
    double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds

    printf("fun() took %f seconds to execute \n", time_taken);
#endif
#if 0
/*
Up through C++03, example was valid, but used a deprecated implicit conversion.
String literal should be treated as being of type char const *, since you can't modify its contents (without causing undefined behavior).
In C++11, the implicit conversion that had been deprecated was officially removed,
so code that depends on it (like your first example) should no longer compile.
String literals are stored in a read-only part of memory
*/
    char *a = "Hello"; // treated as const
    char *b = "World"; // treated as const
/*
    a[2] = b[1]; // cause segmenatation fault, you can't change something that's immutable
*/
    printf("%s\n", a);
    printf("%s\n", b);
#endif
#if 0
    pointer_example();
#endif
#if 0
    void *ptr_v = malloc(sizeof(uint8_t)*4);

    uint8_t *ptr_u8 = (uint8_t*)ptr_v;
    printf("%x, %x, %x, %x\n", ptr_u8[0], ptr_u8[1], ptr_u8[2], ptr_u8[3]);
#endif
#if 0
    printf("%d\n", 10 ? 0 ? 5 : 1 : 12);
#endif
#if 0
    printf("File :%s\n", __FILE__ );
    printf("Line :%d\n", __LINE__ );
    printf("Date :%s\n", __DATE__ );
    printf("Time :%s\n", __TIME__ );
#endif
    return 0;
}

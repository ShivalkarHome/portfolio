#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

int32_t main()
{
    uint8_t size = 10;
    uint8_t count = 0;
    uint8_t *arr = (uint8_t *)malloc(size * size * sizeof(uint8_t));

    for (uint8_t i = 0; i <  size; i++)
    {
        for (uint8_t j = 0; j < size; j++)
        {
            *(arr + i*size + j) = ++count;
        }
    }

    for (uint8_t i = 0; i <  size; i++)
    {
        for (uint8_t j = 0; j < size; j++)
        {
            printf("%d\t", *(arr + i*size + j));
        }
        printf("\n");
    }

    free(arr);

    return 0;
}
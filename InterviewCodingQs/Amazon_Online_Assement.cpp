#include <stdint.h>
#include <stdio.h>

constexpr auto END_OF_FLASH = 1;
constexpr auto READ_BUF_SIZE = 8;

int read(uint8_t* buffer, uint32_t n_bytes, uint32_t offset) {
  /* Use read_8b(uint8_t* buf, uint8_t* mask) to implement this function*/

  printf("offset:%x\n", offset);

  int total_bytes_read = 0;
  uint8_t read_buf[READ_BUF_SIZE] = {0};
  uint8_t bit_mask = 0;
  uint32_t index = 0;

  while (n_bytes > 0) {
    // read 8 bytes at offset
    uint8_t result = read_8b(read_buf, &bit_mask);
    printf("result:%x, bit_mask:%x\n", result, bit_mask);
    // for (uint32_t num : read_buf){
    //     printf("num:%x,", num);
    // }

    // Check if app reached end of flash
    if (result == END_OF_FLASH) {
      break;
    }

    for (uint32_t i = 0; i < 8 && n_bytes > 0; ++i) {
      if (bit_mask & (1 << i)) {
        continue;
      }

      printf("offset:%x,", offset);
      if (offset > 0) {
        --offset;
        continue;
      }

      buffer[index++] = read_buf[i];
      ++total_bytes_read;
      --n_bytes;
      printf("total_bytes_read:%x,", total_bytes_read);
      printf("n_bytes:%x", n_bytes);
      printf("\n");
    }
  }

  return total_bytes_read;
}

int findPattern(const uint32_t numBytes, const uint8_t data[]) {
  // Implement your code here!

  // pattern to find
  const uint32_t pattern = 0xFE6B2840;

  // input validation
  if (data == nullptr || numBytes < 4) {
    return -2;
  }

  // prepare bit mask for sliding window comparision
  const uint32_t total_bits = numBytes * 8;
  uint64_t sliding_window = 0;

  for (uint32_t i = 0; i < total_bits; ++i) {
    sliding_window <<= 1;  // right shift

    uint32_t byte_index = i / 8;

    uint32_t bit_index = 7 - (i % 8);

    printf("i:%d, byte_index:%d, bit_index:%d\n", i, byte_index, bit_index);
    printf("set LSB:%d\n", data[byte_index] & (1 << bit_index));

    if (data[byte_index] & (1 << bit_index)) {
      sliding_window |= 1;
    }

    if (i >= 31) {
      if ((sliding_window & 0xFFFFFFFF) == pattern) {
        return (i - 31);
      }
    }
  }

  return -1;
}
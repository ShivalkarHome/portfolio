#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

uint8_t count_num_words(char *sample);

int main() {

    printf("word count = %d\n", count_num_words("hello world"));
    printf("word count = %d\n", count_num_words("h3llo world"));
    printf("word count = %d\n", count_num_words("   hello     world     "));
    printf("word count = %d\n", count_num_words("a posse ad esse"));

  return 0;
}

//Write a function that counts the words on a string

//"hello world" = 2            // num_spaces = 1, num_words = 2
//"H3llo world" = 1
//"   hello     world     "  = 2

//"Hello world!" = 1

// valid char is 'A' to 'Z', and 'a' to 'z'


// valid/invalid word are separated by space
// num_spaces = x
// num_words = y
// for loop for string -> str[i] = valid char, count, then hit space => O(n)
// substring hello,
// for the substring, iterate => O(n) => O(n^2)

uint8_t count_num_words(char * sample)
{
    uint8_t total_word_count = 0;
    bool    invalid_char = false;

    for (int i = 0; sample[i] != '\0'; i++)
    {
        if( (sample[i] >= 'A' && sample[i] <= 'Z')
        ||  (sample[i] >= 'a' && sample[i] <= 'z') )
        {
            if(sample[i+1] == ' ' || sample[i+1] == '\0')
            {
                if(!invalid_char)
                {
                    total_word_count++;
                }
                else
                {
                    invalid_char = false;
                }
            }
        }
        else if (sample[i] != ' ')
        {
            invalid_char = true;
        }
    }

    return total_word_count;
}

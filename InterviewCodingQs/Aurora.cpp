// Copy all data in src to dst, starting at the first aligned address within the
// dst buffer.
// @param dst - pointer to destination buffer
// @param src - pointer to src buffer
// @param dst_len - size of destination buffer
// @param src_len - size of source buffer
// @param alignment - number of bytes to align to. Valid alignment params are
// powers of 2
// @return pointer to the copied data. return nullptr if failed to copy

// Valid alignment params are powers of 2 -> 1, 2, 4, 8, 16 valid?
// dst = 1+, aligment = 1
// aligment = 2, dst = 31, start position = 32
#include <stdio.h>

#include <cstddef>
#include <cstdint>

void* memcpy_aligned(void* dst, const void* src, size_t dst_len, size_t src_len,
                     uint8_t alignment) {
  // your implementation

  // assume alignment param is valid power of 2
  // verify that dst_len is equal or greater than src_len
  // if (sp + data len > dst_len)
  // dst address does not overlap with src address
  // calculate start position from dst and alignment

  // dst= 1025, aligh = 2, start pos = 1026
  // start pos = dst + align - mod; // 1025 + 2 - 1 = 1026

  uint8_t* src_local = (uint8_t*)src;
  uint64_t offset = alignment - ((uint64_t)dst % alignment);
  uint64_t start_pos = (uint64_t)dst + alignment - ((uint64_t)dst % alignment);
  uint8_t* dst_local = (uint8_t*)start_pos;

  printf("offset=%ld\r\n", start_pos);
  // uint32_t offset = dst_len - ((uint64_t)dst_local % alignment);
  // uint64_t start_pos = dst_local + ((uint64_t)dst_local % alignment);

  // error case : Not enough memory
  // printf("offset=%d\r\n", offset);
  printf("dst_len + offset=%ld\r\n", (dst_len + offset));
  if ((dst_len + offset) > src_len) {
    return nullptr;
  }

  // brute force: copy 1 byte at time
  // todo: debug
  for (uint32_t i = 0; i < (dst_len); i++) {
    dst_local[i] = src_local[i];
  }
  // dst = 50, alignment = 4, 50/4 = 12, 2, num = quod*dc+ rem
  // start pos = 52;
  // dst len = 10; end add = 60
  // src_len = 10;

  return (void*)dst_local;
}

int main() {
  uint8_t dst[1024];
  uint8_t src[1024] = {1, 2, 3, 4};

  // happy path
  //  void* ptr1 = memcpy_aligned(dst, src, 1024, 1024, 1);
  void* ptr2 = memcpy_aligned(dst + 1, src, 1024, 1024, 2);
  // void* ptr3 = memcpy_aligned(dst, src, 1024, 1024, 1);
  if (ptr2) {
    printf("ptr2 not null\r\n");
  } else {
    printf("ptr null\r\n");
  }

  // if(ptr1)
  // {
  //   printf("pt1 not null\r\n");
  // }
  // else {
  //   printf("ptr null\r\n");
  // }
}
// C++ implementation to delete all
// the nodes from the doubly
// linked list that are greater than
// the specified value x
#include <bits/stdc++.h>

using namespace std;

// Node of the doubly linked list
struct Node
{
    int data;
    Node *next;
};

// function to insert a node at the beginning
// of the Doubly Linked List
void push(Node** head_ref, int new_data)
{
    // allocate node
    Node* new_node = (Node*)malloc(sizeof(struct Node));

    // put in the data
    new_node->data = new_data;

    // link the old list off the new node
    new_node->next = (*head_ref);

    // move the head to point to the new node
    (*head_ref) = new_node;
}

void deleteGreaterNodes(Node** head_ref, int x)
{
    Node* ptr = *head_ref;
    Node* temp;

    if(*head_ref == NULL)
    {
        return;
    }

    if((*head_ref)->data >= x)
    {
        *head_ref = ptr->next;
        free(ptr);
        ptr = *head_ref;
    }

    while (ptr != NULL)
    {
        while(ptr != NULL && ptr->data >= x)
        {
            temp = ptr;
            ptr = ptr->next;
        }

        if(ptr == NULL)
        {
            return;
        }

        temp->next = ptr->next;

        free(ptr);

        ptr = temp->next;
    }
}

// function to print nodes in a
// given doubly linked list
void printList(Node* head)
{
    while (head != NULL) {
        cout << head->data << " ";
        head = head->next;
    }
}

// Driver program to test above
int main()
{
    // start with the empty list
    Node* head = NULL;

    push(&head, 1);
    push(&head, 3);
    push(&head, 5);
    push(&head, 7);
    push(&head, 9);
    push(&head, 0);
    push(&head, 2);
    push(&head, 4);
    push(&head, 6);
    push(&head, 8);

    int x = 6;

    cout << "Original List: ";
    printList(head);

    cout << "threshold: " << x << endl;

    deleteGreaterNodes(&head, x);

    cout << "Modified List: ";
    printList(head);
}
#include <stdint.h>
#include <stdio.h>

void print_array(uint8_t *arr_ptr, uint8_t len);
void mem_copy(void *dest_ptr, void *src_ptr, uint32_t len);
void mem_copy_align(void *dest_ptr, void *src_ptr, uint32_t len);

void print_array(uint8_t *arr_ptr, uint8_t len) {
  for (uint8_t i = 0; i < len; i++) {
    printf("%d, ", arr_ptr[i]);
  }
  printf("\n");
}

void mem_copy(void *dest_ptr, void *src_ptr, uint32_t len) {
  uint8_t *dest = (uint8_t *)dest_ptr;
  uint8_t *src = (uint8_t *)src_ptr;

  for (uint32_t i = 0; i < len; i++) {
    dest[i] = src[i];
  }
}

void mem_copy_align(void *dest_ptr, void *src_ptr, uint32_t len) {
  uint8_t *dest = (uint8_t *)dest_ptr;
  uint8_t *src = (uint8_t *)src_ptr;

  while (len > 0) {
    if (*dest % 4 == 0) {
      dest[0] = src[0];
      dest[1] = src[1];
      dest[2] = src[2];
      dest[3] = src[3];
      dest = dest + 4;
      len = len - 4;
    } else if (*dest % 2 == 0) {
      dest[0] = src[0];
      dest[1] = src[1];
      dest = dest + 2;
      len = len - 2;
    } else {
      dest[0] = src[0];
      dest = dest + 1;
      len = len - 1;
    }
  }
}

int main(void) {
  // code
  uint8_t src_buff[] = {1, 0, 2, 0, 3, 0, 4, 0, 5, 0};
  uint8_t dst_buff[10];
  uint8_t dst_buff2[10];

  mem_copy_align(dst_buff, src_buff, 10);
  mem_copy((void *)dst_buff2, (void *)src_buff, 10);

  print_array(src_buff, 10);
  print_array(dst_buff, 10);
  print_array(dst_buff2, 10);

  return 0;
}
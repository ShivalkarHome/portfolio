#include <cstdint>
#include <cstdlib>
#include <iostream>
using namespace std;

class MovingAverage {
 private:
  int32_t *data;
  uint32_t ptr;
  int32_t sum = 0;
  uint32_t no_samples;
  uint32_t max_samples;

 public:
  MovingAverage(int num_samples) {
    // allocate new memory based num_samples

    data = (int32_t *)malloc(num_samples * sizeof(int32_t));
    wmemset(data, 0, num_samples * sizeof(int32_t));

    ptr = 0;
    no_samples = num_samples;
  }

  void add_sample(int32_t sample) {
    // runing sum
    //  sub data[ptr] from sum
    //  add sample to sum
    //  update data[ptr]
    sum = sum + sample;
    sum = sum - data[ptr];

    data[ptr] = sample;
    ptr = (ptr + 1) % no_samples;
  }

  int32_t get_average() {
    // iterate over the entire samples
    // take sum and divided
    return (sum / no_samples);
  }
};

int main() {
  MovingAverage MA(10);

  MA.add_sample(1);
  MA.add_sample(2);
  MA.add_sample(3);
  MA.add_sample(4);

  MA.get_average();

  return 0;
}

#include <iostream>
#include <stdint.h>
#include <string>
using namespace std;

string find_match_simple(string input, string pattern);

string find_match_simple(string input, string pattern)
{
    string ret_str;
    uint32_t len_input = input.length();
    uint32_t len_pattern = pattern.length();

    cout << "len:" << len_input << ", " << len_pattern << ",\t";

    // iterator over the input string
    for(uint32_t i = 0;i<len_input; i++)
    {
        uint32_t j = 0;
        // iterator over the pattern
        for(j = 0; j<len_pattern; j++)
        {
            if (input[i+j] != pattern[j] && (pattern[j] != '*' || pattern[j] != '.'))
            {
              break;
            }
        }
        if(j == len_pattern)
        {
            cout << "match found at: " << i << ",\t";
            ret_str = input.substr(i, len_pattern);
        }
    }

    return ret_str;
}

// To execute C++, please define "uint32_t main()"
int32_t main(void) {
  string text("abcdcef");
  string pattern("cd");

  cout << find_match_simple(text, pattern) << "\n";
  return 0;
}


/*
Your previous Plain Text content is preserved below:

1. Design a function that takes two string parameters:
  input
  pattern

The function should return the longest substring that matches provided pattern. Assume that the input and pattern characters are alphabetic.

  find_match("abcdcef", "cd") -> "cd"

Function prototype:
  string find_match(string input, string pattern);


2. Modify your function to support a don't care "." pattern, that matches any single character:

  find_match("abcdcef", "c.c") -> "cdc"


3. Modify your function to support "*" patterns. A "*" will match zero or more consecutive characters.

    find_match("abcdcef", a*c") -> "abcdc"


 */
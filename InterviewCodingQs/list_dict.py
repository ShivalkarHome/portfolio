#######################################################
# Python3 primer

# print function
print("hello world!")
print(1, "fish", 2, 'fish')

# lists : resizeable arrays of any type of object
empty_list1 = list()   # create an empty list
empty_list2 = []       # short form to create an empty list
list3 = [1, 2.0, 'c']  # create a list with 3 elements (all different types)
print(list3[1])        # print second element of list (lists indexes start from 0)
print(len(list3))      # len() gets number of elements in a list
list3.append('hello')  # append adds an element to the end of a list
print(list3)           # print everything in a list

# dictionaries : maps a value to a unique key
empty_dict1 = dict()    # create an empty dictionary
empty_dict2 = {}        # short form to create an empty dict
dict3 = {'a':1, 'b':2}  # create dict with 2 elements
print(dict3['b'])        # get a value for a specific key in dict
dict3['c'] = 3          # create a new key:value in dict
print(dict3)
dict3['c'] = 4          # re-assign a key to have a new value in dict
print(dict3)
print(dict3.keys())     # get list of keys from dict ('a', 'b')
print(dict3.values())   # get a list of values form dict (1, 2)
print(dict3.items())    # get a list of key-value pairs in dict

# loop through value between 0 and 9
for i in range(10):
    print(i)

# loop through values in list
for v in list3:
    print(v)

# loop through key:value pairs in dict
for k, v in dict3.items():
    print(k, '=', v)

# nested loops
for i in range(3):
    for j in range(4):
        print(i, j)




#######################################################
# Data for problem
list_of_dicts = [
    {'x': 71, 'y': 34},
    {'x': 12, 'y': 23},
    {'x': 31, 'y': 45},
    {'x': 65, 'y': 66},
    {'x': 12, 'y': 77}
]

#######################################################
# STEP 1
# first convert data from a list of dictionaries to a dictionary of list
# you can assume that each dict has exactly the same keys,
# you can assume that list_of_dicts has at least 1 element
# output should be assigned to variable "dict_of_lists"

## START STEP 1 CODE HERE
dict_of_lists = {}
list_keys = list_of_dicts[0].keys()

for mydict in list_of_dicts:
    # print(mydict.keys())
    # dict_of_lists[k] = list()
    for k, v in mydict.items():
        # dict_of_lists[k] = list()
        # dict_of_lists[k].append(v)
        # list_keys.append(k)
        print(k, '=', v)


print(list_keys)

## END STEP 1 CODE

expected_dict_of_lists = {
    'x': [71, 12, 31, 65, 12],
    'y': [34, 23, 45, 66, 77]
}
assert(expected_dict_of_lists == dict_of_lists)



#######################################################
# STEP2
# create dict with average for each key in dict_of_lists
# output should be assigned to variable "avg_dict"

## START STEP 2 CODE HERE

## END STEP 2 CODE

expected_avg_dict = {
    'x': 38.2,  # (71+12+31+65+12) / 5 = 38.2
    'y': 49.0
}
assert(expected_avg_dict == avg_dict)
#include "list.h"

#define CMD_LINE_ARG    0
#define BLOCK_SHUFFLER    1
#define DEBUG_CORE        0
#define DEBUG_SHUFFLER    0

int main(int argc, char *argv[])
{
    Node_t* deck_original_order = NULL;
    Node_t* deck_shuffled_order = NULL;
    Node_t* piles[MAX_PILES] = { NULL }; // max 5 stacks and combination of all of them

    bool match = false;

    static uint8_t num_of_piles = 3;
    static uint32_t counter = 0;

#if CMD_LINE_ARG == 1
    static uint8_t deck_size = 52;

    if (argc == 2)
    {
        deck_size = atoi(argv[1]);
        printf("The argument supplied is %u\n", deck_size);
    }
    else
    {
        printf("Program expects only One argument\n");
    }
#endif

    for (uint8_t i = 0; i < MAX_SUITS; i++)
    {
        for (uint8_t j = 1; j <= MAX_NUM_CARDS_PER_DECK; j++)
        {
            createNode(&deck_original_order, i, j);
            createNode(&deck_shuffled_order, i, j);
        }
    }

#if BLOCK_SHUFFLER == 1
    while (!match)
    {
        counter++;

        splitIntoNumList(deck_shuffled_order, piles, num_of_piles);

        for (uint32_t i = 1; i < num_of_piles; i++)
        {
            appendList(&piles[0], piles[i]);
        }

        deck_shuffled_order = piles[0];

        match = compareTwoList(deck_original_order, deck_shuffled_order);

        if (++num_of_piles > MAX_PILES)
        {
            num_of_piles = 3;
        }
    }
#endif

#if DEBUG_CORE == 1
    for (uint8_t i = 0; i < (MAX_SUITS*MAX_NUM_CARDS_PER_DECK); i++)
    {
        Node_t* temp = popAndReturnNode(&deck_original_order);

        pushNodeFront(&deck_shuffled_order, temp);
    }
#endif

#if DEBUG_SHUFFLER == 1
    num_of_piles = 2;

    splitIntoNumList(deck_shuffled_order, piles, num_of_piles);

    for (uint8_t i = 0; i < num_of_piles; i++)
    {
        printList(piles[i]);
    }
#endif

    printList(deck_original_order);
    printList(deck_shuffled_order);

    /* By observing variety of deck sizes, under go current shuffling method, around 6-8 rounds of shuffle puts every card in a differnt position  compared to original order. Shuffling less does not randomise the pack. But shuffluing more than 8-10, does not randmoise more but starts to put cards back in the deck in some sort of ascending or desending order. */
    printf("match found after %d rounds\n", counter);

    return 0;
}
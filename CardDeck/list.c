/* Headers */
#include "list.h"

// Private data struture
char *suit_printChars[MAX_SUITS] =
{
    [SPADES] = "S",
    [DIAMONDS] = "D",
    [HEARTS] = "H",
    [CLUBS] = "C",
};

/********************* Function Definitions **********************/
/* Description: Create a new node, allocate memory to it
 *              and store input data in the new node
 * Inputs: Double refernce pointer to Head Node of Linked List
 * Return: Void
*/
void createNode(Node_t ** head_ref, suits_t suit_code, uint8_t val)
{
    Node_t* new_node = (Node_t*)malloc(sizeof(Node_t));
    new_node->suit = suit_code;
    new_node->data = val;
    new_node->next = (*head_ref);
    (*head_ref) = new_node;
}

/* Description: Appends Linked List src (source) to end/rear of dst (destination)
 *              Destination list could be empty but source cannot be empty
 * Inputs: Double refernce pointer to destination Linked List
 *           Refernce pointer to srouce Linked List
 * Return: Void
*/
void appendList(Node_t **dst_ref, Node_t *src)
{
    Node_t *current = *dst_ref;

    if (src != NULL)
    {
        if (current == NULL)
        {
            current = src;
            return;
        }
        while (current->next != NULL)
        {
            current = current->next;
        }
        current->next = src;
    }
}

/* Description: Split the given srcList Linked List into inout value
 *              and store the resulting/ new linked lists separetely
 *              Each element from source list is removed ONE-by-ONE
 *              and store in new/ separate linked list
 * Inputs: Double refernce pointer to destination list of Linked List
 *           Refernce pointer to srouce Linked List
 *           num represent the desired num of new linked lists
 * Return: Void
*/
void splitIntoNumList(Node_t * srcList, Node_t ** dst_list_ref, uint8_t num)
{
    // split the nodes into the following list of linked list
    Node_t* dst_list[MAX_PILES] = { NULL };
    uint8_t index = 0;

    if (num < 2)
    {
        printf("Given Linked List cannot be split into less than 2 linked lists\n");
    }

    while (srcList != NULL)
    {
        Node_t* temp = popAndReturnNode(&srcList);
        pushNodeFront(&dst_list[index], temp);
        
        index++;
        
        if (index >= num)
        {
            index = 0;
        }
    }

    // Assign the input refence pointer with the local linked list pointers
    for (uint8_t i = 0; i < num; i++)
    {
        (dst_list_ref)[i] = dst_list[i];
    }
}

/* Description: Given 2 linked list as arguments, compare data at each node from both
 *              return true if all the element match in the same order, false otherwise.
 * Inputs: Refernce pointer to source and destination Linked Lists
 * Return: boolean value
*/
bool compareTwoList(Node_t * srcList, Node_t * dstList)
{
    while ((srcList != NULL) && (dstList != NULL))
    {
        if (srcList->data != dstList->data)
        {
            return false;
        }

        // Advance the src and dst pointer simultaneously 
        srcList = srcList->next;
        dstList = dstList->next;
    }

    // At this point both list should have been traveresd completely
    return (srcList == NULL && dstList == NULL);
}

/* Description: Remove the front node from given linked list and
 *              return pointer to the removed node.
 * Inputs: Refernce pointer to source Linked Lists
 * Return: Pointer to the node from linked list
*/
Node_t* popAndReturnNode(Node_t** headRef)
{
    if (*headRef == NULL)
    {
        return;
    }

    struct Node* head = *headRef;

    (*headRef) = (*headRef)->next;  // unlink the head node for the headRef

    return (head);
}

/* Description: Add new Node to front of the given linked list.
 * Inputs: Refernce pointer to source Linked Lists
 *         Pointer to Node to be added
 * Return: Void
*/
void pushNodeFront(Node_t** headRef, Node_t* newNode)
{
    newNode->next = (*headRef);
    (*headRef) = newNode;
}

/* Description: Print the linked list
 * Inputs: Refernce pointer to source Linked Lists
 * Return: void
*/
void printList(Node_t* ins)
{
    if (ins == NULL)
    {
        printf("list is empty!");
    }

    while (ins != NULL)
    {
        printf("%s", suit_printChars[ins->suit]);
        printf("%d\t", ins->data);
        ins = ins->next;
    }
    printf("\n");
}

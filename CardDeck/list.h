#ifndef _LIST_H_
#define _LIST_H_

#include <stdio.h> 
#include <stdlib.h> 
#include <stdint.h>
#include <stdbool.h>

#define MAX_PILES                    5
#define MAX_NUM_CARDS_PER_DECK        13

typedef enum
{
    SPADES,
    DIAMONDS,
    HEARTS,
    CLUBS,

    MAX_SUITS,
}suits_t;

typedef struct _Node_t {
    suits_t suit;
    uint8_t data;
    struct _Node_t* next;
}Node_t;

void createNode(Node_t **head_ref, suits_t suit_code, uint8_t val);
void appendList(Node_t **dst_ref, Node_t *src);
void splitIntoNumList(Node_t* srcList, Node_t** dst_list_ref, uint8_t num);
bool compareTwoList(Node_t* srcList, Node_t* dstList);
Node_t* popAndReturnNode(Node_t** headRef);
void pushNodeFront(Node_t** headRef, Node_t* newNode);

void printList(Node_t* ins);

#endif /* _LIST_H_ */
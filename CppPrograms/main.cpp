#include "graph.h"
#include "graph_matrix.h"

int main()
{
    // Create a graph given in the above diagram 
    //Graph g(5);
    Graph_Matrix g(5, 4);

    g.add_edge(0, 1);
    g.add_edge(0, 2);
    g.add_edge(1, 3);

    g.print_graph();

    cout << "DFS ";
    g.DFS(0);
    cout << endl;

    cout << "BFS ";
    g.BFS(0);
    cout << endl;

    return 0;
}
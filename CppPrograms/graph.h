#ifndef _GRAPH_H_
#define _GRAPH_H_

#include <iostream>
#include <list>
#include <stdint.h>

using namespace std;


class Graph
{
private:
    uint32_t num_vertices;
    list <uint32_t> *adj_list;

    void dfs_util(uint32_t num, bool visited[]);

public:
    Graph(uint32_t num_ver);
    void add_edge(uint32_t vertice_start, uint32_t vertice_end);

    void DFS(uint32_t start);
    void BFS(uint32_t start);

    void print_graph(void);
};

#endif /* _GRAPH_H_ */

#include "graph.h"

// Private Methods
void Graph::dfs_util(uint32_t num, bool visited[])
{
    visited[num] = true;
    cout << num << " ";

    list<uint32_t>::iterator i;

    for (i = adj_list[num].begin(); i != adj_list[num].end(); ++i)
    {
        if (!visited[*i])
        {
            dfs_util(*i, visited);
        }
    }
}

// Constructor
Graph::Graph(uint32_t num_ver)
{
    num_vertices = num_ver;
    adj_list = new list<uint32_t>[num_ver];
}

// Public Methods
void Graph::add_edge(uint32_t vertice_start, uint32_t vertice_end)
{
    adj_list[vertice_start].push_back(vertice_end);
}

void Graph::DFS(uint32_t start)
{
    bool *visited = new bool[num_vertices];
    
    for (uint32_t i = 0; i < num_vertices; i++)
    {
        visited[i] = false;
    }

    dfs_util(start, visited);
}

void Graph::BFS(uint32_t start)
{
    bool *visited = new bool[num_vertices];

    for (uint32_t i = 0; i < num_vertices; i++)
    {
        visited[i] = false;
    }

    list<uint32_t> queue;

    queue.push_back(start);
    visited[start] = true;

    list<uint32_t>::iterator i;

    while (!queue.empty())
    {
        start = queue.front();
        cout << start << " ";
        queue.pop_front();

        for (i = adj_list[start].begin(); i != adj_list[start].end(); ++i)
        {
            if (!visited[*i])
            {
                visited[*i] = true;
                queue.push_back(*i);
            }
        }
    }
}

void Graph::print_graph(void)
{
    for (uint32_t i = 0; i < num_vertices; i++)
    {
        list<uint32_t> ::iterator it;

        cout << i;

        for (it = adj_list[i].begin(); it != adj_list[i].end(); ++it)
        {
            cout << " -> " << *it ;
        }
        cout << endl;
    }
}

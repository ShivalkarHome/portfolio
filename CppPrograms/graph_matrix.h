#ifndef _GRAPH_MATRIX_H_
#define _GRAPH_MATRIX_H_

#include <iostream>
#include <vector>
#include <stdint.h>

using namespace std;


class Graph_Matrix
{
private:
    uint32_t num_vertices;
    uint32_t num_egdes;
    uint32_t **adj_list;

    void dfs_util(uint32_t num, vector<bool>& visited);
    
public:
    Graph_Matrix(uint32_t num_ver, uint32_t num_egde);
    void add_edge(uint32_t vertice_start, uint32_t vertice_end);

    void DFS(uint32_t start);
    void BFS(uint32_t start);

    void print_graph(void);
};

#endif /* _GRAPH_MATRIX_H_ */

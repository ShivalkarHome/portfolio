#include <vector>
#include "graph_matrix.h"

// Private Methods
void Graph_Matrix::dfs_util(uint32_t num, vector<bool>& visited)
{
    visited[num] = true;
    cout << num << " ";

    for (int i = 0; i < num_vertices; i++)
    {
        if (adj_list[num][i] == 1 && (!visited[i]))
        {
            dfs_util(i, visited);
        }
    }
}

// constructor
Graph_Matrix::Graph_Matrix(uint32_t num_ver, uint32_t num_egde)
{
    num_vertices = num_ver;
    num_egdes = num_egde;

    adj_list = new uint32_t*[num_ver];

    for (uint32_t row = 0; row < num_vertices; row++)
    {
        adj_list[row] = new uint32_t[num_vertices];
        for (uint32_t column = 0; column < num_vertices; column++)
        {
            adj_list[row][column] = 0;
        }
    }
}

// Public Methods
void Graph_Matrix::add_edge(uint32_t vertice_start, uint32_t vertice_end)
{
    adj_list[vertice_start][vertice_end] = 1;

}

void Graph_Matrix::DFS(uint32_t start)
{
    vector<bool> visited(num_vertices, false);

    dfs_util(start, visited);
}

void Graph_Matrix::BFS(uint32_t start)
{
    vector<bool> visited(num_vertices, false);
    vector<uint32_t> queue;

    queue.push_back(start);
    visited[start] = true;

    while (!queue.empty())
    {
        uint32_t index = queue[0];

        cout << index << " ";
        queue.erase(queue.begin());

        for (uint32_t i = 0; i < num_vertices; i++)
        {
            if (!visited[i] && adj_list[index][i] == 1)
            {
                queue.push_back(i);
                visited[i] = true;
            }
        }
    }
}

void Graph_Matrix::print_graph(void)
{
    for (uint32_t i = 0; i < num_vertices; i++)
    {
        for (uint32_t j = 0; j < num_vertices; j++)
        {
            cout << adj_list[i][j] << "\t";
        }
        cout << endl;
    }
}

import argparse

expected_sample = {}
expected_range = {}
first_key_values = []

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("echo")
    args = parser.parse_args()
    file_name = args.echo

    read_file = open(file_name, "r")

    # Read all lines in the file
    for read_line in read_file.readlines():
        words = read_line.split()
        if float(words[0]) < 1.0:
            first_key_values.append(int(words[1]))
        elif words[2] in expected_sample:
            expected_sample[words[2]].append(int(words[1]))
        else:
            expected_sample[words[2]] = []

    max_offset = max(first_key_values)

    for k in  expected_sample.keys():
        ll =  expected_sample[k]
        min_val = min(ll) - max_offset
        max_val = max(ll) - max_offset
        expected_range[k] = [min_val, max_val]
    # Close opened file
    read_file.close()
    print("expected_range =", expected_range)
    # print(max_offset)
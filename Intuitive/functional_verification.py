import argparse
import json
from os import kill

input_data = {}
first_key_values = []
expected_range = {'115': [-100, 111], '116': [116, 351], '117': [357, 590], '118': [603, 830], '119': [842, 1069], '120': [1080, 1308], '121': [1318, 1550], '122': [1553, 1783], '123': [1797, 2024], '124': [2043, 2270], '125': [2278, 2507], '126': [2522, 2746], '127': [2763, 2991], '128': [3003, 3223], '129': [3243, 3466], '130': [3494, 3704], '131': [3713, 3949], '132': [3960, 4187], '133': [4204, 4431], '134': [4445, 4669], '135': [4672, 4909], '136': [4916, 5143], '137': [5172, 5381], '138': [5400, 5621], '139': [5655, 5868], '140': [5888, 6105], '141': [6130, 6343], '142': [6364, 6583], '143': [6599, 6831], '144': [6847, 7061], '145': [7096, 7293], '146': [7325, 7550], '147': [7562, 7791], '148': [7801, 8029], '149': [8057, 8271], '150': [8279, 8503], '151': [8534, 8738], '152': [8760, 8984], '153': [8999, 9217], '154': [9232, 9461], '155': [9489, 9709], '156': [9715, 9944], '157': [9971, 10189], '158': [10200, 10424], '159': [10433, 10664], '160': [10672, 10907], '161': [10915, 11143], '162': [11159, 11382], '163': [11400, 11630], '164': [11649, 11866], '165': [11874, 12107], '166': [12118, 12349], '167': [12352, 12591], '168': [12603, 12819], '169': [12841, 13057], '170': [13076, 13308], '171': [13332, 13545], '172': [13562, 13783], '173': [13801, 14030], '174': [14038, 14267], '175': [14294, 14508], '176': [14521, 14746], '177': [14752, 14989], '178': [14992, 15227], '179': [15234, 15466], '180': [15476, 15703], '181': [15713, 15948], '182': [15958, 16185], '183': [16192, 16424], '184': [16433, 16669], '185': [16673, 16904], '186': [16912, 17147], '187': [17158, 17389], '188': [17393, 17629], '189': [17634, 17871], '190': [17874, 18111], '191': [18117, 18351], '192': [18352, 18353], '114': [-367, -130], '113': [-591, -378], '112': [-840, -629], '111': [-1044, -851], '110': [-1313, -1094], '109': [-1547, -1352], '108': [-1804, -1592], '107': [-2046, -1863], '106': [-2283, -2061], '105': [-2523, -2312], '104': [-2764, -2569], '103': [-3005, -2816], '102': [-3226, -3030], '101': [-3468, -3250], '100': [-3698, -3495], '99': [-3928, -3729], '98': [-4204, -3973], '97': [-4442, -4211], '96': [-4678, -4466], '95': [-4916, -4690], '94': [-5142, -4962], '93': [-5401, -5185], '92': [-5646, -5412], '91': [-5861, -5683], '90': [-6116, -5902], '89': [-6356, -6145], '88': [-6591, -6375], '87': [-6841, -6615], '86': [-7087, -6889], '85': [-7298, -7096], '84': [-7552, -7332], '83': [-7804, -7570], '82': [-8033, -7809], '81': [-8278, -8053], '80': [-8523, -8293], '79': [-8766, -8530], '78': [-9008, -8769], '77': [-9235, -9014], '76': [-9482, -9251], '75': [-9728, -9513], '74': [-9966, -9736], '73': [-10199, -9983], '72': [-10433, -10213], '71': [-10676, -10459], '70': [-10923, -10703], '69': [-11162, -10943], '68': [-11407, -11181], '67': [-11642, -11424], '66': [-11886, -11650], '65': [-12126, -11889], '64': [-12368, -12129], '63': [-12371, -12369]}

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("echo")
    args = parser.parse_args()
    first_time = True

    # get cmd arg filename
    file_name = args.echo
    # print(file_name)
    # read contents of file
    read_file = open(file_name, "r")
    #parse the file
    for read_line in read_file.readlines():
        words = read_line.split()
        if float(words[0]) < 1.0:
            first_key_values.append(int(words[1]))
        elif words[2] in expected_range.keys():
            if first_time:
                max_offset = max(first_key_values)
                first_time = False
                print("max intial offset", max_offset)
            ll = expected_range[words[2]]
            encoder_curr_val = int(words[1]) - max_offset
            # print(words[0], words[1], ll[0], ll[1])
            if(encoder_curr_val < ll[0]) or (encoder_curr_val > ll[1]):
                print("bad sample at:", words[0], "for pot val:", words[2])
    # close the file
    read_file.close()

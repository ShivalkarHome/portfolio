# Zipline Embedded Data Logger C++ Skeleton

This is a skeleton CMake project for building a data logger library and testing it with GoogleTest (gtest).
It includes C++ based sample application for demonstrating logger library usage and Python based log inspection module.

Pre-requisites:

* Install CMake using your platform's package manager. Debian/Ubuntu: `sudo apt install cmake`
* Install GCC/G++ using your platform's package manager. Debian/Ubuntu: `sudo apt install gcc`
* Install Python using your platform's package manager. Debian/Ubuntu: `sudo apt install python`

Useful commands:

* To clean up built artifacts, run
```
./clean.sh
```
* To build your library and run the test suite, run
```
./test.sh
```
* To build your library and along with sample application using `main.cc`, run
```
g++ -std=c++17 logger.h logger.cc main.cc -o test
```
 * Run python module `log_inspect.py` to analyses log file by running
```
python .\log_inspect.py -h
usage: log_inspect.py [-h] [-f FILE handle] [-ts1 datatime object] [-ts2 datatime object]

options:
  -h, --help            show this help message and exit
  -f FILE handle, --file FILE handle
                        log to analysis
  -ts1 datatime object, --timestamp1 datatime object
                        start timestamp1
  -ts2 datatime object, --timestamp2 datatime object
                        end timestamp2
```
 * Example of `log_inspect.py` will be
 ```
python .\log_inspect.py -f Log_20230719_010929.log -ts1="2023-07-19 01:09:29" -ts2="2023-07-19 01:09:30"
 ```

// Tests for the black box logger API.

#include "logger.h"
#include <gtest/gtest.h>

class LogTester : public ::testing::Test
{
protected:
    //....setup
    Logger AppTrace;
};

TEST_F(LogTester, defaultLimitTest)
{
    EXPECT_EQ(AppTrace.getThreshold(), 10000000);
}

TEST_F(LogTester, userSetLimitTest)
{
    AppTrace.setThreshold(100000);
    EXPECT_EQ(AppTrace.getThreshold(), 100000);
}

TEST_F(LogTester, userLimitTest)
{
    AppTrace.setThreshold(100000);
    EXPECT_NE(AppTrace.getThreshold(), 10000000);
}

TEST_F(LogTester, checkFileCreation)
{
    ASSERT_TRUE(AppTrace.isLogFileOpen());
}
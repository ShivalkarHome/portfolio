from datetime import datetime
from dataclasses import dataclass, field
import argparse


@dataclass
class logStruct:
    timestamp: str
    severity: str
    info: str
    timestamp_formatted = ""

    timestamp_format = "%Y-%m-%d %H:%M:%S"
    severity_types = ["DEBUG", "INFO", "WARNING", "ERROR"]

    def isValidTimeStamp(self):
        try:
            self.timestamp_formatted = datetime.strptime(
                self.timestamp, self.timestamp_format
            )
        except ValueError:
            print(self.timestamp, "is does not match format", self.timestamp_format)
            return False
        return True

    def isValidSeverity(self):
        if self.severity.replace(" ", "") in self.severity_types:
            return True
        print(self.severity, "not found in", self.severity_types)
        return False

    def verifyTimeStamp(self, timestamp1, timestamp2):
        try:
            self.timestamp_formatted = datetime.strptime(
                self.timestamp, self.timestamp_format
            )
        except ValueError:
            print(self.timestamp, "is does not match format", self.timestamp_format)

        if (
            timestamp1 <= self.timestamp_formatted
            and self.timestamp_formatted <= timestamp2
        ):
            return self.info
        return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-f", "--file", dest="filename", help="log to analysis", metavar="FILE handle"
    )
    parser.add_argument(
        "-ts1",
        "--timestamp1",
        dest="timestamp1",
        help="start timestamp1",
        metavar="data-time object",
    )
    parser.add_argument(
        "-ts2",
        "--timestamp2",
        dest="timestamp2",
        help="end timestamp2",
        metavar="data-time object",
    )

    args = parser.parse_args()

    # Verify input timestamps are valid
    try:
        ts1 = datetime.strptime(args.timestamp1, logStruct.timestamp_format)
    except ValueError:
        print(args.timestamp1, "is does not match format", logStruct.timestamp_format)

    try:
        ts2 = datetime.strptime(args.timestamp2, logStruct.timestamp_format)
    except ValueError:
        print(args.timestamp2, "is does not match format", logStruct.timestamp_format)

    try:
        fileHandle = open(args.filename, "r")

        for line in fileHandle:
            x = line.replace("[", "").split("]")
            # print(x)
            currLine = logStruct(x[0], x[1], x[2])

            currLine.isValidTimeStamp()
            currLine.isValidSeverity()
            print(currLine.verifyTimeStamp(timestamp1=ts1, timestamp2=ts2))
    except FileNotFoundError:
        print(args.filename, "does not exist, please check filename")

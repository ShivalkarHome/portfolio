// TODO Write your Data Logger API here!

#ifndef _LOGGER_H_
#define _LOGGER_H_

#include <iostream>
#include <fstream>
#include <sstream>
#include <ctime>
#include <string>


enum class LogLevel
{
    DEBUG,
    INFO,
    WARNING,
    ERROR
};

class Logger
{
private:
    uint32_t write_count;
    std::string logFileName;
    std::ofstream logFileHandle;
    const uint32_t maxLogRotateThreshold = 10000000;
    uint32_t logRotateThreshold;

    std::string getCurrentTime(bool formatted);
    void startNewLogFile();

public:
    Logger();
    Logger(uint32_t threshold);
    ~Logger();

    void LOG(LogLevel level, const char *fmt, ...);

    uint32_t getThreshold();
    void setThreshold(uint32_t newThreshold);

    bool isLogFileOpen();
    std::string getLogFileName();
};




#endif /* _LOGGER_H_ */
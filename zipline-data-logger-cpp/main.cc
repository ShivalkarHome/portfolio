#include "logger.h"

int main()
{
    float Vin = 0.0f;
    float OV_alert_level = 4.1f;
    float OV_fault_level = 4.25f;

    Logger APP_TRACE(10000000);
    bool isLogFileOpen = APP_TRACE.isLogFileOpen();
    std::string logFileNameStr = APP_TRACE.getLogFileName();

    std::cout << "Log Rotate threshold is: " << APP_TRACE.getThreshold() << std::endl;

    if(isLogFileOpen)
    {
        std::cout << "Log File Name is: " << logFileNameStr << std::endl;
    }

    APP_TRACE.LOG(LogLevel::DEBUG, "ADC Init successful!");
    APP_TRACE.LOG(LogLevel::INFO, "Voltage Monitor Application started.");

    for (uint32_t i=0;i<5000;i++)
    {
        APP_TRACE.LOG(LogLevel::DEBUG, "ADC reading, Vin:%f", Vin);

        if (Vin > OV_alert_level && Vin < OV_fault_level)
        {
            APP_TRACE.LOG(LogLevel::WARNING, "Vin above Alert and below error level!");
        }
        else if (Vin > OV_fault_level)
        {
            APP_TRACE.LOG(LogLevel::ERROR, "Vin above Fault level!");
        }

        Vin = Vin + 0.001f;
    }

    APP_TRACE.LOG(LogLevel::DEBUG, "Voltage Monitor Application finished execution!");
    APP_TRACE.~Logger();

    return 0;
}

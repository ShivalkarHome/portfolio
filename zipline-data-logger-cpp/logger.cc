#include <cstdarg>
#include <chrono>
#include <iomanip> // put_time

#include "logger.h"


// Private function definition
std::string Logger::getCurrentTime(bool formatted)
{
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);
    std::stringstream datetime;

    if (formatted)
    {
        datetime << std::put_time(std::localtime(&in_time_t), "%Y-%m-%d %H:%M:%S");
    }
    else
    {
        datetime << std::put_time(std::localtime(&in_time_t), "%Y%m%d_%H%M%S");
    }

    return datetime.str();
}

void Logger::startNewLogFile()
{
    if (logFileHandle.is_open())
    {
        logFileHandle.close();
    }

    // Reset write count for new log file
    write_count = 0;
    // Add timestamp to log file name
    logFileName = "Log_" + getCurrentTime(false) + ".log";

    // open new file
    logFileHandle.open(logFileName, std::ios_base::app);

    if (!logFileHandle.is_open())
    {
        std::cerr << "Failed to open log file: " << logFileName << std::endl;
    }
}

// Public function definition
Logger::Logger()
{
    logRotateThreshold = maxLogRotateThreshold;
    startNewLogFile();
}

Logger::Logger(uint32_t threshold)
{
    logRotateThreshold = threshold;
    startNewLogFile();
}

Logger::~Logger()
{
    if (logFileHandle.is_open())
    {
        logFileHandle.close();
    }
}

void Logger::LOG(LogLevel level, const char *fmt, ...)
{
    // Check if log rotation is needed
    if(write_count > logRotateThreshold)
    {
        std::cout << "need to rollover" << std::endl;
        startNewLogFile();
    }
    std::stringstream hFileWrite;
    hFileWrite << "[" << getCurrentTime(true) << "]";

    switch (level) {
        case LogLevel::DEBUG:
            hFileWrite << " [DEBUG] ";
            break;

        case LogLevel::INFO:
            hFileWrite << " [INFO] ";
            break;

        case LogLevel::WARNING:
            hFileWrite << " [WARNING] ";
            break;

        case LogLevel::ERROR:
            hFileWrite << " [ERROR] ";
            break;
    }

    // Format user string
    char prt_string[80] = {0};
    va_list argp;
    va_start(argp, fmt);

    uint32_t num_char = vsprintf(prt_string, fmt, argp);
    if (num_char > 0)
    {
        prt_string[num_char++] = '\0';
    }
    va_end(argp);

    // Write to the log file
    write_count++;
    hFileWrite << prt_string << std::endl;

    if (logFileHandle.is_open())
    {
        logFileHandle << hFileWrite.str(); // Output to log file
    }
}

uint32_t Logger::getThreshold()
{
    return logRotateThreshold;
}

void Logger::setThreshold(uint32_t newThreshold)
{
    logRotateThreshold = newThreshold;
}

bool Logger::isLogFileOpen()
{
    return logFileHandle.is_open();
}

std::string Logger::getLogFileName()
{
    return logFileName;
}

'''
receive control command from span panel
receive control command from user
publish current state
publish current enery usage
'''

import paho.mqtt.client as paho #mqtt library
import time
from random import randint
import re
from pytimedinput import timedInput


broker="localhost"
topic_data="data"
topic_cmd="cmd"
port=1883 #MQTT data listening port
energy_usage = 0
current_state = "OFF"


def on_message(client, userdata, message):
    global energy_usage, current_state
    published_data = message.payload.decode("utf-8")
    word_list = re.findall(r"[\w+']+", published_data)
    current_state = word_list[1]
    print("Current State:", current_state)


very_smart_plug= paho.Client("very_smart_panel") #create client object
very_smart_plug.connect(broker,port,keepalive=60) #establishing connection
very_smart_plug.subscribe(topic_cmd) #subscribe topic_cmd
very_smart_plug.on_message=on_message


if __name__ == "__main__":
    print("simulating button press:")
    while True:
        very_smart_plug.loop_start() #continuously checking for message
        energy_usage = str(randint(0, 9))
        payload="State:" + current_state + ",Energy:" + energy_usage
        if current_state == "ON":
            very_smart_plug.publish(topic_data,payload)
            print(payload)
        button_press, time_out = timedInput(timeOut=1)
        if button_press == "ON" or button_press == "OFF":
            current_state = button_press
        time.sleep(1)

'''
send state control commands
Receive current state
receive current energy usage
aggregate energy usage
display energy usage
'''
import paho.mqtt.client as paho
import re
import time


broker="localhost"
topic_data="data"
topic_cmd="cmd"
port=1883 #MQTT data listening port
energy_usage = 0
current_state = "ON"


def on_message(client, userdata, message):
    global energy_usage, current_state
    published_data = message.payload.decode("utf-8")
    word_list = re.findall(r"[\w+']+", published_data)
    current_state = word_list[1]
    energy_usage = energy_usage + int( word_list[3])
    print("Current State:", current_state)
    print("Energy Usage:", word_list[3], energy_usage)


span_panel = paho.Client("span_panel")
span_panel.on_message=on_message
span_panel.connect(broker,port,keepalive=60)
span_panel.subscribe(topic_data)


if __name__ == "__main__":
    start = int(time.process_time())
    while True:
        span_panel.loop_start() #continuously checking for message
        end = int(time.process_time())
        if ((end - start) > 30):
            start = int(time.process_time())
            if current_state == "OFF":
                current_state = "ON"
            else:
                current_state = "OFF"
            payload = "State:" + current_state
            span_panel.publish(topic_cmd, payload)

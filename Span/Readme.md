### Before you start:
1. Make sure pyhon 3.6.x or above is installed
2. Make sure pip 20.x.x or above is installed
3. run "pip install -r requirements.txt"

### How to run the program
1. run "python very_smart_plug.py"
2. run "python span_panel.py"
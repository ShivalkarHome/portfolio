## Program to find find 50 Pixels with hightest possible values in a grayscale image.
1. Gray scale image used is represented by 2D matrix of 1920 by 1080.
2. Max value of each pixel is represented with unsigned 16 bit value.
3. The program records top 50 pixels with hightest value and their location.


## Usage
1. `image.c` is auto generator using a python script.

```python image_gen.py```

2. Command to compile the program is below:

```gcc .\image.h .\image.c .\findHighestPixels.c -o findHighestPixels```

3. Run the executable.

```.\findHighestPixels```
- The program will print top 50 pixels with hightest value and their location.
// Includes
#include <stdio.h>
#include "image.h"

// Private Macros
#define NUM_HIGHEST_VALUES 50
#define NUM_MAX_COORD 2
#define X_COORD 0
#define Y_COORD 1

// Private Function Declaration
void findHighestPixels(uint16_t image[HEIGHT][WIDTH], uint16_t positions[NUM_HIGHEST_VALUES][2], uint16_t values[NUM_HIGHEST_VALUES]);

// Private Function Definition
void findHighestPixels(uint16_t image[HEIGHT][WIDTH], uint16_t positions[NUM_HIGHEST_VALUES][2], uint16_t values[NUM_HIGHEST_VALUES])
{
    uint16_t i, j, k;
    uint16_t maxIndex = 0;

    // Iterate through the image
    for (k = 0; k < NUM_HIGHEST_VALUES; k++)
    {
        uint16_t maxValue = 0;

        // Find the maximum value in the image
        for (i = 0; i < HEIGHT; i++)
        {
            for (j = 0; j < WIDTH; j++)
            {
                if (image[i][j] > maxValue)
                {
                    maxValue = image[i][j];
                    positions[k][X_COORD] = i;
                    positions[k][Y_COORD] = j;
                }
            }
        }

        // Set the maximum value to 0 to find the next highest value in the next iteration
        values[k] = image[positions[k][X_COORD]][positions[k][1]];
        image[positions[k][X_COORD]][positions[k][Y_COORD]] = 0;
    }
}

void main()
{
    uint16_t highestPixelPositions[NUM_HIGHEST_VALUES][NUM_MAX_COORD] = {0};
    uint16_t highestPixels[NUM_HIGHEST_VALUES] = {0};

    findHighestPixels(gray_image, highestPixelPositions, highestPixels);

    printf("50 pixels with the highest pixel values and their locations:\n");
    for (int k = 0; k < NUM_HIGHEST_VALUES; k++)
    {
        printf("Pixel Value: %d, Location: (%d, %d)\n",
               highestPixels[k],
               highestPixelPositions[k][X_COORD],
               highestPixelPositions[k][Y_COORD]);
    }

    return;
}

import random


def main():
    HEIGHT = 1920
    WIDTH = 1080

    image_src_fptr = open("image.c", "w")

    image_src_fptr.write('#include "image.h"\n\n')
    image_src_fptr.write("uint16_t gray_image [HEIGHT][WIDTH] = {\n")

    for i in range(0, HEIGHT):
        image_src_fptr.write("{")
        for j in range(0, WIDTH):
            k = random.randint(0, 65535)
            if j is (WIDTH - 1):
                s = str(k)
            else:
                s = str(k) + ", "
            image_src_fptr.write(s)
        image_src_fptr.write("},\n")
    image_src_fptr.write("};")
    image_src_fptr.close()


main()

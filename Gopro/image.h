#ifndef _IMAGE_H_
#define _IMAGE_H_

#include <stdint.h>

#define HEIGHT 1920
#define WIDTH 1080

extern uint16_t gray_image[HEIGHT][WIDTH];

#endif /* _IMAGE_H_ */
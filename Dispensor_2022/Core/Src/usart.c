/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    usart.c
  * @brief   This file provides code for the configuration
  *          of the USART instances.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdbool.h>
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* USER CODE BEGIN 0 */
#define BUFFER_SIZE             (16)
#define RX_NUM_BYTES		(1)
#define TX_BUFFER_SIZE          (32)
#define TX_POOL_SIZE		(10)

static int8_t tx_wrt_ptr = 0;
static uint8_t rx_write_pointer = 0;
static uint8_t fifo_buffer[BUFFER_SIZE] = {0};
static uint8_t tx_write_pool[TX_POOL_SIZE][TX_BUFFER_SIZE];

static void vprint(const char *fmt, va_list argp);
/* USER CODE END 0 */

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_tx;

/* USART2 init function */

void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 115200;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */
    HAL_UART_Receive_IT(&huart2+rx_write_pointer, fifo_buffer, RX_NUM_BYTES);
    rx_write_pointer = (rx_write_pointer+1)%BUFFER_SIZE;
  /* USER CODE END USART2_Init 2 */

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspInit 0 */

  /* USER CODE END USART2_MspInit 0 */
    /* USART2 clock enable */
    __HAL_RCC_USART2_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART2 GPIO Configuration
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX
    */
    GPIO_InitStruct.Pin = USART_TX_Pin|USART_RX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART2 DMA Init */
    /* USART2_TX Init */
    hdma_usart2_tx.Instance = DMA1_Stream6;
    hdma_usart2_tx.Init.Channel = DMA_CHANNEL_4;
    hdma_usart2_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
    hdma_usart2_tx.Init.PeriphInc = DMA_PINC_DISABLE;
    hdma_usart2_tx.Init.MemInc = DMA_MINC_ENABLE;
    hdma_usart2_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
    hdma_usart2_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
    hdma_usart2_tx.Init.Mode = DMA_NORMAL;
    hdma_usart2_tx.Init.Priority = DMA_PRIORITY_LOW;
    hdma_usart2_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
    if (HAL_DMA_Init(&hdma_usart2_tx) != HAL_OK)
    {
      Error_Handler();
    }

    __HAL_LINKDMA(uartHandle,hdmatx,hdma_usart2_tx);

  /* USER CODE BEGIN USART2_MspInit 1 */

  /* USER CODE END USART2_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspDeInit 0 */

  /* USER CODE END USART2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART2_CLK_DISABLE();

    /**USART2 GPIO Configuration
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX
    */
    HAL_GPIO_DeInit(GPIOA, USART_TX_Pin|USART_RX_Pin);

    /* USART2 DMA DeInit */
    HAL_DMA_DeInit(uartHandle->hdmatx);

    /* USART2 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART2_IRQn);
  /* USER CODE BEGIN USART2_MspDeInit 1 */

  /* USER CODE END USART2_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */
static void vprint(const char *fmt, va_list argp)
{
    char printStr[TX_BUFFER_SIZE] = {0};
    if (0 < vsprintf(printStr, fmt, argp)) // build string
    {
	strncpy((char *)&tx_write_pool[tx_wrt_ptr][0], printStr, TX_BUFFER_SIZE);
	if(tx_wrt_ptr == 0)
	{
	  HAL_StatusTypeDef status = HAL_UART_Transmit_DMA(&huart2, &tx_write_pool[tx_wrt_ptr][0], TX_BUFFER_SIZE);
	  if (status != HAL_OK)
	  {
	      HAL_UART_DMAStop(&huart2);
	  }
	}
	tx_wrt_ptr = (tx_wrt_ptr+1)%TX_POOL_SIZE;
    }
}

void UART_printf(const char *fmt, ...)
{
    va_list argp;
    va_start(argp, fmt);
    vprint(fmt, argp);
    va_end(argp);
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
    HAL_StatusTypeDef status = HAL_UART_Receive_IT(&huart2, fifo_buffer+rx_write_pointer, RX_NUM_BYTES);
    UNUSED(status);
    rx_write_pointer = (rx_write_pointer+1)%BUFFER_SIZE;
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
    if(tx_wrt_ptr > 0)
    {
      HAL_StatusTypeDef status = HAL_UART_Transmit_DMA(&huart2, &tx_write_pool[tx_wrt_ptr][0], TX_BUFFER_SIZE);
      tx_wrt_ptr = (tx_wrt_ptr-1);

      if (status != HAL_OK)
      {
	  HAL_UART_DMAStop(&huart2);
      }
    }
}
/* USER CODE END 1 */

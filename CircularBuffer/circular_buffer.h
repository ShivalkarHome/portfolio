#ifndef _CIRCULAR_BUFFER_H
#define _CIRCULAR_BUFFER_H

#include <stdbool.h>
#include <stdint.h>

#include <iostream>

using namespace std;

constexpr uint32_t MAX_BUFFER_SIZE = 256;

class Circular_Buffer {
 private:
  uint8_t data[MAX_BUFFER_SIZE];
  uint8_t write_index;
  uint8_t read_index;
  uint8_t size;

 public:
  Circular_Buffer(uint8_t val_size);
  uint32_t store_data(void *input_data, uint32_t input_size);
  uint32_t fetch_data(void *input_data, uint32_t input_size);

  void debug_print(void);
};

#endif /* _CIRCULAR_BUFFER_H */

#include "circular_buffer.h"

int main(void)
{
    Circular_Buffer ring_master(16);

    uint8_t arr1[] = { 0, 1, 2, 3, 4, 5, 6 };
    uint8_t arr2[] = { 7, 8, 9, 10, 11 };
    uint8_t arr3[] = { 12, 13, 14, 15, 16, 17 };
    uint8_t arr4[10] = { 0 };
    uint8_t write_len = 0;

    write_len = sizeof(arr1) / sizeof(arr1[0]);
    cout << (uint32_t)ring_master.store_data(arr1, write_len) << endl;

    write_len = sizeof(arr2) / sizeof(arr2[0]);
    cout << (uint32_t)ring_master.store_data(arr2, write_len) << endl;

    ring_master.debug_print();

    cout << (uint32_t)ring_master.fetch_data(arr4, 10) << endl;
    for (uint8_t i = 0; i < 10; i++)
    {
        cout << (uint32_t)arr4[i] << "\t";
    }
    cout << endl;

    write_len = sizeof(arr3) / sizeof(arr3[0]);
    cout << (uint32_t)ring_master.store_data(arr3, write_len) << endl;

    ring_master.debug_print();

    cout << (uint32_t)ring_master.fetch_data(arr4, 10) << endl;
    for (uint8_t i = 0; i < 10; i++)
    {
        cout << (uint32_t)arr4[i] << "\t";
    }
    cout << endl;

    return 0;
}

#include "circular_buffer.h"

#include <cstring>

Circular_Buffer::Circular_Buffer(uint8_t val_size) {
  write_index = 0;
  read_index = 0;
  size = val_size;

  memset(data, 0, size);
}

uint32_t Circular_Buffer::store_data(void *input_data, uint32_t input_size) {
  uint8_t write_count = 0;
  uint8_t *input_ref = (uint8_t *)input_data;

  while (input_size--) {
    data[write_index] = input_ref[write_count++];

    write_index = (write_index + 1) % size;

    if (size < MAX_BUFFER_SIZE) {
      size++;
    } else {
      read_index = (read_index + 1) % MAX_BUFFER_SIZE;
    }
  }

  return write_count;
}

uint32_t Circular_Buffer::fetch_data(void *input_data, uint32_t input_size) {
  uint8_t read_count = 0;
  if (size == 0) {
    return read_count;
  }

  if (input_size > MAX_BUFFER_SIZE) {
    input_size = MAX_BUFFER_SIZE;
  }

  uint8_t *input_ref = (uint8_t *)input_data;

  while (input_size--) {
    input_ref[read_count++] = data[read_index];

    read_index = (read_index + 1) % size;
  }

  return read_count;
}

void Circular_Buffer::debug_print(void) {
  cout << "write_index:" << (uint32_t)write_index
       << " read_index:" << (uint32_t)read_index << " size:" << (uint32_t)size
       << endl;

  for (uint8_t i = 0; i < size; i++) {
    cout << (uint32_t)data[i] << "\t";
  }
  cout << endl;
}

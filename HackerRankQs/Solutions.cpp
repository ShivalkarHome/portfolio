#include <queue> 
#include <map> 

#include "Solutions.h"
#include <unordered_map>


bool Solutions::isValidChar(char c)
{
    if ((c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
    {
        return true;
    }

    return false;
}

std::string Solutions::remove_backspace(std::string str) {
    std::string res;
    for (char c : str) {
        if (c != '#') {
            res.push_back(c);
        }
        else if (!res.empty()) {
            res.pop_back();
        }
    }
    return res;
}


int Solutions::getSum(int a, int b) {
    while (b != 0)
    {
        int carry = a & b;
        a = a ^ b;
        b = carry << 1;
    }

    return a;
}


int Solutions::missingNumber(std::vector<int>& nums) {
    size_t xor_num = nums.size();
    uint32_t i = 0;
    for (uint32_t i = 0; i < nums.size(); i++)
    {
        xor_num = xor_num ^ nums[i] ^ i;
    }
    return (int)xor_num;
}


uint32_t Solutions::reverseBits(uint32_t n) {
    int reverse = 0;
    for (uint32_t i = 0; i < 32; i++)
    {
        uint32_t MASK = (n & 0x1);
        n = n >> 1;
        reverse = (reverse << 1) | MASK;

        //printf("MASK:%x, n:%x, reverse:%x\n", MASK, n, reverse);
    }

    return reverse;
}


int Solutions::lengthOfLongestSubstring(std::string s) {
    uint32_t len = 0;
    uint32_t left = 0;
    uint32_t right = 0;
    uint32_t arr[26] = { 0 };
    std::queue<char> myqueue;

    for (uint32_t i = 0; i < s.length(); i++)
    {
        uint8_t idx = s[i] - 'a';

        if (arr[idx] == 0)
        {
            arr[idx]++;
            myqueue.push(s[i]);
        }
        else
        {
            arr[idx] = 0;
            while (myqueue.front() != s[i])
            {
                myqueue.pop();
            }
        }

        len = std::max(len, (uint32_t)myqueue.size());
        //printf("idx:%u, s[i]:%c, len:%lu\n", idx, s[i], myqueue.size());
    }

    return len;
}


bool Solutions::isAnagram(std::string s, std::string t) 
{
    uint16_t freq_s[26] = { 0 };
    uint16_t freq_t[26] = { 0 };

    for (uint16_t i = 0; i < s.length(); i++)
    {
        uint8_t idx = s[i] - 'a';
        freq_s[idx]++;
    }
    for (uint16_t i = 0; i < t.length(); i++)
    {
        uint8_t idx = t[i] - 'a';
        freq_t[idx]++;
    }
    for (uint8_t i = 0; i < 26; i++)
    {
        if (freq_t[i] != freq_s[i])
        {
            return false;
        }
    }
    return true;
}


bool Solutions::isPalindrome(std::string s)
{
    int32_t left = 0;
    int32_t right = s.length() - 1;

    while (left < right)
    {
        s[left] = tolower(s[left]);
        s[right] = tolower(s[right]);

        while ((left < (s.length() - 1)) && !isValidChar(s[left]))
        {
            left++;
        }


        while ((right > -1) && !isValidChar(s[right]))
        {
            right--;
        }

        /*std::cout << left << " "
            << s[left] << " "
            << right << " "
            << s[right] << "\n";*/


        if (s[left] != s[right])
        {
            return false;
        }

        left++;
        right--;
    }

    return true;
}

int Solutions::romanToInt(std::string s) 
{
    uint16_t result = 0;
    std::map<char, uint16_t> Roman;
    Roman['I'] = 1;
    Roman['V'] = 5;
    Roman['X'] = 10;
    Roman['L'] = 50;
    Roman['C'] = 100;
    Roman['D'] = 500;
    Roman['M'] = 1000;

    uint16_t s0 = 0;
    uint16_t s1 = 0;

    for (uint8_t i = 0; i < s.length(); i++)
    {
        s0 = Roman.find(s[i])->second;
        printf("s0::%u,%c:%u\n", i, s[i], s0);
        
        if (i < (s.length() - 1))
        {
            s1 = Roman.find(s[i+1])->second;
            printf("s1::%u,%c:%u\n", i, s[i], s1);
        }
        else
        {
            s1 = 0;
        }
        
        if (s1 > s0)
        {
            result = result + (s1 - s0);
            i++;
        }
        else
        {
            result = result + s0;
        }
    }

    return result;
}

int Solutions::search(std::vector<int>& nums, int target)
{
    int left = 0;
    int right = nums.size() - 1;
    int mid = right / 2;

    while (left <= right)
    {
        if (nums[left] == target)
        {
            return left;
        }
        else if (nums[right] == target)
        {
            return right;
        }
        else if (nums[mid] == target)
        {
            return mid;
        }
        else if (nums[mid] > target)
        {
            left++;
            right = mid - 1;
        }
        else if (nums[mid] < target)
        {
            left = mid + 1;
            right--;
        }
    }

    return -1;
}

bool Solutions::backspaceCompare(std::string s, std::string t)
{
    std::string s_rb = remove_backspace(s);
    std::string t_rb = remove_backspace(t);

    std::cout << s << " " << s_rb << std::endl;
    std::cout << t << " " << t_rb << std::endl;

    if (s_rb.compare(t_rb) == 0)
    {
        return true;
    }
    return false;
}

int Solutions::longestPalindrome(std::string s) {
    uint32_t res = 0;

    std::unordered_map<char, int> freq;

    for (uint32_t i = 0; i < s.length(); i++)
    {
        auto search = freq.find(s[i]);
        if (search != freq.end())
        {
            search->second = search->second + 1;
        }
        else
        {
            freq.insert({ s[i], 1 });
        }
    }

    for (auto& p : freq)
    {
        if (p.second % 2 == 0)
        {
            res = res + p.second;
        }
        else if (p.second % 2 == 1)
        {
            res = res + p.second - 1;
        }

        std::cout << p.first << " " << p.second << " " << res << std::endl;
    }

    return (res + 1);
}

std::string Solutions::longestPalindromeSubstring(std::string s) {
    std::string res = "";
    int resLen = 0;
    int left = 0;
    int right = 0;

    for (int i = 0; i < s.length(); i++)
    {
        left = i;
        right = i;
        while (left >= 0 && right < s.length()
            && s[left] == s[right])
        {
            if ((right - left + 1) > resLen)
            {
                resLen = right - left + 1;
                res.assign(s, left, resLen);
                std::cout << "res: " << res << std::endl;
            }
            left--;
            right++;
        }

        left = i;
        right = i + 1;
        while (left >= 0 && right < s.length()
            && s[left] == s[right])
        {
            if ((right - left + 1) > resLen)
            {
                resLen = right - left + 1;
                res.assign(s, left, resLen);
                std::cout << "res: " << res << std::endl;
            }
            left--;
            right++;
        }
    }

    return res;
}

std::vector<std::vector<int>> Solutions::threeSum(std::vector<int>& nums) {
    std::sort(std::begin(nums), std::end(nums));
    std::vector< std::vector<int> > results;

    for (int i = 0; i < nums.size(); i++)
    {
        int target = nums[i];
        int left = i + 1;
        int right = nums.size() - 1;
        while (left < right)
        {
            if ((target + nums[left] + nums[right]) == 0)
            {
                std::cout << "t:" << i
                    << ",l:" << left
                    << ",r: " << right << std::endl;
                std::cout << "t:" << target
                    << ",l:" << nums[left]
                    << ",r: " << nums[right] << std::endl;
                std::vector<int> v1 = { target, nums[left], nums[right] };
                results.push_back(v1);
                break;
            }
            else if (target > (nums[left] + nums[right]) || nums[left] == nums[left + 1])
            {
                left++;
            }
            else if (target < (nums[left] + nums[right]))
            {
                right--;
            }
        }
    }

    return results;
}

bool Solutions::isHappy(int n) {
    int number = n;
    int temp = 0;

    while (number > 0)
    {
        int digit = number % 10;
        number /= 10;
        temp = temp + digit * digit;
        
        std::cout << number << "," << digit << "," << temp << "\n";
        if (n == 0) {
            number = temp;
        }
    }

    return true;
}
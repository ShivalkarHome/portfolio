#pragma once

#include <iostream>
#include <vector>
#include <string>

#include <cstdint>

class Solutions
{
private:
	bool isValidChar(char c);
	std::string remove_backspace(std::string str);
public:
	int getSum(int a, int b);
	int missingNumber(std::vector<int>& nums);
	uint32_t reverseBits(uint32_t n);
	int lengthOfLongestSubstring(std::string s);
	bool isAnagram(std::string s, std::string t);
	bool isPalindrome(std::string s);
	int romanToInt(std::string s);
	int search(std::vector<int>& nums, int target);
	bool backspaceCompare(std::string s, std::string t);
	int longestPalindrome(std::string s);
	std::string longestPalindromeSubstring(std::string s);
	std::vector<std::vector<int>> threeSum(std::vector<int>& nums);
	bool isHappy(int n);
};


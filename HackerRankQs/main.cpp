#include "Solutions.h"

int main()
{
    Solutions sol;
    /*
    std::cout << "Sum: " << sol.getSum(-1, 1) << std::endl;

    std::vector<int> sample = {0,1};
    std::cout << "Missing num is: " << sol.missingNumber(sample) << std::endl;

    std::cout << "" << sol.reverseBits(0xA55AA55A) << std::endl;

    uint32_t num = 200;
    printf("num:%x, reverse:%x\n", num, sol.reverseBits(num));

    std::string s = "abcabcbb";
    std::string s1 = "bbbbb";
    std::string s2 =  "pwwkew";
    printf("len:%d\n", sol.lengthOfLongestSubstring(s));
    printf("len:%d\n", sol.lengthOfLongestSubstring(s1));
    printf("len:%d\n", sol.lengthOfLongestSubstring(s2));
    */

    /*
    std::string s1 = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaab";
    std::string s2 = "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbba";
    printf("isAnagram:%d\n", sol.isAnagram(s1, s2));
    */

    /*
    std::string s1 = "A man, a plan, a canal: Panama";
    std::cout << s1 << std::endl;
    std::cout << "isPalindrome: " << sol.isPalindrome(s1) << std::endl;
    */

    /*
    std::string s2 = "race a car";
    std::cout << s2 << std::endl;
    std::cout << "isPalindrome: " << sol.isPalindrome(s2) << std::endl;
    */
    
    /*
    std::string s3 = ".,";
    std::cout << s3 << std::endl;
    std::cout << "isPalindrome: " << sol.isPalindrome(s3) << std::endl;
    */

    
    /*
    std::string s3 = "MCMXCIV";
    std::cout << "Integer value of " << s3 
        << "is: " << sol.romanToInt(s3) << std::endl;
    */
    
    /*
    std::vector<int> nums1 = { 2,5 };
    std::vector<int> nums2 = { 5 };
    std::vector<int> nums3 = { -1, 0, 3, 5, 9, 12 };
    
    std::cout << "Target found at: " << sol.search(nums1, 0) << std::endl;
    std::cout << "Target found at: " << sol.search(nums2, 5) << std::endl;
    std::cout << "Target found at: " << sol.search(nums3, 9) << std::endl;
    std::cout << "Target found at: " << sol.search(nums3, 2) << std::endl;
    */
    
    /*
    bool isEqual = sol.backspaceCompare("ab##", "c#d#");
    std::cout << "Strings are equal: " << (isEqual ? "TRUE" : "FALSE") << std::endl;
    isEqual = sol.backspaceCompare("a##c", "#a#c");
    std::cout << "Strings are equal: " << (isEqual ? "TRUE" : "FALSE") << std::endl;
    */

    /*
    std::string sample = "civilwartestingwhetherthatnaptionoranynartionsoconceivedandsodedicatedcanlongendureWeareqmetonagreatbattlefiemldoftzhatwarWehavecometodedicpateaportionofthatfieldasafinalrestingplaceforthosewhoheregavetheirlivesthatthatnationmightliveItisaltogetherfangandproperthatweshoulddothisButinalargersensewecannotdedicatewecannotconsecratewecannothallowthisgroundThebravelmenlivinganddeadwhostruggledherehaveconsecrateditfaraboveourpoorponwertoaddordetractTgheworldadswfilllittlenotlenorlongrememberwhatwesayherebutitcanneverforgetwhattheydidhereItisforusthelivingrathertobededicatedheretotheulnfinishedworkwhichtheywhofoughtherehavethusfarsonoblyadvancedItisratherforustobeherededicatedtothegreattdafskremainingbeforeusthatfromthesehonoreddeadwetakeincreaseddevotiontothatcauseforwhichtheygavethelastpfullmeasureofdevotionthatweherehighlyresolvethatthesedeadshallnothavediedinvainthatthisnationunsderGodshallhaveanewbirthoffreedomandthatgovernmentofthepeoplebythepeopleforthepeopleshallnotperishfromtheearth";
    std::cout << "Longest Palindrome: " << sol.longestPalindrome(sample) << std::endl;
    */
    
    /*
    std::string sample = "babad";
    std::cout << "Longest Palindrome Sub String : " << sol.longestPalindromeSubstring(sample) << std::endl;
    */

    /*
    std::vector<int> sample = { 0,0,0,0 };
    std::vector<std::vector<int>> result = sol.threeSum(sample);
    */


    int n = 19;
    if (sol.isHappy(n)) {
        std::cout << n << " is a Happy Number\n";
    }

    n = 2;
    if (sol.isHappy(n)) {
        std::cout << n << " is a Happy Number\n";
    }

    return 0;
}

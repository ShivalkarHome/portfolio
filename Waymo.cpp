/*

Imagine self-driving car that only moves forward and backward in a straight
line.

The initial position is 0, speed is 1, and the car is pointing forward.

Car can be pre-programmed with a sequence of two instructions:

[A]accelerate:
- First move for one unit of time at current velocity,
- and then double the speed.

[AAA]

Start: position 0, speed 1, forward
   A:  position 0 + 1 -> 1, speed 1 * 2 = 2, forward
   A:  position 1 + 2 -> 3, speed 2 * 2 = 4, forward
   A:  position 3 + 4 -> 7, speed 4 * 2 = 8, forward

[R]everse:
- First change direction (forward -> backward or backward -> forward),
- and then reset the speed to 1
- but do not move (no position change).

[AAARA]

Start: position 0, speed 1, forward
   A:  position 0 + 1 -> 1, speed 1 * 2 = 2, forward
   A:  position 1 + 2 -> 3, speed 2 * 2 = 4, forward
   A:  position 3 + 4 -> 7, speed 4 * 2 = 8, forward
   R:  speed 1, backward, position 7
   A:  position 7 - 1 -> 6, speed 1 * 2 = 2, backward

A. Write a function "drive(sequence)" that, given a "sequence", calculates how
far the car will move, i.e. the "end position".

e.g. drive("AAA") -> 7


[AAARAAA]
Start: position 0, speed 1, forward
   A:  position 0 + 1 -> 1, speed 1 * 2 = 2, forward
   A:  position 1 + 2 -> 3, speed 2 * 2 = 4, forward
   A:  position 3 + 4 -> 7, speed 4 * 2 = 8, forward
   R:  speed 1, backward, position 7
   A:  position 7 - 1 -> 6, speed 1 * 2 = 2, backward
   A:  position 6 - 2 -> 4, speed 2 * 2 = 4, backward

B. Write the inverse function "driveTo(target)"" that, given a target position,
returns a "sequence" that will move to that position.

e.g. driveTo(7) -> "AAA"

string dirvetoTarget(int position){
    check with current position

    case greater than
    ex 9, f=7, move bardward, ARRA
    ex 10, f=7, move bardward, AA

    case less than
    ex: pos: 1, f= 7, move forward, AAAARA
                1 -> 2 -> 4 -> 8
}

AAAARRAARRAA
0 -> 1 -> 3 -> 7 -> 15 -> 16 -> 18 -> 19 -> 21

15, 1

21

0,1,+ and asked for -21
0 -> -1 -> -3 -> -7 -> -15 -> -16 -> 18 -> -19 -> -21

RAAAARRAARRAA
*/

#include <cstdint>
#include <iostream>
using namespace std;

#define FORWARD 1
#define BACKWARD 0

// initial condition
uint32_t speed = 1;           // 1--31 0s
uint8_t direction = FORWARD;  // 1 is forward and 0 is backwards
int32_t position = 0;

// position usr_cmd(char instruction){
int32_t usr_cmd(std::string instruction) {
  // should maintain record direction, speed, position

  for (uint32_t i = 0; i < instruction.length(); i++) {
    // current direction with instruction
    if (instruction[i] == 'A') {
      if (direction == FORWARD) {
        position = position + speed;  // account for increased speed
      } else {
        position = position - speed;
      }
      speed = speed * 2;  // right shifting 1
    } else if (instruction[i] == 'R') {
      direction =
          (direction == 1) ? 0 : 1;  // verify the syntax; check for bugs
      speed = 1;
    }
  }

  // update position
  // update speed

  return position;
}

// To execute C++, please define "int main()"
int main() {
  // std::cout << usr_cmd("AAA");

  // std::cout << usr_cmd("AAARAA");

  std::cout << usr_cmd("RAAAARRAARRAA");

  //   auto words = { "Hello, ", "World!", "\n" };
  //   for (const char* const& word : words) {
  //     cout << word;
  //   }

  return 0;
}

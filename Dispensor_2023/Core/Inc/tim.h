/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    tim.h
  * @brief   This file contains all the function prototypes for
  *          the tim.c file
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __TIM_H__
#define __TIM_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

extern TIM_HandleTypeDef htim1;

extern TIM_HandleTypeDef htim2;

/* USER CODE BEGIN Private defines */
typedef enum
{
    PWM_CHANNEL_NONE    = 0xFFFFFFFFU,
    PWM_CHANNEL_1       = 0x00000000U,
    PWM_CHANNEL_2       = 0x00000004U,
    PWM_CHANNEL_3       = 0x00000008U,
    PWM_CHANNEL_4       = 0x0000000CU,
    PWM_CHANNEL_ALL     = 0x0000003CU,
}pwm_channel_et;
/* USER CODE END Private defines */

void MX_TIM1_Init(void);
void MX_TIM2_Init(void);

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* USER CODE BEGIN Prototypes */
void PWM_set_duty_cycle(pwm_channel_et channel, uint8_t duty_cycle);
void PWM_channel_start(pwm_channel_et channel);
void PWM_channel_stop(pwm_channel_et channel);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif

#endif /* __TIM_H__ */

